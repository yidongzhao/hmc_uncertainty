import copy
import torch
import numpy as np
import time
import tqdm
from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD as TSVD
from pathlib import Path
from scipy.sparse import csr_matrix
import time
import joblib


def convert_state_dict_to_vector(state_dict):
    num_params = sum(p.numel() for p in state_dict.values())
    vec = np.zeros(shape=(num_params, ))
    pointer = 0
    for param in state_dict.values():
        param_dim = param.numel()
        vec[pointer: pointer + param_dim] = param.view(-1).numpy()
        pointer += param_dim
    return vec


log_pre, log_epi = u"\u001b[36m", u"\u001b[0m"
base_path = Path("/home/yidongzhao/umbrella/nnunetdata/results/nnUNet/2d/Task027_ACDC"
                 "/nnUNetTrainerV2_cSGHMC__nnUNetPlansv2.1/")
config = "mid_lr_m3_sch_ht"
epochs = list(range(868, 996, 4))

procedure_start = time.time()

# SGHMC weight loading
sghmc_weight_vec_list = []
for e in tqdm.tqdm(epochs):
    model = torch.load(base_path / Path(f"{config}/fold_0") / f"model_ep_{e:04d}.model")
    model = model["state_dict"]
    vec_e = convert_state_dict_to_vector(model)
    sghmc_weight_vec_list.append(vec_e)

# SVD
svd = TSVD(n_components=5, n_iter=7, random_state=21)
svd_start = time.time()

# build sparse matrix
dense_matrix = np.array(sghmc_weight_vec_list)
dense_mean = dense_matrix.mean(axis=0)
dense_matrix = dense_matrix - dense_mean
sparsify_start = time.time()
sparse_matrix = csr_matrix(dense_matrix)
sparsify_end = time.time()

print(log_pre + f"Sparsifying matrix (SGHMC): {sparsify_end - sparsify_start:.3f} seconds" + log_epi)
reduced_trajectory = svd.fit_transform(sparse_matrix)
svd_end = time.time()
print(log_pre + f"SGHMC-SVD: {svd_end - svd_start:.3f} seconds" + log_epi)

print("Sv:", svd.explained_variance_ratio_, svd.singular_values_)
joblib.dump([dense_mean, svd],
            f"/mnt/barrel/Data/sghmc_results/loss_landscape/unimodal_2d/{config}.svd")
procedure_end = time.time()
print(log_pre + f"Done: {procedure_end - procedure_start:.3f} seconds" + log_epi)