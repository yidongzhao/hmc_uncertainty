"""
For single model, we have several variants:
    - Max. softmax
    - Temperature scaling, T in [5, 10, 100]
    - MC-Dropout

Evaluation metrics:
    - DICE
    - Brier
    - IoU(U, E)
"""
import numpy as np
import torch

import utilities as util
import sys, os
import argparse
import tqdm
from pathlib import Path

parser = argparse.ArgumentParser(description="Generate predictions")
parser.add_argument("--config", type=str)
parser.add_argument("--name", type=str)
args = parser.parse_args()


# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


TERM_RED, TERM_BLUE, TERM_YELLOW, TERM_CYAN, TERM_RESET = "\u001b[31m", \
                                                          "\u001b[34m", \
                                                          "\u001b[33m", \
                                                          "\u001b[36m", \
                                                          "\u001b[0m"

TRAINER_NAME = "nnUNetTrainerV2_cSGHMC"
CONFIG = args.config
NAME = args.name
ALL_CHECKPOINTS = [[332 * (m + 1) - 4 * i for i in range(33)]
                   for m in range(3)]


trainer = util.restore_trainer(fold=0, network_trainer=TRAINER_NAME,
                               training=False, config=CONFIG)


def build_trainers(num_models=10):
    trainers = [util.restore_trainer(fold=0, network_trainer=TRAINER_NAME,
                                     training=False, config=CONFIG) for _ in range(num_models)]
    return trainers


def trainers_load_checkpoints(trainers, epochs_chosen, identifier="ep"):
    checkpoints = util.get_all_checkpoints(trainers[0], epochs=epochs_chosen, epoch_num_width=4, id=identifier)
    for t in trainers:
        checkpoint, checkpoint_path = next(checkpoints)
        t.load_checkpoint_ram(checkpoint, train=False)
        util.enable_test_time_dropout_trainer(t)


def trainers_load_checkpoint_files(trainers, checkpoint_files):
    for t, ckpt_f in zip(trainers, checkpoint_files):
        checkpoint = torch.load(ckpt_f)
        t.load_checkpoint_ram(checkpoint, train=False)
        util.enable_test_time_dropout_trainer(t)


def compute_averaged_dice(x, y, num_classes=4):
    aver_dice = 0
    aver_dice_denom = 0
    for c in range(1, num_classes):
        xc = (x == c)
        yc = (y == c)
        if xc.sum() + yc.sum() > 0:
            aver_dice_denom += 1
            dice = 2 * (xc * yc).sum() / (xc.sum() + yc.sum())
            aver_dice += dice
    return aver_dice / aver_dice_denom


def validate_diversity(_trainers, list_of_image_file, list_of_gt_file, num_classes=4):
    confusion_matrix = np.zeros((len(_trainers) + 1, len(_trainers) + 1))
    mutual_difference = np.zeros((len(_trainers) + 1, len(_trainers) + 1))
    aver_dice = [0 for _ in _trainers]
    accumulate_dice = [0 for _ in _trainers]

    for image_path, gt_path in tqdm.tqdm(zip(list_of_image_file, list_of_gt_file)):
        # load images and do padding, resizing
        image, gt, slicer, properties = util.load_nib_image_for_prediction(_trainers[0],
                                                                           image_path,
                                                                           gt_path,
                                                                           return_slicer=True)
        gt[gt < 0] = 0
        gt = gt[:, 0, :, :]
        prob = util.multiple_trainer_predict(_trainers, image_to_predict=image, return_indiv=True)
        average_prob = prob.mean(axis=0)

        pred_masks = prob.argmax(axis=2)    # individual predictive masks, [NE, SL, H, W]
        average_pred_mask = average_prob.argmax(1)  # averaged predictive mask, [SL, H, W]

        for ens in range(pred_masks.shape[0]):
            aver_dice[ens] += compute_averaged_dice(pred_masks[ens], gt, num_classes=num_classes) / len(list_of_image_file)
            prob_t = prob[:ens + 1, :, :, :, :].mean(axis=0)
            mask_t = prob_t.argmax(axis=1)
            dice_t = compute_averaged_dice(mask_t, gt, num_classes=num_classes) / len(list_of_image_file)
            accumulate_dice[ens] += dice_t

        # find error area
        error_area = (average_pred_mask != gt)
        # chosen_area = average_prob.max(axis=1) < 0.999
        # error_area = chosen_area
        for i in range(pred_masks.shape[0]):
            for j in range(i, pred_masks.shape[0]):
                mutual_difference[i, j] += (pred_masks[i] != pred_masks[j]).sum()
                mutual_difference[j, i] += (pred_masks[i] != pred_masks[j]).sum()
                pred1 = pred_masks[i, error_area]
                pred2 = pred_masks[j, error_area]
                dice = compute_averaged_dice(pred1, pred2, num_classes=num_classes)
                confusion_matrix[i, j] += dice
                confusion_matrix[j, i] += dice
            pred_aver = average_pred_mask[error_area]
            dice = compute_averaged_dice(pred1, pred_aver, num_classes=num_classes)
            confusion_matrix[i, -1] += dice
            confusion_matrix[-1, i] += dice
    confusion_matrix /= len(list_of_image_file)

    return {"confusion": confusion_matrix,
            "mutual_difference": mutual_difference,
            "aver_dice": np.array(aver_dice),
            "accmul_dice": np.array(accumulate_dice)}


list_of_image_file, list_of_gt_file = [], []
validation_base = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/EvaluationData/ACDC_ValidationSet")
variants = ["original"]

for v in variants:
    variant_base = validation_base / v
    for fname in (variant_base / "images").glob("*.nii.gz"):
        list_of_image_file.append(fname)
        list_of_gt_file.append(variant_base / "labels" / fname.parts[-1])

epochs = ALL_CHECKPOINTS[0][:10:2] + ALL_CHECKPOINTS[1][:10:2] + ALL_CHECKPOINTS[2][:10:2]
# epochs = ALL_CHECKPOINTS[2][::2]
trainers = build_trainers(num_models=len(epochs))
trainers_load_checkpoints(trainers, epochs_chosen=epochs)
res = validate_diversity(trainers, list_of_image_file, list_of_gt_file)
np.savez_compressed(f"/home/yidongzhao/barrel/Data/sghmc_results/diversity/{NAME}.npz", **res)


# DEEP ENSEMBLE
# trainers = build_trainers(num_models=15)
# ensemble_base = Path("/home/yidongzhao/umbrella/nnunetdata/results/nnUNet/2d/Task027_ACDC"
#                      "/nnUNetTrainerV2_cSGHMC__nnUNetPlansv2.1")
# checkpoint_paths = []
# for i in range(15):
#     path = ensemble_base / f"deep_ensemble_{i:02d}" / "fold_0" / "model_final_checkpoint.model"
#     if path.exists():
#         checkpoint_paths.append(path)
#     else:
#         path = ensemble_base / f"deep_ensemble_{i:02d}" / "fold_0" / "model_latest.model"
#         checkpoint_paths.append(path)
# trainers_load_checkpoint_files(trainers, checkpoint_paths)
# res = validate_diversity(trainers, list_of_image_file, list_of_gt_file)
# np.savez_compressed(f"/home/yidongzhao/barrel/Data/sghmc_results/diversity/ensemble.npz", **res)

#
# # DROPOUT
# confusion_matrix = validate_diversity(trainers, list_of_image_file, list_of_gt_file)
# np.savez_compressed(f"/tmp/tmp_confusion/ensemble.npz",
#                     confusion=confusion_matrix)
