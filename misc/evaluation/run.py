import metrics
from pathlib import Path
import numpy as np
import utilities as util
import pickle
import evalpaths as evp
import time
from medpy.metric.binary import hd95, hd, assd

TERM_RED, TERM_BLUE, TERM_YELLOW, TERM_CYAN, TERM_RESET = "\u001b[31m", \
                                                          "\u001b[34m", \
                                                          "\u001b[33m", \
                                                          "\u001b[36m", \
                                                          "\u001b[0m"


def run_evaluation(seg_base, prediction_base, evaluation_base, temp=1, name="default", vfilter=None):
    seg_base = Path(seg_base)
    prediction_base = Path(prediction_base)
    evaluation_base = Path(evaluation_base)

    # DICE coefficient
    dice_results = dict()
    per_slice_dice_results = dict()
    per_slice_num_pts = dict()
    mean_recall = dict()
    mean_entropy = dict()
    per_slice_entropy = dict()
    per_slice_recall = dict()

    # number of points
    per_slice_num_pts_pred = dict()
    per_slice_hd95_results = dict()

    # Error-Uncertainty Overlap
    per_slice_error_area = dict()
    per_slice_uncert_area = dict()
    per_slice_ue_overlap = dict()
    u_e_overlap_agg = dict()

    # calibration metric estimators
    N_BINS = 20
    ece_estimator = util.ExpectedCalibrationErrorEstimator(n_bins=N_BINS)
    brier_estimator = util.Brier_Estimator(n_classes=4)
    nll_estimator = util.NLL_Estimator(n_classes=4)

    num_image = 1
    for subject in prediction_base.iterdir():
    # for subject in [prediction_base / "patient163_A_1_frame10.npz"]:
        patient_frame_code = subject.parts[-1].split('.')[0]
        if not patient_frame_code.startswith("patient"):
            continue
        vendor = patient_frame_code.split('_')[1]
        if vfilter is not None:
            if vendor != vfilter:
                continue

        t = time.time()
        seg = np.load(seg_base / f"{patient_frame_code}.npz")["label"]
        with np.load(subject) as npdat:
            predicted_probability = npdat[f"temp={temp:.2f}"]
        t1 = time.time()

        # evaluate DICE coefficient
        aver_p_array = predicted_probability
        vol_predicted_segmentation = np.argmax(aver_p_array, axis=1)
        vol_dice_results = metrics.compute_hard_dice(seg[:, 0, :, :], vol_predicted_segmentation)
        per_slice_detail = [metrics.compute_hard_dice(seg[j, 0, :, :], vol_predicted_segmentation[j, :, :])
                            for j in range(seg.shape[0])]
        per_slice_dice = [pd[0][1:] for pd in per_slice_detail]
        per_slice_pts = [pd[1][1:] for pd in per_slice_detail]
        per_slice_pts_pred = [pd[2][1:] for pd in per_slice_detail]
        dice_results[patient_frame_code] = vol_dice_results
        per_slice_dice_results[patient_frame_code] = np.array(per_slice_dice)
        per_slice_num_pts[patient_frame_code] = np.array(per_slice_pts)
        per_slice_num_pts_pred[patient_frame_code] = np.array(per_slice_pts_pred)

        # hd95 results
        # hd95_dist = []
        # for sl in range(vol_predicted_segmentation.shape[0]):
        #     pred_seg_sl = vol_predicted_segmentation[sl, :, :]
        #     ref_seg_sl = seg[sl, 0, :, :]
        #     hd_95_per_class = []
        #     for c in [1, 2, 3]:
        #         pred_seg_c = pred_seg_sl == c
        #         ref_seg_c = ref_seg_sl == c
        #         if (pred_seg_c.sum() == 0) and (ref_seg_c.sum() == 0):
        #             hausdorff95 = 0
        #         elif (pred_seg_c.sum() > 0) and (ref_seg_c.sum() == 0):
        #             hausdorff95 = 100
        #         elif (pred_seg_c.sum() == 0) and (ref_seg_c.sum() > 0):
        #             hausdorff95 = 100
        #         else:
        #             hausdorff95 = hd95(pred_seg_c, ref_seg_c)
        #         hd_95_per_class.append(hausdorff95)
        #     hd95_dist.append(hd_95_per_class)
        # per_slice_hd95_results[patient_frame_code] = np.array(hd95_dist)

        # estimate recall rate
        recall, recall_slice = [], []
        entropy, entropy_slice = [], []
        error_area, uncertain_area = [], []
        u_e_overlap = []
        u_e_overlap_all_sl = []

        for c in (1, 2, 3):
            pc = aver_p_array[:, c, :, :]
            entropy_c = util.compute_binary_entropy(pc)
            entropy_threshold = 0.1
            entropy_c[entropy_c < entropy_threshold] = 0

            # compute error map
            seg_map_c = vol_predicted_segmentation == c
            gt_c = (seg[:, 0, :, :] == c)
            error_map_c = (seg_map_c != gt_c)
            u_e_overlap_all_sl.append(2 * (entropy_c * error_map_c).sum() / (entropy_c.sum() + error_map_c.sum()))
            seg_map_b = vol_predicted_segmentation != c
            beta = 0.2
            # H = (util.compute_entropy(aver_p_array, sumaxis=1) / 2)
            H = entropy_c
            tp = seg_map_c * (1 - beta * H)
            fn = seg_map_b * H
            fp = seg_map_c * beta * H
            denom = (2 * tp.sum() + fp.sum() + fn.sum())
            recall_c = 2 * tp.sum() / denom
            recall.append(recall_c)
            recall_per_slice = []

            # error-uncertainty overlap
            error_area_per_slice = []
            uncert_area_per_slice = []
            u_e_overlap_per_slice = []

            # for each slice
            for sl in range(entropy_c.shape[0]):
                entropy_c_sl = entropy_c[sl, :, :]
                error_area_sl_c = error_map_c[sl, :, :].sum()
                uncert_area_sl_c = entropy_c_sl.sum()

                # ERROR-uncertainty overlap
                u_e_overlap_per_slice.append(2 * (entropy_c_sl * error_map_c[sl, :, :]).sum() / (
                            uncert_area_sl_c + error_area_sl_c + 1e-6))
                error_area_per_slice.append(error_area_sl_c)
                uncert_area_per_slice.append(uncert_area_sl_c)

                tp_c = tp[sl, :, :]
                fp_c = fp[sl, :, :]
                fn_c = fn[sl, :, :]
                uncertain_mask = entropy_c_sl > entropy_threshold
                if tp_c.sum() < 5:
                    if uncertain_mask.any() and entropy_c_sl.max() < 2.0:
                        recall_per_slice.append(1 - entropy_c_sl[uncertain_mask].mean())
                    elif uncertain_mask.sum() < 10:
                        recall_per_slice.append(0.95)
                    else:
                        recall_per_slice.append(2 * tp_c.sum() / (2 * tp_c.sum() + fp_c.sum() + fn_c.sum()))
                else:
                    recall_per_slice.append(2 * tp_c.sum() / (2 * tp_c.sum() + fp_c.sum() + fn_c.sum()))
            recall_slice.append(recall_per_slice)
            error_area.append(error_area_per_slice)
            uncertain_area.append(uncert_area_per_slice)
            u_e_overlap.append(u_e_overlap_per_slice)

            entropy_slice_list = []
            for sl in range(entropy_c.shape[0]):
                entropy_c_sl = entropy_c[sl, :, :]
                seg_map_c_sl = seg_map_c[sl, :, :]
                if seg_map_c_sl.sum() > 5:
                    entropy_slice_list.append(entropy_c_sl[seg_map_c_sl].mean())
                else:
                    if (entropy_c_sl > 0.05).any():
                        entropy_slice_list.append(np.mean(entropy_c_sl[entropy_c_sl > 0.05]))
                    else:
                        entropy_slice_list.append(0.)
            entropy_slice.append(np.array(entropy_slice_list))

            if seg_map_c.sum() > 0:
                entropy.append(entropy_c[seg_map_c].mean())
            else:
                entropy.append(0.)

        # mean recall, entropy
        mean_recall[patient_frame_code] = recall
        per_slice_recall[patient_frame_code] = np.array(recall_slice).T
        mean_entropy[patient_frame_code] = entropy
        per_slice_entropy[patient_frame_code] = np.array(entropy_slice).T

        # error-uncertainty overlap
        per_slice_error_area[patient_frame_code] = np.array(error_area).T
        per_slice_uncert_area[patient_frame_code] = np.array(uncertain_area).T
        per_slice_ue_overlap[patient_frame_code] = np.array(u_e_overlap).T
        u_e_overlap_agg[patient_frame_code] = np.array(u_e_overlap_all_sl)

        # compute per image ECE
        error_map = seg[:, 0, :, :] != vol_predicted_segmentation
        seg = seg[:, 0, :, :]
        bbox = util.mask_to_bounding_box(seg > 0, alpha=1.0)

        ece_estimator.add_sample(aver_p_array.max(axis=1), error_map, mask=bbox)
        nll_estimator.add_sample(aver_p_array, seg, mask=bbox)
        brier_estimator.add_sample(aver_p_array, seg, mask=bbox)

        num_image += 1
        # print(f"\u001b[32m>>>ET: {time.time() - t:.3f} seconds, IO: {t1 - t:.3f} seconds<<<\u001b[0m")
        print(f"\r\t {patient_frame_code:40s} [{num_image:3d}], ET: {time.time() - t:.3f} seconds, "
              f"IO: {t1 - t:.3f} seconds", end="", flush=True)
    print("[----] Done!")

    ece_result = (ece_estimator.get_current_ece(),
                  ece_estimator.running_confidence,
                  ece_estimator.running_accuracy,
                  ece_estimator.running_sample_num)

    evaluation_results = {"dice": dice_results,
                          "mean_recall": mean_recall,
                          "mean_entropy": mean_entropy,
                          "per_slice_dice": per_slice_dice_results,
                          "per_slice_num_pts": per_slice_num_pts,
                          "per_slice_num_pts_pred": per_slice_num_pts_pred,
                          "per_slice_recall": per_slice_recall,
                          "per_slice_entropy": per_slice_entropy,
                          "per_slice_hd95": per_slice_hd95_results,
                          "per_slice_error_area": per_slice_error_area,
                          "per_slice_uncert_area": per_slice_uncert_area,
                          "per_slice_u_e_overlap": per_slice_ue_overlap,
                          "u_e_overlap_agg": u_e_overlap_agg,
                          "ece_acdc": ece_result,
                          "brier": brier_estimator.current_brier,
                          "nll": nll_estimator.current_nll
                          }
    evaluation_base.mkdir(exist_ok=True, parents=True)
    f_name = evaluation_base / f"{name}_T{temp:.2f}.pkl"
    with open(f_name, "wb") as stream:
        pickle.dump(evaluation_results, stream)


# prediction_base = evp.acdc_prediction_base
# run some evaluation

# Single model
# for mode in ["dropout", "phiseg",
#              "cSGHMC_lt_multi"]:
#     for num_model in [30]:
#         for run_id in range(1):
#             for scaling_t in [1, ]:
#                 print(TERM_RED + f"Evaluating {num_model} models, ID: {run_id}, T: {scaling_t}" + TERM_RESET)
#                 run_evaluation(prediction_base / "ResampledSeg",
#                                prediction_base / mode / f"Model_{num_model:02d}_Run_{run_id}",
#                                prediction_base / "EvaluationResults" / mode, temp=scaling_t,
#                                name=f"Model_{num_model}_Run_{run_id}")
# #
# mode = "phiseg"
# for num_model in [1, 3, 9, 12, 21, 30]:
#     for run_id in range(1):
#         for scaling_t in [1, ]:
#             print(TERM_RED + f"Evaluating {num_model} models, ID: {run_id}, T: {scaling_t}" + TERM_RESET)
#             run_evaluation(prediction_base / "ResampledSeg",
#                            prediction_base / mode / f"Model_{num_model:02d}_Run_{run_id}",
#                            prediction_base / "EvaluationResults" / mode, temp=scaling_t,
#                            name=f"Model_{num_model}_Run_{run_id}")

# mode = "cSGHMC_lt_single"
# for num_model in [1, 3, 9, 12, 21, 30]:
#     for run_id in range(1):
#         for scaling_t in [1, ]:
#             print(TERM_RED + f"Evaluating {num_model} models, ID: {run_id}, T: {scaling_t}" + TERM_RESET)
#             run_evaluation(prediction_base / "ResampledSeg",
#                            prediction_base / mode / f"Model_{num_model:02d}_Run_{run_id}",
#                            prediction_base / "EvaluationResults" / mode, temp=scaling_t,
#                            name=f"Model_{num_model}_Run_{run_id}")
# # #
# mode = "dropout"
# for num_model in [1, 3, 9, 12, 21, 30]:
#     for run_id in range(1):
#         for scaling_t in [1, ]:
#             print(TERM_RED + f"Evaluating {num_model} models, ID: {run_id}, T: {scaling_t}" + TERM_RESET)
#             run_evaluation(prediction_base / "ResampledSeg",
#                            prediction_base / mode / f"Model_{num_model:02d}_Run_{run_id}",
#                            prediction_base / "EvaluationResults" / mode, temp=scaling_t,
#                            name=f"Model_{num_model}_Run_{run_id}")
# # #
# # #
# mode = "RIE_default"
# for num_model in [15]:
#     for run_id in range(1):
#         for scaling_t in [1, ]:
#             print(TERM_RED + f"Evaluating {num_model} models, ID: {run_id}, T: {scaling_t}" + TERM_RESET)
#             run_evaluation(prediction_base / "ResampledSeg",
#                            prediction_base / mode / f"Model_{num_model:02d}_Run_{run_id}",
#                            prediction_base / "EvaluationResults" / mode, temp=scaling_t,
#                            name=f"Model_{num_model}_Run_{run_id}")


# MM Evaluation
prediction_base = evp.mm_prediction_base
# variants = ["RIE_best_1", "phiseg", "dropout", "RIE_default_15", "cSGHMC_lt2_single",
#             "cSGHMC_lt2_multi"]
variants = ["dropout"]
for vendor_filter in ["A", "B", "C", "D"]:
    for v in variants:
        if v.startswith("RIE"):
            n_model = int(v.split("_")[-1])
        else:
            n_model = 30
        print(TERM_BLUE, vendor_filter, v, TERM_RESET)
        run_evaluation(prediction_base / "ResampledSeg",
                       prediction_base / v / f"Model_{n_model:02d}_Run_5",
                       prediction_base / "EvaluationResults" / f"Vendor_{vendor_filter}", temp=1,
                       name=v,
                       vfilter=vendor_filter)
