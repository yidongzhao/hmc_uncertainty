from nnunet.run.default_configuration import get_default_configuration
import torch.nn as nn
from torch.nn.functional import softmax as softmax_helper
from pathlib import Path
import torch
from batchgenerators.augmentations.utils import pad_nd_image
import metrics
from collections import OrderedDict
import numpy as np
from scipy.ndimage import laplace as laplace
from PIL import ImageFont
from PIL import ImageDraw
from actseeker import ActivationSeeker
import itertools
import os
from nnunet.paths import user_defined_config_base
from typing import Union, Tuple
from nnunet.preprocessing.preprocessing import (get_lowres_axis,
                                                get_do_separate_z,
                                                resample_data_or_seg)


def create_image_segmentation_composed(image, segmentation, use_contour=False):
    # normalize, data format conversion
    image_normalized = (image - image.min()) / (image.max() - image.min())
    image_uint8_gray = (image_normalized * 255).astype(np.uint8)
    image_uint8_rgb = np.repeat(image_uint8_gray[:, :, None], 3, axis=2)

    segmentation_uint8_rgb = np.zeros((*segmentation.shape, 3), dtype=np.uint8)
    colors = [[222, 33, 33], [33, 222, 33], [33, 33, 222]]
    for c in range(4):
        if c == 0 or np.sum(segmentation == c) == 0:
            continue
        segmentation_uint8_rgb[segmentation == c, :] = colors[c - 1]

    # colorized segmentation map
    if use_contour:
        # compute contours for each class
        composed = image_uint8_rgb.copy()
        for c in range(4):
            if c == 0 or np.sum(segmentation == c) == 0:
                continue
            seg_c = (segmentation == c)
            contour_c = laplace(seg_c) > 0
            composed[contour_c, :] = colors[c - 1]
    else:
        # create composed image
        foreground = segmentation > 0
        composed = image_uint8_rgb.copy()
        composed[foreground, :] = image_uint8_rgb[foreground, :] / 2 + segmentation_uint8_rgb[foreground, :] / 2
    return composed, image_uint8_rgb, segmentation_uint8_rgb


def restore_trainer(fold=0, task="Task027_ACDC", network_trainer="nnUNetTrainerV2RIE",
                    network='2d', training=False, p_dropout=0.0, config=None):
    _, output_folder_name, dataset_directory, batch_dice, stage, trainer_class = \
        get_default_configuration(network, task, network_trainer)

    config_file = os.path.join(user_defined_config_base, f"{config}.json")
    if config is None:
        plans_file = f"{output_folder_name}/plans.pkl"
    else:
        plans_file = f"{output_folder_name}/{config}/plans.pkl"

    trainer = trainer_class(plans_file, fold, output_folder=output_folder_name, dataset_directory=dataset_directory,
                            batch_dice=batch_dice, stage=stage, unpack_data=True,
                            deterministic=False,
                            fp16=False,
                            user_defined_config=config_file)
    trainer.initialize(training=training, p_dropout=p_dropout)  # initialize data loader (if training is true)
    return trainer


def enable_test_time_dropout_module(m):
    if type(m) == nn.Dropout2d or type(m) == nn.Dropout3d:
        m.train()


def enable_test_time_dropout_trainer(trainer):
    trainer.network.eval()
    trainer.network.apply(enable_test_time_dropout_module)
    return trainer


def get_all_checkpoints(trainer, epochs=(100, 200), verbose=False, epoch_num_width=3, id="ep"):
    output_base = Path(trainer.output_folder)
    # get paths of all checkpoints
    for e in epochs:
        if epoch_num_width == 3:
            checkpoint_path = output_base / f"model_{id}_{e:03d}.model"
        else:
            checkpoint_path = output_base / f"model_{id}_{e:04d}.model"
        saved_model = torch.load(checkpoint_path)
        if verbose:
            print(f"Loading {checkpoint_path.parts[-3:]} done!")
        yield saved_model, checkpoint_path


def multi_trainer_predictions(trainers, image_to_predict, scaling_temp=(1, 5, 10)):
    # make predictions using multiple trainers
    num_slices = image_to_predict.shape[0]
    ensemble_predictions = {i: []
                            for i in range(num_slices)}
    for sl in range(num_slices):
        image_slice = image_to_predict[[sl], :, :, :]
        image_slice = torch.from_numpy(image_slice).cuda().float()
        prob = []
        for trainer in trainers:
            score = trainer.network(image_slice)[0]
            prob_scaled = dict()
            for st in scaling_temp:
                p = trainer.network.inference_apply_nonlin(score / st)
                p = p.detach().cpu().numpy()
                prob_scaled[st] = p[0, :, :, :]
            prob.append(prob_scaled)
        prob = np.array(prob)
        ensemble_predictions[sl] = prob
    return ensemble_predictions


def multiple_trainer_predict(trainers, image_to_predict, scaling_temp=1, return_indiv=False):
    # make predictions using multiple trainers
    image_torch = torch.from_numpy(image_to_predict).cuda().float()
    probabilities = []
    chunks = image_torch.shape[0] // 52 + (image_torch.shape[0] % 52 > 0)
    image_chunks = torch.chunk(image_torch, chunks, dim=0)
    for trainer in trainers:
        trainer.network.eval()
        p = []
        for image_chunk in image_chunks:
            # [0] for using the prediction at full resolution only
            score = trainer.network(image_chunk)[0]
            p_chunk = trainer.network.inference_apply_nonlin(score)
            p_chunk = p_chunk.detach().cpu().numpy()
            p.append(p_chunk)
        p = np.concatenate(p, axis=0)
        probabilities.append(p)
    # Bayesian averaging
    probabilities = np.array(probabilities)
    if return_indiv:
        ret = probabilities
    else:
        prediction = probabilities.mean(axis=0)
        ret = prediction
    return ret


def multiple_trainer_predict2(trainers, image_to_predict):
    # make predictions using multiple trainers
    ret = []
    for t in trainers:
        seg = t.predict_preprocessed_data_return_seg_and_softmax(image_to_predict,
                                                                 do_mirroring=False,
                                                                 mirror_axes=[],
                                                                 use_sliding_window=False,
                                                                 use_gaussian=False,
                                                                 verbose=True,
                                                                 mixed_precision=False)
        ret.append(seg[1])
    return np.array(ret)


def multiple_trainer_flow_predict(trainers, image_to_predict, group_image, group_flow,
                                  scaling_temp=1, return_indiv=False):
    # make predictions using multiple trainers
    image_torch = torch.from_numpy(image_to_predict).cuda().float()
    image_group = [torch.from_numpy(g).cuda().float() for g in group_image]
    flow_group = [torch.from_numpy(gf).cuda().float() for gf in group_flow]

    probabilities = []
    for trainer in trainers:
        # [0] for using the prediction at full resolution only
        score = trainer.network.group_forward(image_torch, image_group, flow_group)[0]
        p = trainer.network.inference_apply_nonlin(score / scaling_temp)
        p = p.detach().cpu().numpy()
        probabilities.append(p)
    # Bayesian averaging
    probabilities = np.array(probabilities)
    if return_indiv:
        ret = probabilities
    else:
        prediction = probabilities.mean(axis=0)
        ret = prediction
    return ret


def multiple_trainer_predict_multi_temp(trainers, image_to_predict, scaling_temp=(1, 2), return_indiv=False):
    # make predictions using multiple trainers
    image_torch = torch.from_numpy(image_to_predict).cuda().float()
    probabilities = []
    for trainer in trainers:
        # [0] for using the prediction at full resolution only
        score = trainer.network(image_torch)[0]
        pt = []
        for t in scaling_temp:
            p = trainer.network.inference_apply_nonlin(score / t)
            p = p.detach().cpu().numpy()
            pt.append(p)
        probabilities.append(pt)
    # Bayesian averaging
    probabilities = np.array(probabilities)
    if return_indiv:
        ret = probabilities
    else:
        prediction = probabilities.mean(axis=0)
        ret = prediction
    return ret


def multiple_trainer_predict_tent(trainers, image_to_predict, scaling_temp=1, return_indiv=False):
    # make predictions using multiple trainers
    image_torch = torch.from_numpy(image_to_predict).cuda().float()
    probabilities = []
    for trainer in trainers:
        # [0] for using the prediction at full resolution only
        score = tent_prediction(trainer, image_torch)[0]
        p = trainer.network.inference_apply_nonlin(score / scaling_temp)
        p = p.detach().cpu().numpy()
        probabilities.append(p)
    # Bayesian averaging
    probabilities = np.array(probabilities)
    if return_indiv:
        ret = probabilities
    else:
        prediction = probabilities.mean(axis=0)
        ret = prediction
    return ret


def retrieve_activations(trainer, image_to_predict, feature_paths=None):
    image_torch = torch.from_numpy(image_to_predict).cuda().float()
    if feature_paths is None:
        localization_paths = [f"conv_blocks_localization.{i}.{j}.blocks.0.instnorm" for i in range(6) for j in range(2)]
        context_paths = [f"conv_blocks_context.{i}.blocks.{j}.instnorm" for i in range(6) for j in range(2)] + \
                        [f"conv_blocks_context.6.{j}.blocks.0.instnorm" for j in range(2)]
        seg_paths = ["seg_outputs.5"]
        localization_paths.reverse()
        context_paths.reverse()
        feature_paths = seg_paths + localization_paths + context_paths
    trainer.network.eval()
    grad_act_seeker = ActivationSeeker()
    grad_act_seeker.attach_hooks(trainer.network, {x: x for x in feature_paths})
    trainer.network.zero_grad()
    output = trainer.network(image_torch)
    activations = grad_act_seeker.activation
    # full resolution
    activations["image"] = image_torch
    activations["output"] = output[0]
    for block_name in activations.keys():
        activations[block_name] = activations[block_name].detach().cpu().numpy()
    grad_act_seeker.remove_handles()
    return activations


def tent_prediction(trainer, image_torch, n_update=100):
    # image_torch = torch.from_numpy(image_to_predict).cuda().float()
    trainer.network.eval()
    entropy_loss = PredictionEntropyLoss()

    # allow instance normalization update
    def enable_inst_norm_update(m):
        if type(m) == nn.InstanceNorm2d:
            m.train()

    trainer.network.apply(enable_inst_norm_update)
    # require grad!
    for m in trainer.network.modules():
        if isinstance(m, nn.InstanceNorm2d):
            for p in m.parameters():
                p.requires_grad = True
        else:
            for p in m.parameters():
                p.requires_grad = False
    norm_layer_params = itertools.chain.from_iterable([m.parameters() for m in trainer.network.modules()
                                                       if isinstance(m, nn.InstanceNorm2d)])
    optimizer = torch.optim.Adam(norm_layer_params, lr=1e-3)
    for i in range(n_update):
        optimizer.zero_grad()
        output = trainer.network(image_torch)[0]
        entropy = entropy_loss(output)
        entropy.backward()
        optimizer.step()
        if i == 0:
            print(f"\tStep: {i:03d}, entropy:{entropy.detach().cpu().item():.3e}")
    print(f"\tStep: {i:03d}, entropy:{entropy.detach().cpu().item():.3e}")

    trainer.network.eval()
    output = trainer.network(image_torch)
    return output


class PredictionEntropyLoss(nn.Module):
    def __init__(self, eps=1e-6):
        super().__init__()
        self.eps = eps
        # self.eps = torch.Tensor(self.eps).cuda().float()

    def forward(self, X):
        prob = torch.softmax(X / 2, dim=1)
        prob = torch.clip(prob, min=self.eps, max=(1 - self.eps))
        log_prob = torch.log2(prob)
        return torch.mean(torch.sum(- prob * log_prob, dim=1))


def single_trainer_multi_predictions(trainer, image_to_predict, n_forward=30, scaling_temp=(1, 5, 10)):
    # make predictions using multiple trainers
    num_slices = image_to_predict.shape[0]
    ensemble_predictions = {i: []
                            for i in range(num_slices)}
    for sl in range(num_slices):
        image_slice = image_to_predict[[sl], :, :, :]
        image_slice = torch.from_numpy(image_slice).cuda().float()
        prob = []
        for _ in range(n_forward):
            score = trainer.network(image_slice)[0]
            prob_scaled = dict()
            for st in scaling_temp:
                p = trainer.network.inference_apply_nonlin(score / st)
                p = p.detach().cpu().numpy()
                prob_scaled[st] = p[0, :, :, :]
            prob.append(prob_scaled)
        prob = np.array(prob)
        ensemble_predictions[sl] = prob
    return ensemble_predictions


def get_predictions_at_T(all_predictions, T=1):
    result = dict()
    for k, v in all_predictions.items():
        frame_result = [x[T] for x in v]
        result[k] = frame_result
    return result


def segmentation_mask_from_p(all_predictions):
    result = dict()
    for k, v in all_predictions.items():
        varray = np.array(v)
        aver_p = np.mean(varray, axis=0)
        segmentation_map = np.argmax(aver_p, axis=0)
        result[k] = (aver_p, segmentation_map)
    return result


def segmentation_mask_dict_to_array(result):
    frame_indices = list(result.keys())
    frame_indices.sort()
    prediction = []
    for f_idx in frame_indices:
        prediction.append(result[f_idx][1])
    return np.array(prediction)


def convert_probability_dict_to_array(probabilities):
    result = []
    frame_indices = list(probabilities.keys())
    frame_indices.sort()
    for i in range(len(probabilities[0])):
        probability_i = np.array([probabilities[fi][i] for fi in frame_indices])
        result.append(probability_i)
    return result


def compute_mutual_dice(list_of_p):
    n_pred = len(list_of_p)
    if n_pred == 1:
        mutual_dice = [1, 1, 1]
    else:
        cross_dice = []
        for i in range(n_pred - 1):
            for j in range(i + 1, n_pred):
                seg_i, seg_j = list_of_p[i].argmax(axis=1), list_of_p[j].argmax(axis=1)
                dice_ij = metrics.compute_hard_dice(seg_i, seg_j)[0]
                cross_dice.append(dice_ij)
        cross_dice = np.array(cross_dice)
        mutual_dice = cross_dice.mean(axis=0)
    return mutual_dice


def compute_consecutive_dice(p_array):
    n_pred = p_array.shape[0]
    if n_pred == 1:
        mutual_dice = [1, 1, 1]
    else:
        per_slice_cross_dice = []
        if n_pred <= 5:
            for i in range(n_pred - 1):
                for j in range(i, n_pred):
                    seg_i, seg_j = p_array[i].argmax(axis=1), p_array[j].argmax(axis=1)
                    per_slice_dice_ij = [metrics.compute_hard_dice(seg_i[fi, :], seg_j[fi, :])[0]
                                         for fi in range(seg_i.shape[0])]
                    per_slice_cross_dice.append(per_slice_dice_ij)
        else:
            for i in range(n_pred - 1):
                seg_i, seg_j = p_array[i].argmax(axis=1), p_array[i + 1].argmax(axis=1)
                per_slice_dice_ij = [metrics.compute_hard_dice(seg_i[fi, :], seg_j[fi, :])[0]
                                     for fi in range(seg_i.shape[0])]
                per_slice_cross_dice.append(per_slice_dice_ij)
        per_slice_cross_dice = np.array(per_slice_cross_dice)
        per_slice_cross_dice[per_slice_cross_dice == -1.0] = 1.0
        per_slice_cross_dice = per_slice_cross_dice.mean(axis=0)
        mutual_dice = per_slice_cross_dice
    return mutual_dice


def compute_per_slice_dice(list_of_p):
    n_pred = len(list_of_p)
    if n_pred == 1:
        mutual_dice = [1, 1, 1]
    else:
        cross_dice = []
        for i in range(n_pred - 1):
            for j in range(i + 1, n_pred):
                seg_i, seg_j = list_of_p[i].argmax(axis=1), list_of_p[j].argmax(axis=1)
                dice_ij = metrics.compute_hard_dice(seg_i, seg_j)[0]
                cross_dice.append(dice_ij)
        cross_dice = np.array(cross_dice)
        mutual_dice = cross_dice.mean(axis=0)
    return mutual_dice


def compute_ECE(conf, error_map, step=0.1, chosen_mask=None):
    ece = 0.
    if chosen_mask is None:
        chosen_mask = np.ones_like(error_map)
    for i in range(10):
        mask = (conf >= i * step) & (conf < (i + 1) * step) & chosen_mask
        n_samples = mask.sum()
        if n_samples > 0.0:
            conf_bin = conf[mask].mean()
            acc_bin = (1 - error_map[mask]).mean()
            ece += n_samples * np.abs(conf_bin - acc_bin)
    return ece / conf[chosen_mask].size


def dropout_model_prediction(trainer, image_to_predict, n_forwards=15, scaling_temp=(1, 2,), return_indiv=False):
    # make predictions using multiple trainers
    image_torch = torch.from_numpy(image_to_predict).cuda().float()
    probabilities = []
    for forward in range(n_forwards):
        # [0] for using the prediction at full resolution only
        score = trainer.network(image_torch)[0]
        pt = []
        for t in scaling_temp:
            p = trainer.network.inference_apply_nonlin(score / t)
            p = p.detach().cpu().numpy()
            pt.append(p)
        probabilities.append(pt)
    # Bayesian averaging
    probabilities = np.array(probabilities)
    if return_indiv:
        ret = probabilities
    else:
        prediction = probabilities.mean(axis=0)
        ret = prediction
    return ret


def PHiSeg_model_prediction(trainer, image_to_predict, n_forwards=15, scaling_temp=(1, 2,), return_indiv=False):
    # make predictions using multiple trainers
    image_torch = torch.from_numpy(image_to_predict).cuda().float()
    probabilities = []
    with torch.no_grad():
        prior_mu, prior_sigma, _ = trainer.prior(image_torch)
        for forward in range(n_forwards):
            # [0] for using the prediction at full resolution only
            z = [mu + sigma * torch.randn(sigma.size(), device=sigma.device) for mu, sigma in
                 zip(prior_mu, prior_sigma)]
            logits = trainer.likelihood(z)
            score = logits[0]
            pt = []
            for t in scaling_temp:
                p = softmax_helper(score / t, dim=1)
                p = p.detach().cpu().numpy()
                pt.append(p)
            probabilities.append(pt)
    # Bayesian averaging
    probabilities = np.array(probabilities)
    if return_indiv:
        ret = probabilities
    else:
        prediction = probabilities.mean(axis=0)
        ret = prediction
    return ret


def load_nib_image_for_prediction(trainer, nib_image_path: Path, nib_seg_path: Path = None, return_slicer=False,
                                  divisible_by=None):
    seg_file_str = nib_seg_path.as_posix() if nib_seg_path is not None else None
    data, seg, properties = trainer.preprocess_patient_seg([nib_image_path.as_posix()],
                                                           seg_file=seg_file_str,
                                                           verbose=False)
    if divisible_by is None:
        divisible_by = trainer.network.input_shape_must_be_divisible_by
    data, slicer = pad_nd_image(data, mode="constant", kwargs=None, return_slicer=True,
                                shape_must_be_divisible_by=divisible_by)
    data = np.transpose(data, axes=[1, 0, 2, 3])
    if seg is not None:
        seg = np.transpose(seg, axes=[1, 0, 2, 3])
        seg, _ = pad_nd_image(seg, mode="constant", kwargs=None, return_slicer=True,
                              shape_must_be_divisible_by=divisible_by)
    else:
        seg = None
    if return_slicer:
        return data, seg, slicer, properties
    else:
        return data, seg


def load_nib_image_for_prediction_3D(trainer, nib_image_path: Path, nib_seg_path: Path = None, return_slicer=False,
                                     divisible_by=None):
    seg_file_str = nib_seg_path.as_posix() if nib_seg_path is not None else None
    data, seg, properties = trainer.preprocess_patient_seg([nib_image_path.as_posix()],
                                                           seg_file=seg_file_str,
                                                           verbose=False)
    if divisible_by is None:
        divisible_by = trainer.network.input_shape_must_be_divisible_by
    data, slicer = pad_nd_image(data, mode="edge", kwargs={}, return_slicer=True,
                                shape_must_be_divisible_by=divisible_by)

    if seg is not None:
        seg, _ = pad_nd_image(seg, mode="edge", kwargs={}, return_slicer=True,
                              shape_must_be_divisible_by=divisible_by)
    else:
        seg = None
    if return_slicer:
        return data, seg, slicer, properties
    else:
        return data, seg


def stochastic_weight_averaging_gaussian_fitting(checkpoints):
    weights_mean = OrderedDict()
    weights_diagonal = OrderedDict()
    weights_D = OrderedDict()
    ckpt_idx = 0
    for ckpt in checkpoints:
        network_weights = ckpt["state_dict"]
        for weight_name in network_weights.keys():
            if weight_name.endswith(".instnorm.bias") or weight_name.endswith(".instnorm.weight"):
                weights_mean[weight_name] = network_weights[weight_name]
                continue

            current_mean = weights_mean.get(weight_name, 0.)
            current_diagonal = weights_diagonal.get(weight_name, 0.)
            current_D = weights_D.get(weight_name, [])

            new_mean = current_mean * ckpt_idx / (ckpt_idx + 1) + network_weights[weight_name] / (ckpt_idx + 1)
            new_diagonal = current_diagonal * ckpt_idx / (ckpt_idx + 1) + (network_weights[weight_name] ** 2) / (
                    ckpt_idx + 1)
            if ckpt_idx > 0:
                current_D.append(network_weights[weight_name] - new_mean)

            weights_mean[weight_name] = new_mean
            weights_diagonal[weight_name] = new_diagonal
            weights_D[weight_name] = current_D
        ckpt_idx += 1

    for weight_name in weights_diagonal.keys():
        weights_diagonal[weight_name] = weights_diagonal[weight_name] - weights_mean[weight_name] ** 2

    return weights_mean, weights_diagonal, weights_D


def update_batch_normalization(net_trainer, evaluate_loss=False):
    net_trainer.network.train()
    n_forward = 100
    for _ in range(n_forward):
        net_trainer.run_iteration(net_trainer.tr_gen, do_backprop=False)

    if evaluate_loss:
        losses = []
        for _ in range(n_forward):
            losses.append(net_trainer.run_iteration(net_trainer.tr_gen, do_backprop=False))
    else:
        losses = None

    del net_trainer.tr_gen
    del net_trainer.val_gen

    return net_trainer, losses


def SWAG_sampling(w_mean, w_diagonal, w_d):
    sampled_weight = OrderedDict()
    for weight_name in w_mean.keys():
        if weight_name.endswith(".instnorm.bias") or weight_name.endswith(".instnorm.weight"):
            sampled_weight[weight_name] = w_mean[weight_name]
            continue

        mu = w_mean[weight_name]
        sigma = torch.clamp(w_diagonal[weight_name], min=0.) ** 0.5
        D = w_d[weight_name]
        K = len(D)
        D = torch.stack(D, dim=-1)

        sampled = mu + 0.707 * sigma * torch.randn(mu.size()) + 0.707 / np.sqrt(K - 1) * torch.matmul(D, torch.randn(K))
        if torch.isnan(sampled).any():
            print(f"NaN: {weight_name}")
        sampled_weight[weight_name] = sampled
    return sampled_weight


class ExpectedCalibrationErrorEstimator:
    def __init__(self, n_bins=50, start=0.0, end=1.0):
        self.n_bins = n_bins
        self.bin_values, self.step = np.linspace(start, end, num=self.n_bins, endpoint=False, retstep=True)
        self.running_sample_num = np.zeros_like(self.bin_values)
        self.running_accuracy = np.zeros_like(self.bin_values)
        self.running_confidence = np.zeros_like(self.bin_values)

    def add_sample(self, confidence, error_map, mask=None):
        if mask is not None:
            confidence = confidence[mask]
            error_map = error_map[mask]

        for bv_ind, bv in enumerate(self.bin_values.tolist()):
            in_bin_mask = (confidence >= bv) & (confidence < self.step + bv)
            n_in_bin = in_bin_mask.sum()
            if n_in_bin == 0:
                continue
            c_in_bin = confidence[in_bin_mask]
            e_in_bin = error_map[in_bin_mask]
            a_in_bin = (1 - e_in_bin.astype(np.float32)).mean()
            w_n = self.running_sample_num[bv_ind] / (self.running_sample_num[bv_ind] + n_in_bin)
            w_m = n_in_bin / (self.running_sample_num[bv_ind] + n_in_bin)
            self.running_confidence[bv_ind] = self.running_confidence[bv_ind] * w_n + c_in_bin.mean() * w_m
            self.running_accuracy[bv_ind] = self.running_accuracy[bv_ind] * w_n + a_in_bin.mean() * w_m
            self.running_sample_num[bv_ind] += n_in_bin

    def get_current_ece(self, ignore_last=0):
        ece = np.abs(
            self.running_accuracy[:self.n_bins - ignore_last] - self.running_confidence[:self.n_bins - ignore_last]) * \
              self.running_sample_num[:self.n_bins - ignore_last] / self.running_sample_num[
                                                                    :self.n_bins - ignore_last].sum()
        return ece.sum()

    def get_current_calib_curve(self):
        return self.running_accuracy, self.running_confidence


class NLL_Estimator:
    def __init__(self, n_classes=4):
        self.n_classes = n_classes
        self.current_num_samples = 0
        self.current_nll = 0

    def add_sample(self, prob, label, mask=None):
        # create one hot for label
        label_onehot = np.zeros((label.shape[0], self.n_classes, *label.shape[1:]))
        for c in range(self.n_classes):
            label_onehot[:, c, :, :] = (label == c).astype(np.float32)
        nll = - (np.log(np.maximum(prob, 1e-12)) * label_onehot).mean(axis=1)
        if mask is not None:
            nll = nll[mask]
        n_pixels = np.prod(nll.shape)
        nll = nll.mean()
        self.current_nll = self.current_nll * self.current_num_samples + nll * n_pixels
        self.current_nll /= (self.current_num_samples + n_pixels)
        self.current_num_samples += n_pixels


class Brier_Estimator:
    def __init__(self, n_classes=4):
        self.current_num_samples = 0
        self.current_brier = 0
        self.n_classes = n_classes

    def add_sample(self, prob, label, mask=None):
        # create one hot for label
        label_onehot = np.zeros((label.shape[0], self.n_classes, *label.shape[1:]))
        for c in range(self.n_classes):
            label_onehot[:, c, :, :] = (label == c).astype(np.float32)
        brier = np.mean((prob - label_onehot) ** 2, axis=1)
        if mask is not None:
            brier = brier[mask]
        n_pixels = np.prod(brier.shape)
        brier = brier.mean()
        self.current_brier = self.current_brier * self.current_num_samples + brier * n_pixels
        self.current_brier /= (self.current_num_samples + n_pixels)
        self.current_num_samples += n_pixels


def mask_to_bounding_box(foreground, alpha=1.3):
    Dx, Dy, Dz = foreground.shape
    bounding_box = np.zeros_like(foreground)
    where = np.where(foreground)
    min_x, max_x = where[0].min(), where[0].max()
    min_y, max_y = where[1].min(), where[1].max()
    min_z, max_z = where[2].min(), where[2].max()
    span_x, span_y, span_z = max_x - min_x, max_y - min_y, max_z - min_z
    xl, xh = np.round(min_x - (alpha - 1.0) * span_x / 2), np.round(max_x + (alpha - 1.0) * span_x / 2)
    yl, yh = np.round(min_y - (alpha - 1.0) * span_y / 2), np.round(max_y + (alpha - 1.0) * span_y / 2)
    zl, zh = np.round(min_z - (alpha - 1.0) * span_z / 2), np.round(max_z + (alpha - 1.0) * span_z / 2)
    xl, yl, zl = np.maximum([xl, yl, zl], 0).astype(np.int32)
    xh, yh, zh = np.minimum([xh, yh, zh], [Dx - 1, Dy - 1, Dz - 1]).astype(np.int32)
    bounding_box[xl:xh, yl:yh, zl:zh] = 1
    return bounding_box


def compute_entropy(p, sumaxis=0):
    # 4 x W, H
    logp = np.log2(np.maximum(p, 1e-8))
    # log1p = np.log2(np.maximum(1-p, 1e-8))
    ent = -np.sum(p * logp, axis=sumaxis)
    return ent


def compute_binary_entropy(p):
    logp = np.log2(np.maximum(p, 1e-8))
    log_1mp = np.log2(np.maximum(1 - p, 1e-8))
    # log1p = np.log2(np.maximum(1-p, 1e-8))
    ent = -(p * logp + (1.0 - p) * log_1mp)
    return ent


def write_text_on_pil_image(image, text="Hello!", font_size=3, loc=(0, 0), color=(255, 255, 255)):
    font_file = "/home/yidongzhao/fonts/open-sans.ttf"
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype(font_file, font_size)
    draw.text(loc, text, color, font=font)
    return image


def compute_brier_score(p, seg, s=(0, 1, 2, 3)):
    briers = []
    for semantic in s:
        smap = (seg == semantic)
        p_semantic = p[:, semantic, :, :]
        brier = np.sum((p_semantic - smap.astype(np.float32)) ** 2) / smap.sum()
        briers.append(brier)
    return briers


def convert_data_to_correct_shape(x: Union[str, np.ndarray],
                                  properties_dict: dict, order: int = 1,
                                  is_seg: bool = False,
                                  force_separate_z: bool = None,
                                  interpolation_order_z: int = 0) -> np.ndarray:
    """
    Adapted from nnunet.inference.segmentation_export.save_segmentation_nifti_from_softmax.
    """
    # first resample, then put result into bbox of cropping
    current_shape = x.shape
    shape_original_after_cropping = properties_dict.get('size_after_cropping')
    shape_original_before_cropping = properties_dict.get('original_size_of_raw_data')

    # Resample
    if np.any([i != j for i, j in zip(np.array(current_shape[1:]), np.array(shape_original_after_cropping))]):
        if force_separate_z is None:
            if get_do_separate_z(properties_dict.get('original_spacing')):
                do_separate_z = True
                lowres_axis = get_lowres_axis(properties_dict.get('original_spacing'))
            elif get_do_separate_z(properties_dict.get('spacing_after_resampling')):
                do_separate_z = True
                lowres_axis = get_lowres_axis(properties_dict.get('spacing_after_resampling'))
            else:
                do_separate_z = False
                lowres_axis = None
        else:
            do_separate_z = force_separate_z
            if do_separate_z:
                lowres_axis = get_lowres_axis(properties_dict.get('original_spacing'))
            else:
                lowres_axis = None

        if lowres_axis is not None and len(lowres_axis) != 1:
            do_separate_z = False
        x_old_spacing = resample_data_or_seg(x, shape_original_after_cropping,
                                             is_seg=is_seg,
                                             axis=lowres_axis,
                                             order=order,
                                             do_separate_z=do_separate_z,
                                             order_z=interpolation_order_z)
    else:
        x_old_spacing = x

    # Padding
    bbox = properties_dict.get('crop_bbox')
    if bbox is not None:
        x_old_size = np.zeros((x_old_spacing.shape[0], *shape_original_before_cropping))
        for c in range(3):
            bbox[c][1] = np.min((bbox[c][0] + x_old_spacing.shape[c + 1], shape_original_before_cropping[c]))
        x_old_size[:, bbox[0][0]:bbox[0][1], bbox[1][0]:bbox[1][1], bbox[2][0]:bbox[2][1]] = x_old_spacing
    else:
        x_old_size = x_old_spacing
    return x_old_size
