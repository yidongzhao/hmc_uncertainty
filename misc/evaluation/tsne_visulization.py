import numpy as np
import matplotlib

matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt
from sklearn.manifold import TSNE
import sys, os
import utilities as util

plt.rcParams.update({'font.size': 8})
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["xtick.direction"] = "in"
plt.rcParams["ytick.direction"] = "in"
plt.rcParams["svg.fonttype"] = "none"

# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


TERM_RED, TERM_BLUE, TERM_YELLOW, TERM_CYAN, TERM_RESET = "\u001b[31m", \
                                                          "\u001b[34m", \
                                                          "\u001b[33m", \
                                                          "\u001b[36m", \
                                                          "\u001b[0m"

TRAINER_NAME = "nnUNetTrainerV2_cSGHMC"
CONFIG = "mid_lr_m3_sch_lt2"
ALL_CHECKPOINTS = [[332 * (m + 1) - 4 * i for i in range(33)]
                   for m in range(3)]
blockPrint()
trainer = util.restore_trainer(fold=0, network_trainer=TRAINER_NAME,
                               training=False, config=CONFIG)
enablePrint()
WEIGHT_NAMES = [f"conv_blocks_localization.{i}.0.blocks.0.conv.weight" for i in range(6)] + \
               [f"conv_blocks_context.{i}.blocks.0.conv.weight" for i in range(6)]


def compute_cosine_sim(state_dict0, state_dict1):
    inner_product, norm1, norm2 = 0, 0, 0
    for wname, w0 in state_dict0.items():
        w1 = state_dict1[wname]
        inner_product += (w0 * w1).sum().item()
        norm1 += (w0 * w0).sum().item()
        norm2 += (w1 * w1).sum().item()
    return inner_product / np.sqrt(norm1) / np.sqrt(norm2)


def build_weight_vector(checkpoint, weight_names=[]):
    vec = []
    state_dict = checkpoint["state_dict"]
    for wname in weight_names:
        w = state_dict[wname][:10, :10, :, :]
        vec = vec + list(w.detach().cpu().numpy().flatten())
    return vec


# checkpoints = util.get_all_checkpoints(trainer, epochs=ALL_CHECKPOINTS[0],
#                                        epoch_num_width=4, id="ep")
# acc = 0
# sim = []
# for ckpt, _ in checkpoints:
#     if acc == 0:
#         wT = ckpt["state_dict"]
#     wx = ckpt["state_dict"]
#     cos_sim = compute_cosine_sim(wT, wx)
#     sim.append(cos_sim)
#     acc += 1
# print(sim)
#
# checkpoints = util.get_all_checkpoints(trainer, epochs=[e for ac in ALL_CHECKPOINTS for e in reversed(ac[:10:2])],
#                                        epoch_num_width=4, id="ep")
# weights = []
# for ckpt, _ in checkpoints:
#     weights.append(ckpt["state_dict"])
# sim_matrix = np.zeros((len(weights), len(weights)))
# for i in range(len(weights)):
#     for j in range(i, len(weights)):
#         sim = compute_cosine_sim(weights[i], weights[j])
#         sim_matrix[j, i] = sim
#         sim_matrix[i, j] = sim
# np.savez("/home/yidongzhao/barrel/Data/sghmc_results/loss_landscape/sim_matrix.npz", sim_mat=sim_matrix)


N_POINTS_PER_CYCLE = 16
checkpoints = util.get_all_checkpoints(trainer, epochs=[e for ac in ALL_CHECKPOINTS for e in ac[:N_POINTS_PER_CYCLE]],
                                       epoch_num_width=4, id="ep")
weight_all_epochs = []
for ckpt, _ in checkpoints:
    weight_all_epochs.append(build_weight_vector(ckpt, weight_names=WEIGHT_NAMES))
weight_all_epochs = np.array(weight_all_epochs)
print(weight_all_epochs.shape)

tsne = TSNE(n_components=2)
reduced = tsne.fit_transform(weight_all_epochs)
reduced_weights = reduced[:N_POINTS_PER_CYCLE * 3, :]
np.savez("/home/yidongzhao/barrel/Data/sghmc_results/loss_landscape/tsne.npz", tsne=reduced_weights)


colors = np.array([(66, 167, 34, 77),
                   (198, 97, 230, 77),
                   (51, 87, 230, 77)])
colors = colors / 255.0

fig = plt.figure(figsize=(2.0, 2.0))
for i in range(3):
    plt.plot(reduced[i * N_POINTS_PER_CYCLE:(i + 1) * N_POINTS_PER_CYCLE, 0],
             reduced[i * N_POINTS_PER_CYCLE:(i + 1) * N_POINTS_PER_CYCLE, 1], 'o-',
             color=colors[i],
             markersize=3,
             markeredgecolor=[77 / 255, 77 / 255, 77 / 255],
             markerfacecolor=colors[i])
plt.xlabel("weight space, d1")
plt.ylabel("weight space, d2")
# plt.show()
plt.savefig("/home/yidongzhao/barrel/Data/sghmc_results/loss_landscape//t-sne.svg")
plt.close()
