export nnUNet_raw_data_base=/home/yidongzhao/barrel/Data/nnunetdata/raw
export nnUNet_preprocessed=/home/yidongzhao/barrel/Data/nnunetdata/preprocessed
export RESULTS_FOLDER=/home/yidongzhao/umbrella/nnunetdata/results
export nnUNet_config=/home/yidongzhao/tnwbulk/nnunetdata/configs

#python rie_evaluation.py
python cSGHMC_single_evaluation.py --config mid_lr_m3_sch_lt --name lt_multi
python cSGHMC_single_evaluation.py --config mid_lr_m3_sch_lt --name lt_single
python cSGHMC_single_evaluation.py --config mid_lr_m3_sch --name mt_multi
python cSGHMC_single_evaluation.py --config mid_lr_m3_sch --name mt_single
#python cSGHMC_single_evaluation.py --config mid_lr_m3_sc --name sc_single
