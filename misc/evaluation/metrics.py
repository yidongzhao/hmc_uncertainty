import numpy as np


def compute_hard_dice(seg, prediction):
    seg[seg == -1] = 0
    dice_per_class = []
    num_pixels_per_class = []
    num_pixels_per_class_prediction = []
    for c in range(4):
        seg_slice_c_mask = (seg == c)
        prediction_slice_c_mask = (prediction == c)
        intersection = 2 * np.sum(seg_slice_c_mask * prediction_slice_c_mask)
        denominator = seg_slice_c_mask.sum() + prediction_slice_c_mask.sum()
        if denominator > 0:
            dice_c = intersection / denominator
        else:
            # no pixels for this class
            dice_c = -1.0
        dice_per_class.append(dice_c)
        num_pixels_per_class.append(seg_slice_c_mask.sum())
        num_pixels_per_class_prediction.append(prediction_slice_c_mask.sum())
    return dice_per_class, num_pixels_per_class, num_pixels_per_class_prediction


def compute_stratified_brier_score(seg, predicted_probability):
    seg[seg == -1] = 0
    predicted_probability = np.transpose(predicted_probability, (1, 0, 2, 3))
    brier = {c: -1.0 for c in range(4)}
    for c in range(4):
        mask = (seg == c)
        p = predicted_probability[int(c), mask]
        if mask.sum() > 0:
            brier[c] = ((p - 1) ** 2).sum() / mask.sum()
    return brier


def compute_brier_score(seg, predicted_probability):
    seg[seg == -1] = 0
    seg_one_hot = np.zeros((4, *seg.shape))
    for c in range(4):
        seg_one_hot[c, seg == c] = 1

    predicted_probability = np.transpose(predicted_probability, (1, 0, 2, 3))
    brier = ((seg_one_hot[:, seg > 0] - predicted_probability[:, seg > 0]) ** 2).mean()
    return brier
