"""
For single model, we have several variants:
    - Max. softmax
    - Temperature scaling, T in [5, 10, 100]
    - MC-Dropout

Evaluation metrics:
    - DICE
    - Brier
    - IoU(U, E)
"""
import numpy as np
import utilities as util
import evalpaths as evp
import joblib
import sys, os


# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


TERM_RED, TERM_BLUE, TERM_YELLOW, TERM_CYAN, TERM_RESET = "\u001b[31m",\
                                                          "\u001b[34m",\
                                                          "\u001b[33m",\
                                                          "\u001b[36m",\
                                                          "\u001b[0m"

TRAINER_NAME = "nnUNetTrainerV2_cSGHMC"


def build_trainers(num_models=10):
    trainers = [util.restore_trainer(fold=0, network_trainer=TRAINER_NAME,
                                     training=False, config=f"deep_ensemble_{nm:02d}") for nm in range(num_models)]
    return trainers


def traienrs_load_checkpoints(trainers):
    for t in trainers:
        t.load_latest_checkpoint(train=False)
        util.enable_test_time_dropout_trainer(t)


def run_inference_trajectory(num_models=1, run_id=0,
                             split="val", variant="original", save_seg=False, name="single",
                             scaling_temp=(1, 2)):
    if split == "val":
        valid_base = evp.acdc_validation_base / variant / "images"
        all_volume_paths = [r for r in valid_base.glob("*.nii.gz")]
        seg_base = evp.acdc_validation_base / variant / "labels"
        resampled_seg_base = evp.acdc_prediction_base / "ResampledSeg"
        prediction_base = evp.acdc_prediction_base

    else:
        all_volume_paths = evp.mm_raw_data_path.iterdir()
        seg_base = evp.mm_seg_data_path
        resampled_seg_base = evp.mm_prediction_base / "ResampledSeg"
        prediction_base = evp.mm_prediction_base

    directory = prediction_base / f"RIE_{name}" / f"RIE_{name}_{variant}" / f"Model_{num_models:02d}_Run_{run_id:01d}"
    directory.mkdir(parents=True, exist_ok=True)

    num_image = 1
    for volume in all_volume_paths:
        if not volume.parts[-1].endswith(".nii.gz"):
            continue
        patient_frame_code = volume.parts[-1].split('.')[0]
        if patient_frame_code.endswith("_0000"):
            patient_frame_code = patient_frame_code[:-5]
        print(f"\r\t {patient_frame_code:40s} [{num_image:3d}]", end="", flush=True)
        image, seg = util.load_nib_image_for_prediction(trainers[0], volume,
                                                        seg_base / f"{patient_frame_code}.nii.gz")
        predicted_probability = util.multiple_trainer_predict_multi_temp(trainers, image, scaling_temp=scaling_temp)
        predicted_probability = {f"temp={scaling_temp[i]:.2f}": predicted_probability[i] for i in
                                 range(len(scaling_temp))}
        file_name = f"{patient_frame_code}"
        np.savez_compressed(directory / file_name, **predicted_probability)
        if save_seg:
            np.savez_compressed(resampled_seg_base / f"{patient_frame_code}", label=seg)
        num_image += 1
    print("\t [------] Done!")


if __name__ == '__main__':
    blockPrint()
    max_trainers = build_trainers(num_models=15)
    enablePrint()

    # for num_models in [1, 3, 9, 12, 15]:
    #     for run_id in range(1):
    #         chosen = list(np.random.choice(list(range(15)), size=num_models, replace=False))
    #         trainers = [max_trainers[_] for _ in chosen]
    #         print(TERM_RED + f"Loading trainers for {num_models} models, ID: {run_id}" + TERM_RESET)
    #         print(TERM_YELLOW, "INST IDs:", TERM_RESET, chosen)
    #         blockPrint()
    #         traienrs_load_checkpoints(trainers)
    #         enablePrint()
    #         run_inference_trajectory(num_models=num_models, run_id=run_id,
    #                                  name="default", variant="original")
    trainers = max_trainers
    blockPrint()
    traienrs_load_checkpoints(trainers)
    enablePrint()
    # multi model
    # run_inference_trajectory(num_models=15, run_id=0, split="test", name="default_15")

    # single model
    trainers = max_trainers[6:7]
    run_inference_trajectory(num_models=1, run_id=0, split="test", name="default_1")

    # print(f"================ Original ======================")
    # run_inference_trajectory(num_models=NUM_MODELS, run_id=0, split="val", variant="original", name=NAME)
    # for skew in ["noise", "blur", "biasfield", "motion"]:
    #     for skew_intensity in range(5):
    #         print(f"================ {skew}_{skew_intensity:02d} ======================")
    #         run_inference_trajectory(num_models=NUM_MODELS, run_id=0, variant=f"{skew}_{skew_intensity:02d}",
    #                                  name=NAME)
