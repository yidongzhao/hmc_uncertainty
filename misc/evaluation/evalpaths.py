from pathlib import Path
mm_raw_data_path = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/imagesTr")
mm_seg_data_path = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/labelsTr")
acdc_raw_data_path = Path("/home/yidongzhao/barrel/Data/nnunetdata/raw/nnUNet_raw_data/Task027_ACDC/imagesTr")
acdc_seg_data_path = Path("/home/yidongzhao/barrel/Data/nnunetdata/raw/nnUNet_raw_data/Task027_ACDC/labelsTr")
mm_prediction_base = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/EvaluationData/MM_Predictions")
acdc_prediction_base = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/EvaluationData/ACDC_Predictions")
swag_weights_base = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/EvaluationData/SWAG_Weights")

acdc_validation_base = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/EvaluationData/ACDC_ValidationSet")
