"""
For single model, we have several variants:
    - Max. softmax
    - Temperature scaling, T in [5, 10, 100]
    - MC-Dropout

Evaluation metrics:
    - DICE
    - Brier
    - IoU(U, E)
"""
import numpy as np
import torch

import utilities as util
import sys, os
import argparse
import tqdm
from pathlib import Path

parser = argparse.ArgumentParser(description="Generate predictions")
parser.add_argument("--config", type=str)
parser.add_argument("--name", type=str)
args = parser.parse_args()


# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


TERM_RED, TERM_BLUE, TERM_YELLOW, TERM_CYAN, TERM_RESET = "\u001b[31m", \
                                                          "\u001b[34m", \
                                                          "\u001b[33m", \
                                                          "\u001b[36m", \
                                                          "\u001b[0m"

TRAINER_NAME = "nnUNetTrainerV2_dropout"
CONFIG = "middle_dropout_0.5"
NAME = "dropout"
trainer = util.restore_trainer(fold=0, network_trainer=TRAINER_NAME,
                               training=False, config=CONFIG)
NUM_MODELS = 15
trainer.load_final_checkpoint()
util.enable_test_time_dropout_trainer(trainer)


def compute_averaged_dice(x, y, num_classes=4):
    aver_dice = 0
    aver_dice_denom = 0
    for c in range(1, num_classes):
        xc = (x == c)
        yc = (y == c)
        if xc.sum() + yc.sum() > 0:
            aver_dice_denom += 1
            dice = 2 * (xc * yc).sum() / (xc.sum() + yc.sum())
            aver_dice += dice
    return aver_dice / aver_dice_denom


def validate_diversity(_trainer, list_of_image_file, list_of_gt_file, num_classes=4):
    confusion_matrix = np.zeros((NUM_MODELS + 1, NUM_MODELS + 1))
    aver_dice = [0 for _ in range(NUM_MODELS)]
    accumulate_dice = [0 for _ in range(NUM_MODELS)]
    for image_path, gt_path in tqdm.tqdm(zip(list_of_image_file, list_of_gt_file)):
        # load images and do padding, resizing
        image, gt, slicer, properties = util.load_nib_image_for_prediction(_trainer,
                                                                           image_path,
                                                                           gt_path,
                                                                           return_slicer=True)
        gt[gt < 0] = 0
        gt = gt[:, 0, :, :]
        prob = util.dropout_model_prediction(_trainer, n_forwards=NUM_MODELS, scaling_temp=(1.,),
                                             image_to_predict=image, return_indiv=True)
        prob = prob[:, 0, :, :, :, :]
        average_prob = prob.mean(axis=0)

        pred_masks = prob.argmax(axis=2)    # individual predictive masks, [NE, SL, H, W]
        average_pred_mask = average_prob.argmax(1)  # averaged predictive mask, [SL, H, W]

        for ens in range(pred_masks.shape[0]):
            aver_dice[ens] += compute_averaged_dice(pred_masks[ens], gt, num_classes=num_classes) / len(list_of_image_file)
            prob_t = prob[:ens + 1, :, :, :, :].mean(axis=0)
            mask_t = prob_t.argmax(axis=1)
            dice_t = compute_averaged_dice(mask_t, gt, num_classes=num_classes) / len(list_of_image_file)
            accumulate_dice[ens] += dice_t

        # find error area
        error_area = (average_pred_mask != gt)
        # chosen_area = average_prob.max(axis=1) < 0.999
        # error_area = chosen_area
        for i in range(pred_masks.shape[0]):
            for j in range(i, pred_masks.shape[0]):
                pred1 = pred_masks[i, error_area]
                pred2 = pred_masks[j, error_area]
                dice = compute_averaged_dice(pred1, pred2, num_classes=num_classes)
                confusion_matrix[i, j] += dice
                confusion_matrix[j, i] += dice
            pred_aver = average_pred_mask[error_area]
            dice = compute_averaged_dice(pred1, pred_aver, num_classes=num_classes)
            confusion_matrix[i, -1] += dice
            confusion_matrix[-1, i] += dice
    confusion_matrix /= len(list_of_image_file)

    return {"confusion": confusion_matrix, "aver_dice": np.array(aver_dice), "accmul_dice": np.array(accumulate_dice)}


list_of_image_file, list_of_gt_file = [], []
validation_base = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/EvaluationData/ACDC_ValidationSet")
variants = ["original"]

for v in variants:
    variant_base = validation_base / v
    for fname in (variant_base / "images").glob("*.nii.gz"):
        list_of_image_file.append(fname)
        list_of_gt_file.append(variant_base / "labels" / fname.parts[-1])

# trainers = build_trainers(num_models=30)
# epochs = ALL_CHECKPOINTS[0][::3] + ALL_CHECKPOINTS[1][::3] + ALL_CHECKPOINTS[2][::3]
# trainers_load_checkpoints(trainers, epochs_chosen=epochs)

# DROPOUT
res = validate_diversity(trainer, list_of_image_file, list_of_gt_file)
np.savez_compressed(f"/home/yidongzhao/barrel/Data/sghmc_results/diversity/dropout.npz", **res)

