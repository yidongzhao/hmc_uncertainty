"""
For single model, we have several variants:
    - Max. softmax
    - Temperature scaling, T in [5, 10, 100]
    - MC-Dropout

Evaluation metrics:
    - DICE
    - Brier
    - IoU(U, E)
"""
import numpy as np
import metrics
import utilities as util
import evalpaths as evp
import pickle

N_ENSEMBLES, END_EPOCH, EPOCH_STEP = 30, 2000, 2
FOLD = 0
CONFIG = "Snapshot"

TRAINER_CONFIG_NAME = {"Single_TE": "nnUNetTrainerV2RIE_TE",
                       "Single": "nnUNetTrainerV2RIE",
                       "Snapshot": "nnUNetTrainerV2RIE_Snapshot",
                       "SnapshotV3": "nnUNetTrainerV2RIE_SnapshotV3"}
TRAINER_NAME = TRAINER_CONFIG_NAME[CONFIG]


def run_inference_trajectory(split="val", save_seg=False):
    # load trainer
    trainers = [util.restore_trainer(fold=FOLD, network_trainer=TRAINER_NAME,
                                     training=(split == "val")) for _ in range(N_ENSEMBLES)]
    epochs_chosen = list(range(END_EPOCH, END_EPOCH - N_ENSEMBLES * EPOCH_STEP, -EPOCH_STEP))
    checkpoints = util.get_all_checkpoints(trainers[0], epochs=epochs_chosen)
    for t in trainers:
        checkpoint, checkpoint_path = next(checkpoints)
        t.load_checkpoint_ram(checkpoint, train=False)
        util.enable_test_time_dropout_trainer(t)

    if split == "val":
        all_volume_codes = trainers[0].dataset_val.keys()
        all_volume_paths = [evp.acdc_raw_data_path / f"{v}_0000.nii.gz" for v in all_volume_codes]
        seg_base = evp.acdc_seg_data_path
        resampled_seg_base = evp.acdc_prediction_base / "ResampledSeg"
        prediction_base = evp.acdc_prediction_base

    else:
        all_volume_paths = evp.mm_raw_data_path.iterdir()
        seg_base = evp.mm_seg_data_path
        resampled_seg_base = evp.mm_prediction_base / "ResampledSeg"
        prediction_base = evp.mm_prediction_base

    directory = prediction_base / f"{CONFIG}_{END_EPOCH}_{EPOCH_STEP}_{N_ENSEMBLES}"
    directory.mkdir(parents=True, exist_ok=True)

    num_image = 1
    for volume in all_volume_paths:
        if not volume.parts[-1].endswith(".nii.gz"):
            continue
        patient_frame_code = volume.parts[-1].split('.')[0][:-5]
        print(f"\u001b[32m>>>{patient_frame_code} [{num_image}/300]<<<\u001b[0m")
        image, seg = util.load_nib_image_for_prediction(trainers[0], volume,
                                                        seg_base / f"{patient_frame_code}.nii.gz")
        predicted_probability = util.multi_trainer_predictions(trainers, image, scaling_temp=(1,))

        file_name = f"{patient_frame_code}"
        with open(directory / file_name, "wb") as stream:
            pickle.dump(predicted_probability, stream)
        if save_seg:
            np.savez(resampled_seg_base / f"{patient_frame_code}", label=seg)
        num_image += 1


if __name__ == '__main__':
    run_inference_trajectory(split="test")





