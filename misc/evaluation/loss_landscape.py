import copy

import joblib
import torch
import misc.evaluation.utilities as util
import numpy as np
import time
import misc.evaluation.evalpaths as evp
from nnunet.training.loss_functions.dice_loss import DC_and_CE_loss
import tqdm
from sklearn.manifold import TSNE
import joblib


def state_dict_dot(w1, w2):
    # <w1, w2>
    dot = 0.
    for w_name in w1.keys():
        dot += torch.sum(w1[w_name] * w2[w_name])
    return dot


def state_dict_minus(w1, w2):
    # w1 - w2, note: w1 - w2 =/= w2 - w1 !!
    d = copy.deepcopy(w1)
    for w_name in d.keys():
        d[w_name] = w1[w_name] - w2[w_name]
    return d


def state_dict_add(w1, e1, e2, x, y):
    # w1 + w2
    s = copy.deepcopy(w1)
    for w_name in s.keys():
        s[w_name] = w1[w_name] + x * e1[w_name] + y * e2[w_name]
    return s


def state_dict_interpolation(e1, e2, a, b):
    # w1 + w2
    s = copy.deepcopy(e1)
    for w_name in s.keys():
        s[w_name] = a * e1[w_name] + b * e2[w_name]
    return s


def create_orthonomal_basis(w1, w2, w3):
    d1 = state_dict_minus(w2, w1)
    d2 = state_dict_minus(w3, w1)

    # Gram-Schmidt Orthogonal.
    d1_norm_l2 = state_dict_dot(d1, d1)
    d1_dot_d2 = state_dict_dot(d1, d2)
    for w_name in d2.keys():
        d2[w_name] = d2[w_name] - d1[w_name] * d1_dot_d2 / d1_norm_l2
    d2_norm_l2 = state_dict_dot(d2, d2)
    for w_name in d1.keys():
        d1[w_name] /= torch.sqrt(d1_norm_l2)
        d2[w_name] /= torch.sqrt(d2_norm_l2)
    return d1, d2


def get_coordinate(e1, e2, d):
    x = state_dict_dot(e1, d)
    y = state_dict_dot(e2, d)
    return x, y


def convert_vec_to_state_dict(vec, state_dict):
    pointer = 0
    for p_name in state_dict.keys():
        param = state_dict[p_name]
        param_dim = param.numel()
        param = torch.Tensor(vec[pointer: pointer + param_dim]).view(param.shape)
        state_dict[p_name] = param
        pointer += param_dim
    return state_dict


def get_loss(_trainer, split="train"):
    _trainer.network.eval()
    if split == "train":
        all_volume_codes = _trainer.dataset_tr.keys()
    else:
        all_volume_codes = _trainer.dataset_val.keys()
    all_volume_paths = [evp.acdc_raw_data_path / f"{v}_0000.nii.gz" for v in all_volume_codes]
    seg_base = evp.acdc_seg_data_path
    loss = DC_and_CE_loss({'batch_dice': _trainer.batch_dice, 'smooth': 1e-5, 'do_bg': False},
                          {})
    val_losses = []
    for volume in all_volume_paths:
        if not volume.parts[-1].endswith(".nii.gz"):
            continue
        patient_frame_code = volume.parts[-1].split('.')[0][:-5]
        image, seg = util.load_nib_image_for_prediction(_trainer, volume,
                                                        seg_base / f"{patient_frame_code}.nii.gz")
        image = torch.from_numpy(image).cuda().float()
        seg = torch.from_numpy(seg).cuda().float()
        seg[seg < 0] = 0
        output = trainer.network(image)[0]
        vol_val_loss = loss(output, seg)
        val_losses.append(vol_val_loss.detach().cpu().numpy())
    return np.mean(val_losses)


# build trainers
trainer = util.restore_trainer(fold=0, network_trainer="nnUNetTrainerV2_cSGHMC", training=True,
                               config="mid_lr_m3_sch_lt")
basis_epochs = [332, 664, 996]
basis_checkpoints = list(util.get_all_checkpoints(trainer, epochs=basis_epochs, epoch_num_width=4))
basis_weights = [bc[0]["state_dict"] for bc in basis_checkpoints]
w0 = basis_weights[0]

# uni-modal landscape
[center, svd] = joblib.load("/mnt/barrel/Data/sghmc_results/loss_landscape/unimodal_2d/mid_lr_m3_sch_lt.svd")
center = list(center)
v1 = list(svd.components_[0, :])
v2 = list(svd.components_[1, :])
v3 = list(svd.components_[2, :])
dummy_checkpoint = copy.deepcopy(w0)
center_ckpt = convert_vec_to_state_dict(center, dummy_checkpoint)
dummy_checkpoint = copy.deepcopy(w0)
v1_ckpt = convert_vec_to_state_dict(v1, dummy_checkpoint)
dummy_checkpoint = copy.deepcopy(w0)
v2_ckpt = convert_vec_to_state_dict(v2, dummy_checkpoint)
dummy_checkpoint = copy.deepcopy(w0)
v3_ckpt = convert_vec_to_state_dict(v3, dummy_checkpoint)


def get_loss_2d(w0, u, v, alpha, beta, split="validation"):
    dummy_checkpoint = copy.deepcopy(basis_checkpoints[0][0])
    dummy_checkpoint["state_dict"] = state_dict_add(w0, u, v, alpha, beta)
    trainer.load_checkpoint_ram(dummy_checkpoint)
    val_loss = get_loss(trainer, split=split)
    del dummy_checkpoint
    return val_loss


# get training trajectory, uni-modal
# epochs = list(range(868, 996, 4))
# checkpoints = util.get_all_checkpoints(trainer, epochs=epochs, epoch_num_width=4)
# coordinates = []
# for ckpt in tqdm.tqdm(checkpoints):
#     x, y = get_coordinate(v2_ckpt, v3_ckpt, state_dict_minus(ckpt[0]["state_dict"], center_ckpt))
#     coordinates.append([x, y])
# coordinates = np.array(coordinates)
# np.savez("/mnt/barrel/Data/sghmc_results/loss_landscape/unimodal_2d/trajectory.npz", epochs=np.array(epochs),
#          trajectory=coordinates)

# uni-modal loss landscape
ALPHA_VAL = np.arange(-8, 9) * 10
BETA_VAL = np.arange(-8, 9) * 10
n_mesh_pts_a, n_mesh_pts_b = ALPHA_VAL.shape[0], BETA_VAL.shape[0]
alpha_mesh, beta_mesh = np.meshgrid(ALPHA_VAL, BETA_VAL)
data_loss_mesh = np.zeros((n_mesh_pts_a, n_mesh_pts_b))
for i in range(n_mesh_pts_a):
    for j in tqdm.tqdm(range(n_mesh_pts_b)):
        val_loss = get_loss_2d(center_ckpt, v2_ckpt, v3_ckpt, alpha_mesh[i, j], beta_mesh[i, j])
        data_loss_mesh[i, j] = val_loss
    print(f"Completed: [{i+1:2d}/{n_mesh_pts_a:2d}]")
np.savez("/mnt/barrel/Data/sghmc_results/loss_landscape/unimodal_2d/local_landscape.npz",
         alpha_mesh=alpha_mesh,
         beta_mesh=beta_mesh,
         data_loss=data_loss_mesh)



# build orthogonal bases
u, v = create_orthonomal_basis(*basis_weights[:3])



# ALPHA_VAL = np.arange(-5, 35) * 11.5
# BETA_VAL = np.arange(-5, 35) * 10.0
# n_mesh_pts_a, n_mesh_pts_b = ALPHA_VAL.shape[0], BETA_VAL.shape[0]
# alpha_mesh, beta_mesh = np.meshgrid(ALPHA_VAL, BETA_VAL)
# data_loss_mesh = np.zeros((n_mesh_pts_a, n_mesh_pts_b))
# for i in range(n_mesh_pts_a):
#     for j in tqdm.tqdm(range(n_mesh_pts_b)):
#         val_loss = get_loss_2d(w0, u, v, alpha_mesh[i, j], beta_mesh[i, j])
#         data_loss_mesh[i, j] = val_loss
#     print(f"Completed: [{i+1:2d}/{n_mesh_pts_a:2d}]")
# np.savez("/mnt/barrel/Data/sghmc_results/loss_landscape/modes_2d/three_modes.npz",
#          alpha_mesh=alpha_mesh,
#          beta_mesh=beta_mesh,
#          data_loss=data_loss_mesh)

# interpolation
# lambdas = np.linspace(0.0, 1.0, num=30)
# val_losses = []
# for lbd in tqdm.tqdm(lambdas):
#     w = state_dict_interpolation(basis_weights[1], basis_weights[2], lbd, 1.-lbd)
#     dummy_checkpoint = copy.deepcopy(basis_checkpoints[0][0])
#     dummy_checkpoint["state_dict"] = w
#     trainer.load_checkpoint_ram(dummy_checkpoint)
#     val_loss = get_loss(trainer)
#     val_losses.append(val_loss)
# np.savez("/mnt/barrel/Data/sghmc_results/losslandscape/interpolation/cosine_anneal_01",
#          lam=lambdas, loss=np.array(val_losses))
# print("Finished!")
