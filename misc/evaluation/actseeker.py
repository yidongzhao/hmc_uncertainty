import torch.nn as nn


class ActivationSeeker:
    def __init__(self):
        self.activation = {}
        self.handles = []

    def get_activation(self, name):
        def hook(model, input, output):
            self.activation[name] = output
        return hook

    def attach_hooks(self, model, hook_name_paths_dict):
        for param_name, param_path in hook_name_paths_dict.items():
            child_path = param_path
            child, child_path = get_module_recursive(model, param_path)
            handle = child.register_forward_hook(self.get_activation(param_name))
            self.handles.append(handle)

    def get_data_activations(self, model=None, inputs=None):
        if model is not None and inputs is not None:
            model(inputs)
        activation_dict = dict(self.activation)
        return activation_dict

    def get_dl_activations(self, agent, dl):
        ix = 0
        dl_activations = []
        for data in dl:
            dl_activations.append(self.get_data_activations(agent, data))
            ix += 1
            if ix ==2:
                break
        return dl_activations

    def remove_handles(self):
        while len(self.handles) > 0:
            handle = self.handles.pop()
            handle.remove()


class GradActivationSeeker(ActivationSeeker):
    def __init__(self):
        super().__init__()
        self.grads = {}
        self.grad_handles = []

    def get_grad(self, name):
        def hook(grad):
            self.grads[name] = grad
        return hook

    def get_activation(self, name):
        def hook(model, input, output):
            self.activation[name] = output
            handle = output.register_hook(self.get_grad(name))
            self.grad_handles.append(handle)
        return hook

    def attach_hooks(self, model, hook_name_paths_dict):
        for param_name, param_path in hook_name_paths_dict.items():
            child, child_path = get_module_recursive(model, param_path)
            handle = child.register_forward_hook(self.get_activation(param_name))
            self.handles.append(handle)

    def remove_handles(self):
        while len(self.handles) > 0:
            handle = self.handles.pop()
            handle.remove()
        while len(self.grad_handles) > 0:
            handle = self.grad_handles.pop()
            handle.remove()


def get_module_recursive(parent_module, child_path):
    module = parent_module
    new_child_path = []
    for module_name in child_path.split('.'):
        child_module = getattr(module, module_name)
        if isinstance(child_module, nn.Module):
            module = child_module
            new_child_path.append(module_name)
        else:
            break
    return module, '.'.join(new_child_path)
