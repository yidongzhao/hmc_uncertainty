import numpy as np
import pickle
import utilities as util
import torch
import time
from pathlib import Path
from nnunet.inference.segmentation_export import save_segmentation_nifti, get_segmentation_old_size
from skimage.transform import resize
from torch.nn.functional import interpolate as torch_interp


TRAINER_NAME = "PHiSeg_trainer"
CONFIG = "phiseg"
NAME = "phiseg"
trainer = util.restore_trainer(fold=0, network_trainer=TRAINER_NAME,
                               training=False, config=CONFIG)
NUM_MODELS = 30
trainer.load_latest_checkpoint(train=False)
trainer.prior.eval()
trainer.likelihood.eval()


def run_inference_trajectory(image_dir: Path, seg_save_base: Path, save_uncertainty=False,
                             save_indiv=False):
    # inference on images
    all_volume_paths = image_dir.glob('*.nii.gz')
    for num_image, volume in enumerate(all_volume_paths):
        patient_frame_code = volume.parts[-1].split('.')[0][:-5]
        # if not(patient_frame_code.startswith("Subject_38") or patient_frame_code.startswith("Subject_40")):
        #     continue
        print(f"\u001b[32m>>>{patient_frame_code} [{num_image}]<<<\u001b[0m")
        image, _, slicer, properties = util.load_nib_image_for_prediction(trainer, volume, return_slicer=True,
                                                                          divisible_by=[2 ** trainer.resolution_levels]*2)
        # network forward
        predicted_probability = util.PHiSeg_model_prediction(trainer, image, n_forwards=30,
                                                             scaling_temp=(1, ), return_indiv=True)[:, 0, ...]

        # clipping padded areas
        predicted_probability = predicted_probability[:, slicer[1], :, slicer[2], slicer[3]]  # (#samples, b, 4, h', w')

        # resize predicted probability
        predicted_probability = torch.from_numpy(predicted_probability)
        target_size = properties["size_after_cropping"][1:]  # [h, w]
        predicted_probability = torch_interp(predicted_probability,
                                             (4, *target_size))
        predicted_probability = predicted_probability.detach().cpu().numpy()  # (#samples, b, 4, h, w)
        renorm = predicted_probability.sum(axis=2, keepdims=True)  # (#samples, b, 1, h, w)
        predicted_probability = predicted_probability / renorm

        # crop box
        cbox = properties['crop_bbox']
        original_size = properties['original_size_of_raw_data']

        predicted_probability_original = np.zeros((predicted_probability.shape[0],
                                                   original_size[0], 4,
                                                   original_size[1], original_size[2]))
        predicted_probability_original[:, cbox[0][0]:cbox[0][1], :, cbox[1][0]:cbox[1][1],
        cbox[2][0]:cbox[2][1]] = predicted_probability
        predicted_probability = predicted_probability_original

        # individual masks
        if save_indiv:
            indiv_masks = predicted_probability.argmax(axis=2)  # (#samples, b, h, w)
            indiv_masks = np.transpose(indiv_masks, (2, 3, 1, 0))  # (h, w, b, #samples)
            indiv_save_base = seg_save_base / "indiv" / f"{volume.parts[-1][:-7]}.npz"
            np.savez_compressed(indiv_save_base, masks=indiv_masks.astype(np.uint8))

        # Bayesian model averaging, mean p and do rescale
        predicted_probability = predicted_probability.mean(axis=0)  # (b, 4, h, w)
        dest_prob_file_name = seg_save_base / "prob" / f"{volume.parts[-1][:-7]}.npz"
        np.savez(dest_prob_file_name,
                 prob=predicted_probability)

        if save_uncertainty:
            dest_uncert_file_name = seg_save_base / "uncert" / f"{volume.parts[-1][:-7]}.npz"
            np.savez(dest_uncert_file_name, ent=util.compute_entropy(predicted_probability, sumaxis=1))

        # save mean segmentation map
        dest_seg_file_name = seg_save_base / "seg" / f"{volume.parts[-1][:-7]}.nii.gz"
        save_segmentation_nifti(predicted_probability.argmax(1), dest_seg_file_name.as_posix(), properties)
        print(f"\u001b[32m>>>{patient_frame_code} [{num_image}] done! <<<\u001b[0m")


if __name__ == '__main__':
    SOURCE = Path("/mnt/barrel/Data/nnunetdata/raw/nnUNet_raw_data/Task027_ACDC/imagesTs")
    seg_save_base = Path(f"/home/yidongzhao/umbrella/experiments_data/test/hmc_uncertainty/ACDC_test/{NAME}")
    s, p, u, indiv = seg_save_base / "seg", seg_save_base / "prob", \
                     seg_save_base / "uncert", seg_save_base / "indiv"
    s.mkdir(exist_ok=True, parents=True)
    p.mkdir(exist_ok=True, parents=True)
    u.mkdir(exist_ok=True, parents=True)
    indiv.mkdir(exist_ok=True, parents=True)
    run_inference_trajectory(image_dir=SOURCE,
                             seg_save_base=seg_save_base,
                             save_indiv=True,
                             save_uncertainty=False)
