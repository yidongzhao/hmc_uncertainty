import numpy as np
import pickle
import utilities as util
import torch
import time
from pathlib import Path
from nnunet.inference.segmentation_export import save_segmentation_nifti, get_segmentation_old_size
from skimage.transform import resize
import os
import sys
from torch.nn.functional import interpolate as torch_interp

stdout = sys.stdout

CHECKPOINTS_PATH_BASE = Path("/home/yidongzhao/umbrella/nnunetdata/results/nnUNet/2d/Task027_ACDC"
                             "/nnUNetTrainerV2_cSGHMC__nnUNetPlansv2.1/mid_lr_m3_sch_lt2")
NAME = "cSGHMC_multi"
# EPOCHS = [332 * j - i * 4 for i in range(30) for j in range(3, 4)]
EPOCHS = [572, 324, 552, 312, 256, 304, 656, 912, 992, 228,
          636, 604, 320, 252, 284, 908, 888, 236, 616, 620,
          600, 868, 244, 972, 652, 664, 904, 584, 300, 936]

ALL_CHECKPOINTS_TO_LOAD = [CHECKPOINTS_PATH_BASE / "fold_0" / f"model_ep_{e:04d}.model" for e in EPOCHS]
ALL_CHECKPOINTS_TO_LOAD = [CHECKPOINTS_PATH_BASE / f"fold_0" / f"model_ep_{e:04d}.model"
                           for e in EPOCHS]


def run_inference_trajectory(image_dir: Path, seg_save_base: Path,
                             save_indiv: bool=False,
                             save_uncertainty: bool = False):
    # load checkpoint
    print("\u001b[31m"
          "Start loading checkpoints"
          "\u001b[0m")
    ts = time.time()

    f = open(os.devnull, "w")
    sys.stdout = f
    trainers = [util.restore_trainer(fold=0, network_trainer="nnUNetTrainerV2_cSGHMC",
                                     config="mid_lr_m3_sc")
                for _ in ALL_CHECKPOINTS_TO_LOAD]
    checkpoints = (torch.load(ckpt) for ckpt in ALL_CHECKPOINTS_TO_LOAD)
    for t in trainers:
        checkpoint = next(checkpoints)
        t.load_checkpoint_ram(checkpoint, train=False)
        t.network.eval()

    sys.stdout = stdout
    te = time.time()
    print(f"Checkpoints loading done! Elapsed: {te - ts:.3f} [s]")

    # inference on images
    all_volume_paths = list(image_dir.glob('*.nii.gz'))
    for num_image, volume in enumerate(all_volume_paths):
        patient_frame_code = volume.stem
        print(f"\u001b[34m{patient_frame_code} [{num_image + 1} / {len(all_volume_paths)}]\u001b[0m",
              end="\n", flush=True)
        image, _, slicer, properties = util.load_nib_image_for_prediction(trainers[0], volume,
                                                                          return_slicer=True)
        ts = time.time()
        # network forward
        predicted_probability = util.multiple_trainer_predict(trainers, image,
                                                              return_indiv=True)

        # clipping padded areas
        predicted_probability = predicted_probability[:, slicer[1], :, slicer[2], slicer[3]]  # (#samples, b, 4, h', w')

        # resize predicted probability
        predicted_probability = torch.from_numpy(predicted_probability)
        target_size = properties["size_after_cropping"][1:]                                    # [h, w]
        predicted_probability = torch_interp(predicted_probability,
                                             (4, *target_size), )
        predicted_probability = predicted_probability.detach().cpu().numpy()                        # (#samples, b, 4, h, w)
        renorm = predicted_probability.sum(axis=2, keepdims=True)                                   # (#samples, b, 1, h, w)
        predicted_probability = predicted_probability / renorm

        # crop box
        cbox = properties['crop_bbox']
        original_size = properties['original_size_of_raw_data']

        predicted_probability_original = np.zeros((predicted_probability.shape[0],
                                                   original_size[0], 4,
                                                   original_size[1], original_size[2]))
        predicted_probability_original[:, cbox[0][0]:cbox[0][1], :, cbox[1][0]:cbox[1][1],  cbox[2][0]:cbox[2][1]] = predicted_probability
        predicted_probability = predicted_probability_original
        te = time.time()

        # individual masks
        if save_indiv:
            indiv_masks = predicted_probability.argmax(axis=2)                                  # (#samples, b, h, w)
            indiv_masks = np.transpose(indiv_masks, (2, 3, 1, 0))                               # (h, w, b, #samples)
            indiv_save_base = seg_save_base / "indiv" / f"{volume.parts[-1][:-7]}.npz"
            np.savez_compressed(indiv_save_base, masks=indiv_masks.astype(np.uint8))

        # Bayesian model averaging, mean p and do rescale
        predicted_probability = predicted_probability.mean(axis=0)                              # (b, 4, h, w)
        dest_prob_file_name = seg_save_base / "prob" / f"{volume.parts[-1][:-7]}.npz"
        np.savez(dest_prob_file_name,
                 prob=predicted_probability)

        if save_uncertainty:
            dest_uncert_file_name = seg_save_base / "uncert" / f"{volume.parts[-1][:-7]}.npz"
            np.savez(dest_uncert_file_name, ent=util.compute_entropy(predicted_probability, sumaxis=1))

        # save mean segmentation map
        dest_seg_file_name = seg_save_base / "seg" / f"{volume.parts[-1][:-7]}.nii.gz"
        save_segmentation_nifti(predicted_probability.argmax(1), dest_seg_file_name.as_posix(), properties)
        print(f"\u001b[32m>>>{patient_frame_code} [{num_image}] done! {te-ts:.3f}<<<\u001b[0m")


if __name__ == '__main__':
    SOURCE = Path("/mnt/barrel/Data/nnunetdata/raw/nnUNet_raw_data/Task027_ACDC/imagesTs")
    seg_save_base = Path(f"/home/yidongzhao/umbrella/experiments_data/test/hmc_uncertainty/ACDC_test/{NAME}")

    s, p, u, indiv = seg_save_base / "seg", seg_save_base / "prob", \
                     seg_save_base / "uncert", seg_save_base / "indiv"
    s.mkdir(exist_ok=True, parents=True)
    p.mkdir(exist_ok=True, parents=True)
    u.mkdir(exist_ok=True, parents=True)
    indiv.mkdir(exist_ok=True, parents=True)
    run_inference_trajectory(image_dir=SOURCE,
                             seg_save_base=seg_save_base,
                             save_indiv=True,
                             save_uncertainty=True)
