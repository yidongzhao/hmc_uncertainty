import torch
from torch import Tensor
from typing import List, Optional
from torch.optim.optimizer import Optimizer, required


class SymEulerSGMCMC(Optimizer):
    def __init__(self, params, lr=required, momentum=0, dampening=0,
                 weight_decay=0, nesterov=False, temperature=1, dataset_size=1000, steps_per_epoch=300,
                 estimate_mass_steps=32):
        if lr is not required and lr < 0.0:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if momentum < 0.0:
            raise ValueError("Invalid momentum value: {}".format(momentum))
        if weight_decay < 0.0:
            raise ValueError("Invalid weight_decay value: {}".format(weight_decay))

        defaults = dict(lr=lr, momentum=momentum, dampening=dampening,
                        weight_decay=weight_decay, nesterov=nesterov)
        if nesterov and (momentum <= 0 or dampening != 0):
            raise ValueError("Nesterov momentum requires a momentum and zero dampening")
        super(SymEulerSGMCMC, self).__init__(params, defaults)

        self.temperature = temperature
        self.dataset_size = dataset_size
        self.mass_scale = None
        self.step_counter = 0
        self.steps_per_epoch = steps_per_epoch
        self.estimate_mass_steps = estimate_mass_steps
        self.inject_noise = False

    def __setstate__(self, state):
        super(SymEulerSGMCMC, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('nesterov', False)

    @torch.no_grad()
    def accumulate_mass(self):
        for group in self.param_groups:
            mass_buffer_list = []
            params_with_grad = []
            for p in group['params']:
                if p.grad is not None:
                    params_with_grad.append(p)
                    state = self.state[p]
                    grad_2 = torch.clone(p.grad).detach() ** 2
                    if 'mass_buffer' not in state:
                        mass_buffer_list.append(grad_2)
                    else:
                        mass_buffer_list.append(state['mass_buffer'] + grad_2)
            # update momentum_buffers in state
            for p, mass_buffer in zip(params_with_grad, mass_buffer_list):
                state = self.state[p]
                state['mass_buffer'] = mass_buffer

    @torch.no_grad()
    def estimate_mass(self, epsilon=1e-6, use_identity=True):
        m_min = None
        for group in self.param_groups:
            m_list = []
            params_with_grad = []
            for p in group['params']:
                if p.grad is not None:
                    params_with_grad.append(p)
                    state = self.state[p]
                    if 'm_buff' in state:
                        m_pre = state['m_buffer']
                    else:
                        m_pre = 1
                    if 'momentum_buffer' in state:
                        state['momentum_buffer'].mul_((1 / m_pre) ** 0.5)

                    if 'mass_buffer' not in state:
                        pass
                    else:
                        mass = state["mass_buffer"]
                        if use_identity:
                            m = torch.mean(mass) / torch.mean(mass)
                        else:
                            m = (torch.mean(mass) / self.estimate_mass_steps + epsilon) ** 0.5
                        if m_min is None:
                            m_min = m
                        else:
                            m_min = torch.minimum(m, m_min)
                        del state["mass_buffer"]
                    m_list.append(m)

                # update momentum_buffers in state
                for p, m in zip(params_with_grad, m_list):
                    state = self.state[p]
                    state['m_buffer'] = m
        # save mass minimum
        self.mass_scale = m_min

        # update momentum
        for group in self.param_groups:
            for p in group['params']:
                if p.grad is not None:
                    if 'momentum_buffer' in state:
                        state['momentum_buffer'].mul_((state['m_buffer'] / self.mass_scale) ** 0.5)

    @torch.no_grad()
    def step(self, closure=None):
        """Performs a single optimization step.

        Args:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """

        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        if self.step_counter < self.estimate_mass_steps:
            self.accumulate_mass()
        if self.step_counter == self.estimate_mass_steps:
            self.estimate_mass()
        if self.step_counter >= self.estimate_mass_steps:
            for group in self.param_groups:
                params_with_grad = []
                d_p_list = []
                momentum_buffer_list = []
                mass_buffer_list = []
                weight_decay = group['weight_decay']
                momentum = group['momentum']
                lr = group['lr']
                for p in group['params']:
                    if p.grad is not None:
                        params_with_grad.append(p)
                        d_p_list.append(p.grad)
                        state = self.state[p]
                        if 'momentum_buffer' not in state:
                            momentum_buffer_list.append(None)
                        else:
                            momentum_buffer_list.append(state['momentum_buffer'])
                        mass_buffer_list.append(state['m_buffer'] / self.mass_scale)

                langevin(params_with_grad,
                         d_p_list,
                         momentum_buffer_list,
                         mass_buffer_list,
                         weight_decay=weight_decay,
                         h=(lr / self.dataset_size) ** 0.5,
                         gamma=(1 - momentum) * (self.dataset_size / lr) ** 0.5,
                         n=self.dataset_size,
                         T=self.temperature
                         )

                # update momentum_buffers in state
                for p, momentum_buffer in zip(params_with_grad, momentum_buffer_list):
                    state = self.state[p]
                    state['momentum_buffer'] = momentum_buffer

        self.step_counter = (self.step_counter + 1) % self.steps_per_epoch
        return loss


def langevin(params: List[Tensor],
             d_p_list: List[Tensor],
             momentum_buffer_list: List[Optional[Tensor]],
             mass_buffer_list,
             *,
             weight_decay: float,
             gamma: float,
             h: float,
             n: float,
             T: float):
    for i, param in enumerate(params):
        d_p = d_p_list[i]
        if weight_decay != 0:
            d_p = d_p.add(param, alpha=weight_decay/n)

        m = mass_buffer_list[i]
        if (1 - h * gamma) > 0:
            buf = momentum_buffer_list[i]
            if T > 0.:
                noise_std = (2 * m * gamma * h * T) ** 0.5
                noise = noise_std * torch.randn(*d_p.size(), device=d_p.device)
            else:
                noise = 0.

            if buf is None:
                buf = torch.clone(d_p).detach()
                momentum_buffer_list[i] = buf
            else:
                buf.mul_(1 - h * gamma).add_(d_p, alpha=-h * n)
                buf.add_(noise)
        param.add_(buf, alpha=h/m)
