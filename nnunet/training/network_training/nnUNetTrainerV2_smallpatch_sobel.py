#    Copyright 2020 Division of Medical Image Computing, German Cancer Research Center (DKFZ), Heidelberg, Germany
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
import pickle
from pathlib import Path
from collections import OrderedDict
from typing import Tuple
from nnunet.inference.segmentation_export import save_segmentation_nifti_from_softmax
import numpy as np
import torch
from nnunet.training.data_augmentation.data_augmentation_moreDA import get_moreDA_augmentation
from nnunet.training.loss_functions.deep_supervision import MultipleOutputLoss2
from nnunet.utilities.to_torch import maybe_to_torch, to_cuda
from nnunet.network_architecture.generic_UNet import Generic_UNet
from nnunet.network_architecture.initialization import InitWeights_He
from nnunet.network_architecture.neural_network import SegmentationNetwork
from nnunet.training.data_augmentation.default_data_augmentation import default_2D_augmentation_params, \
    get_patch_size, default_3D_augmentation_params
from nnunet.training.dataloading.dataset_loading import unpack_dataset
from nnunet.training.network_training.nnUNetTrainer import nnUNetTrainer
from nnunet.utilities.nd_softmax import softmax_helper
from sklearn.model_selection import KFold
from torch import nn
from torch.cuda.amp import autocast
from nnunet.training.learning_rate.poly_lr import poly_lr
from batchgenerators.utilities.file_and_folder_operations import *
from nnunet.optimizer.SymEulerSGMCMC import SymEulerSGMCMC
import time
import copy
from batchgenerators.augmentations.utils import pad_nd_image
from kornia.filters import sobel as kornia_sobel


def cyclical_restart_lr(epoch, lr_init, epoch_per_cycle, restart_lr=1e-1, max_epochs=None,
                        restart_epochs=5, restart_slope=5e-2, const_after=1.0):
    if max_epochs is None:
        max_epochs = epoch_per_cycle
    in_cycle_epoch = epoch % epoch_per_cycle
    in_cycle_epoch = min(const_after * epoch_per_cycle - 1, in_cycle_epoch)
    lr = poly_lr(in_cycle_epoch, max_epochs, lr_init)
    if epoch % epoch_per_cycle < restart_epochs and epoch >= epoch_per_cycle:
        lr = restart_lr + restart_slope * (epoch % epoch_per_cycle)
    return lr


def cosine_annealing_lr(epoch, lr_init, epoch_per_cycle):
    return lr_init * 0.5 * (np.cos(np.pi * (epoch % epoch_per_cycle) / epoch_per_cycle) + 1)


def simulated_annealing(epoch, Tm1=1e-3, Tm2=1e-3, burn_in_num_epochs=200, inner_cycle_size=50, n_inner_cycles=6):
    if epoch < burn_in_num_epochs:
        Tx = 0
    else:
        inner_cycle_idx = (epoch - burn_in_num_epochs) // inner_cycle_size
        if inner_cycle_idx % n_inner_cycles == 0:
            T0 = Tm1
            time_constant = 5
        else:
            T0 = Tm2
            time_constant = 5
        in_cycle_epoch_num = (epoch - burn_in_num_epochs) % inner_cycle_size
        Tx = T0 * np.exp(- time_constant * in_cycle_epoch_num / inner_cycle_size)
    return Tx


class nnUNetTrainerV2_SmallPatchSobel(nnUNetTrainer):
    """
    Info for Fabian: same as internal nnUNetTrainerV2_2
    """

    def __init__(self, plans_file, fold, output_folder=None, dataset_directory=None, batch_dice=True, stage=None,
                 unpack_data=True, deterministic=True, fp16=False, user_defined_config=None):
        super().__init__(plans_file, fold, output_folder, dataset_directory, batch_dice, stage, unpack_data,
                         deterministic, fp16, user_defined_config=user_defined_config)
        self.deep_supervision_scales = None
        self.ds_loss_weights = None
        self.p_dropout = 0.0
        self.pin_memory = True

        # cyclical parameters
        self.mode_jumping_type = self.user_defined_config["mode_jumping"]
        self.initial_lr = self.user_defined_config["initial_lr"]
        self.restart_lr = self.user_defined_config["restart_lr"]
        # self.initial_lr = 1e-4
        self.restart_slope = self.user_defined_config["restart_slope"]
        self.restart_epochs = self.user_defined_config["restart_epochs"]
        self.warm_up_ratio = self.user_defined_config["warm_up_ratio"]
        self.enable_hmc = self.user_defined_config["enable_hmc"]
        self.temper = self.user_defined_config["temper"]
        self.n_train = self.user_defined_config["n_train"]
        self.const_after = self.user_defined_config["const_after"]
        self.num_of_cycles = self.user_defined_config["num_of_cycles"]
        self.epoch_per_cycle = self.user_defined_config["epoch_per_cycle"]
        self.sampling_every = self.user_defined_config["sampling_every"]
        self.max_num_epochs = self.epoch_per_cycle * self.num_of_cycles

        self.cyclical_lr_scheduler = None
        if self.mode_jumping_type == "high_lr":
            self.cyclical_lr_scheduler = lambda epoch: cyclical_restart_lr(epoch, self.initial_lr,
                                                                           self.epoch_per_cycle,
                                                                           restart_lr=self.restart_lr,
                                                                           restart_epochs=self.restart_epochs,
                                                                           restart_slope=self.restart_slope,
                                                                           const_after=self.const_after)
        elif self.mode_jumping_type == "cosine_annealing":
            self.cyclical_lr_scheduler = lambda epoch: cosine_annealing_lr(epoch, self.initial_lr,
                                                                           self.epoch_per_cycle)
        else:
            raise ValueError(f"Unknown mode jumping: {self.mode_jumping_type}")

        # noise injection flag
        self.noise_injection = False
        self.running_average_weight = None
        self.online_monitoring_results = []
        self.oversample_foreground_percent = 0.8

    def load_plans_file(self):
        """
        This is what actually configures the entire experiment. The plans file is generated by experiment planning
        :return:
        """
        self.plans = load_pickle(self.plans_file)

    def process_plans(self, plans):
        if self.stage is None:
            assert len(list(plans['plans_per_stage'].keys())) == 1, \
                "If self.stage is None then there can be only one stage in the plans file. That seems to not be the " \
                "case. Please specify which stage of the cascade must be trained"
            self.stage = list(plans['plans_per_stage'].keys())[0]
        self.plans = plans

        stage_plans = self.plans['plans_per_stage'][self.stage]
        self.batch_size = 40
        print("BATCHSIZE: ", self.batch_size)
        self.net_pool_per_axis = [5, 5]
        self.patch_size = np.array([128, 128]).astype(int)
        self.do_dummy_2D_aug = stage_plans['do_dummy_2D_data_aug']

        if 'pool_op_kernel_sizes' not in stage_plans.keys():
            assert 'num_pool_per_axis' in stage_plans.keys()
            self.print_to_log_file("WARNING! old plans file with missing pool_op_kernel_sizes. Attempting to fix it...")
            self.net_num_pool_op_kernel_sizes = []
            for i in range(max(self.net_pool_per_axis)):
                curr = []
                for j in self.net_pool_per_axis:
                    if (max(self.net_pool_per_axis) - j) <= i:
                        curr.append(2)
                    else:
                        curr.append(1)
                self.net_num_pool_op_kernel_sizes.append(curr)
        else:
            self.net_num_pool_op_kernel_sizes = stage_plans['pool_op_kernel_sizes'][:5]

        if 'conv_kernel_sizes' not in stage_plans.keys():
            self.print_to_log_file("WARNING! old plans file with missing conv_kernel_sizes. Attempting to fix it...")
            self.net_conv_kernel_sizes = [[3] * len(self.net_pool_per_axis)] * (max(self.net_pool_per_axis) + 1)
        else:
            self.net_conv_kernel_sizes = stage_plans['conv_kernel_sizes'][:6]

        self.pad_all_sides = None  # self.patch_size
        self.intensity_properties = plans['dataset_properties']['intensityproperties']
        self.normalization_schemes = plans['normalization_schemes']
        self.base_num_features = plans['base_num_features']
        self.num_input_channels = plans['num_modalities']
        self.num_classes = plans['num_classes'] + 1  # background is no longer in num_classes
        self.classes = plans['all_classes']
        self.use_mask_for_norm = plans['use_mask_for_norm']
        self.only_keep_largest_connected_component = plans['keep_only_largest_region']
        self.min_region_size_per_class = plans['min_region_size_per_class']

        if plans.get('transpose_forward') is None or plans.get('transpose_backward') is None:
            print("WARNING! You seem to have data that was preprocessed with a previous version of nnU-Net. "
                  "You should rerun preprocessing. We will proceed and assume that both transpose_foward "
                  "and transpose_backward are [0, 1, 2]. If that is not correct then weird things will happen!")
            plans['transpose_forward'] = [0, 1, 2]
            plans['transpose_backward'] = [0, 1, 2]
        self.transpose_forward = plans['transpose_forward']
        self.transpose_backward = plans['transpose_backward']

        if len(self.patch_size) == 2:
            self.threeD = False
        elif len(self.patch_size) == 3:
            self.threeD = True
        else:
            raise RuntimeError("invalid patch size in plans file: %s" % str(self.patch_size))

        if "conv_per_stage" in plans.keys():  # this has been added to the plans only recently
            self.conv_per_stage = plans['conv_per_stage']
        else:
            self.conv_per_stage = 2

    def initialize(self, training=True, force_load_plans=False, p_dropout=0.0):
        """
        - replaced get_default_augmentation with get_moreDA_augmentation
        - enforce to only run this code once
        - loss function wrapper for deep supervision

        :param training:
        :param force_load_plans:
        :param p_dropout:
        :return:
        """
        self.p_dropout = p_dropout
        if not self.was_initialized:
            maybe_mkdir_p(self.output_folder)

            if force_load_plans or (self.plans is None):
                self.load_plans_file()

            self.process_plans(self.plans)

            self.setup_DA_params()

            ################# Here we wrap the loss for deep supervision ############
            # we need to know the number of outputs of the network
            net_numpool = len(self.net_num_pool_op_kernel_sizes)

            # we give each output a weight which decreases exponentially (division by 2) as the resolution decreases
            # this gives higher resolution outputs more weight in the loss
            weights = np.array([1 / (2 ** i) for i in range(net_numpool)])

            # we don't use the lowest 2 outputs. Normalize weights so that they sum to 1
            mask = np.array([True] + [True if i < net_numpool - 1 else False for i in range(1, net_numpool)])
            weights[~mask] = 0
            weights = weights / weights.sum()
            self.ds_loss_weights = weights
            # now wrap the loss
            self.loss = MultipleOutputLoss2(self.loss, self.ds_loss_weights)
            ################# END ###################

            self.folder_with_preprocessed_data = join(self.dataset_directory, self.plans['data_identifier'] +
                                                      "_stage%d" % self.stage)
            if training:
                self.dl_tr, self.dl_val = self.get_basic_generators()
                if self.unpack_data:
                    print("unpacking dataset")
                    unpack_dataset(self.folder_with_preprocessed_data)
                    print("done")
                else:
                    print(
                        "INFO: Not unpacking data! Training may be slow due to that. Pray you are not using 2d or you "
                        "will wait all winter for your model to finish!")

                self.tr_gen, self.val_gen = get_moreDA_augmentation(
                    self.dl_tr, self.dl_val,
                    self.data_aug_params[
                        'patch_size_for_spatialtransform'],
                    self.data_aug_params,
                    deep_supervision_scales=self.deep_supervision_scales,
                    pin_memory=self.pin_memory,
                    use_nondetMultiThreadedAugmenter=False
                )
                self.print_to_log_file("TRAINING KEYS:\n %s" % (str(self.dataset_tr.keys())),
                                       also_print_to_console=False)
                self.print_to_log_file("VALIDATION KEYS:\n %s" % (str(self.dataset_val.keys())),
                                       also_print_to_console=False)
            else:
                pass

            self.initialize_network()
            self.initialize_optimizer_and_scheduler()

            assert isinstance(self.network, (SegmentationNetwork, nn.DataParallel))
        else:
            self.print_to_log_file('self.was_initialized is True, not running self.initialize again')
        self.was_initialized = True
        save_json(self.user_defined_config, os.path.join(self.output_folder, "config"))

    def initialize_network(self):
        """
        - momentum 0.99
        - SGD instead of Adam
        - self.lr_scheduler = None because we do poly_lr
        - deep supervision = True
        - i am sure I forgot something here

        Known issue: forgot to set neg_slope=0 in InitWeights_He; should not make a difference though
        :return:
        """
        if self.threeD:
            conv_op = nn.Conv3d
            dropout_op = nn.Dropout3d
            norm_op = nn.InstanceNorm3d

        else:
            conv_op = nn.Conv2d
            dropout_op = nn.Dropout2d
            norm_op = nn.InstanceNorm2d

        norm_op_kwargs = {'eps': 1e-5, 'affine': True}
        dropout_op_kwargs = {'p': self.p_dropout, 'inplace': True}
        net_nonlin = nn.LeakyReLU
        net_nonlin_kwargs = {'negative_slope': 1e-2, 'inplace': True}
        self.network = Generic_UNet(self.num_input_channels, self.base_num_features, self.num_classes,
                                    len(self.net_num_pool_op_kernel_sizes),
                                    self.conv_per_stage, 2, conv_op, norm_op, norm_op_kwargs, dropout_op,
                                    dropout_op_kwargs,
                                    net_nonlin, net_nonlin_kwargs, True, False, lambda x: x, InitWeights_He(1e-2),
                                    self.net_num_pool_op_kernel_sizes, self.net_conv_kernel_sizes, False, True, True)
        if torch.cuda.is_available():
            self.network.cuda()
        self.network.inference_apply_nonlin = softmax_helper

    def initialize_optimizer_and_scheduler(self):
        assert self.network is not None, "self.initialize_network must be called first"
        # self.optimizer = torch.optim.SGD(self.network.parameters(), self.initial_lr,
        #                                  weight_decay=self.weight_decay,
        #                                  momentum=0.99, nesterov=True)
        self.optimizer = SymEulerSGMCMC(self.network.parameters(),
                                        self.initial_lr,
                                        momentum=0.99,
                                        weight_decay=self.weight_decay,
                                        dataset_size=self.n_train,
                                        temperature=0,
                                        steps_per_epoch=self.num_batches_per_epoch,
                                        estimate_mass_steps=5)
        # self.optimizer = pSGHMC(self.network.parameters(), self.initial_lr,
        #                         weight_decay=self.weight_decay,
        #                         num_burn_in_steps=int(
        #                             self.warm_up_ratio * self.epoch_per_cycle * self.num_batches_per_epoch),
        #                         momentum=0.95, scale_grad=self.temper / self.n_train)
        self.lr_scheduler = None

    def run_online_evaluation(self, output, target):
        """
        due to deep supervision the return value and the reference are now lists of tensors. We only need the full
        resolution output because this is what we are interested in in the end. The others are ignored
        :param output:
        :param target:
        :return:
        """
        target = target[0]
        output = output[0]
        return super().run_online_evaluation(output, target)

    def validate(self, do_mirroring: bool = True, use_sliding_window: bool = True,
                 step_size: float = 0.5, save_softmax: bool = True, use_gaussian: bool = True, overwrite: bool = True,
                 validation_folder_name: str = 'validation_raw', debug: bool = False, all_in_gpu: bool = False,
                 segmentation_export_kwargs: dict = None, run_postprocessing_on_folds: bool = True):
        """
        We need to wrap this because we need to enforce self.network.do_ds = False for prediction
        """
        ds = self.network.do_ds
        self.network.do_ds = False
        ret = super().validate(do_mirroring=do_mirroring, use_sliding_window=use_sliding_window, step_size=step_size,
                               save_softmax=save_softmax, use_gaussian=use_gaussian,
                               overwrite=overwrite, validation_folder_name=validation_folder_name, debug=debug,
                               all_in_gpu=all_in_gpu, segmentation_export_kwargs=segmentation_export_kwargs,
                               run_postprocessing_on_folds=run_postprocessing_on_folds)

        self.network.do_ds = ds
        return ret

    def predict_preprocessed_data_return_seg_and_softmax(self, data: np.ndarray, do_mirroring: bool = True,
                                                         mirror_axes: Tuple[int] = None,
                                                         use_sliding_window: bool = True, step_size: float = 0.5,
                                                         use_gaussian: bool = True, pad_border_mode: str = 'constant',
                                                         pad_kwargs: dict = None, all_in_gpu: bool = False,
                                                         verbose: bool = True, mixed_precision=True) -> Tuple[
        np.ndarray, np.ndarray]:
        """
        We need to wrap this because we need to enforce self.network.do_ds = False for prediction
        """
        ds = self.network.do_ds
        self.network.do_ds = False
        ret = super().predict_preprocessed_data_return_seg_and_softmax(data,
                                                                       do_mirroring=do_mirroring,
                                                                       mirror_axes=mirror_axes,
                                                                       use_sliding_window=use_sliding_window,
                                                                       step_size=step_size, use_gaussian=use_gaussian,
                                                                       pad_border_mode=pad_border_mode,
                                                                       pad_kwargs=pad_kwargs, all_in_gpu=all_in_gpu,
                                                                       verbose=verbose,
                                                                       mixed_precision=mixed_precision)
        self.network.do_ds = ds
        return ret

    def run_iteration(self, data_generator, do_backprop=True, run_online_evaluation=False):
        """
        gradient clipping improves training stability

        :param data_generator:
        :param do_backprop:
        :param run_online_evaluation:
        :return:
        """
        data_dict = next(data_generator)
        data = data_dict['data']
        target = data_dict['target']

        data = maybe_to_torch(data)
        data = kornia_sobel(data)
        amin = data.amin(dim=[2, 3], keepdim=True)
        amax = data.amax(dim=[2, 3], keepdim=True)
        data = (data - amin) / (amax - amin + 1e-8)

        target = maybe_to_torch(target)

        if torch.cuda.is_available():
            data = to_cuda(data)
            target = to_cuda(target)

        self.optimizer.zero_grad()

        if self.fp16:
            with autocast():
                output = self.network(data)
                del data
                l = self.loss(output, target)
                l_plus_noise = l
                self.optimizer.inject_noise = self.noise_injection

            if do_backprop:
                self.amp_grad_scaler.scale(l_plus_noise).backward()
                self.amp_grad_scaler.unscale_(self.optimizer)
                torch.nn.utils.clip_grad_norm_(self.network.parameters(), 12)
                self.amp_grad_scaler.step(self.optimizer)
                self.amp_grad_scaler.update()
        else:
            output = self.network(data)
            del data
            l = self.loss(output, target)
            l_plus_noise = l
            self.optimizer.inject_noise = self.noise_injection

            if do_backprop:
                l_plus_noise.backward()
                torch.nn.utils.clip_grad_norm_(self.network.parameters(), 12)
                self.optimizer.step()

        if run_online_evaluation:
            self.run_online_evaluation(output, target)

        del target

        return l.detach().cpu().numpy()

    def do_split(self):
        """
        The default split is a 5 fold CV on all available training cases. nnU-Net will create a split (it is seeded,
        so always the same) and save it as splits_final.pkl file in the preprocessed data directory.
        Sometimes you may want to create your own split for various reasons. For this you will need to create your own
        splits_final.pkl file. If this file is present, nnU-Net is going to use it and whatever splits are defined in
        it. You can create as many splits in this file as you want. Note that if you define only 4 splits (fold 0-3)
        and then set fold=4 when training (that would be the fifth split), nnU-Net will print a warning and proceed to
        use a random 80:20 data split.
        :return:
        """
        if self.fold == "all":
            # if fold==all then we use all images for training and validation
            tr_keys = val_keys = list(self.dataset.keys())
        else:
            splits_file = join(self.dataset_directory, "splits_final.pkl")

            # if the split file does not exist we need to create it
            if not isfile(splits_file):
                self.print_to_log_file("Creating new 5-fold cross-validation split...")
                splits = []
                all_keys_sorted = np.sort(list(self.dataset.keys()))
                kfold = KFold(n_splits=5, shuffle=True, random_state=12345)
                for i, (train_idx, test_idx) in enumerate(kfold.split(all_keys_sorted)):
                    train_keys = np.array(all_keys_sorted)[train_idx]
                    test_keys = np.array(all_keys_sorted)[test_idx]
                    splits.append(OrderedDict())
                    splits[-1]['train'] = train_keys
                    splits[-1]['val'] = test_keys
                save_pickle(splits, splits_file)

            else:
                self.print_to_log_file("Using splits from existing split file:", splits_file)
                splits = load_pickle(splits_file)
                self.print_to_log_file("The split file contains %d splits." % len(splits))

            self.print_to_log_file("Desired fold for training: %d" % self.fold)
            if self.fold < len(splits):
                tr_keys = splits[0]['train']
                val_keys = splits[0]['val']
                self.print_to_log_file("This split has %d training and %d validation cases."
                                       % (len(tr_keys), len(val_keys)))
            else:
                self.print_to_log_file("INFO: You requested fold %d for training but splits "
                                       "contain only %d folds. I am now creating a "
                                       "random (but seeded) 80:20 split!" % (self.fold, len(splits)))
                # if we request a fold that is not in the split file, create a random 80:20 split
                rnd = np.random.RandomState(seed=12345 + self.fold)
                keys = np.sort(list(self.dataset.keys()))
                idx_tr = rnd.choice(len(keys), int(len(keys) * 0.8), replace=False)
                idx_val = [i for i in range(len(keys)) if i not in idx_tr]
                tr_keys = [keys[i] for i in idx_tr]
                val_keys = [keys[i] for i in idx_val]
                self.print_to_log_file("This random 80:20 split has %d training and %d validation cases."
                                       % (len(tr_keys), len(val_keys)))

        tr_keys.sort()
        val_keys.sort()
        self.dataset_tr = OrderedDict()
        for i in tr_keys:
            self.dataset_tr[i] = self.dataset[i]
        self.dataset_val = OrderedDict()
        for i in val_keys:
            self.dataset_val[i] = self.dataset[i]

    def setup_DA_params(self):
        """
        - we increase roation angle from [-15, 15] to [-30, 30]
        - scale range is now (0.7, 1.4), was (0.85, 1.25)
        - we don't do elastic deformation anymore

        :return:
        """

        self.deep_supervision_scales = [[1, 1, 1]] + list(list(i) for i in 1 / np.cumprod(
            np.vstack(self.net_num_pool_op_kernel_sizes), axis=0))[:-1]

        if self.threeD:
            self.data_aug_params = default_3D_augmentation_params
            self.data_aug_params['rotation_x'] = (-30. / 360 * 2. * np.pi, 30. / 360 * 2. * np.pi)
            self.data_aug_params['rotation_y'] = (-30. / 360 * 2. * np.pi, 30. / 360 * 2. * np.pi)
            self.data_aug_params['rotation_z'] = (-30. / 360 * 2. * np.pi, 30. / 360 * 2. * np.pi)
            if self.do_dummy_2D_aug:
                self.data_aug_params["dummy_2D"] = True
                self.print_to_log_file("Using dummy2d data augmentation")
                self.data_aug_params["elastic_deform_alpha"] = \
                    default_2D_augmentation_params["elastic_deform_alpha"]
                self.data_aug_params["elastic_deform_sigma"] = \
                    default_2D_augmentation_params["elastic_deform_sigma"]
                self.data_aug_params["rotation_x"] = default_2D_augmentation_params["rotation_x"]
        else:
            self.do_dummy_2D_aug = False
            if max(self.patch_size) / min(self.patch_size) > 1.5:
                default_2D_augmentation_params['rotation_x'] = (-15. / 360 * 2. * np.pi, 15. / 360 * 2. * np.pi)
            self.data_aug_params = default_2D_augmentation_params
        self.data_aug_params["mask_was_used_for_normalization"] = self.use_mask_for_norm

        if self.do_dummy_2D_aug:
            self.basic_generator_patch_size = get_patch_size(self.patch_size[1:],
                                                             self.data_aug_params['rotation_x'],
                                                             self.data_aug_params['rotation_y'],
                                                             self.data_aug_params['rotation_z'],
                                                             self.data_aug_params['scale_range'])
            self.basic_generator_patch_size = np.array([self.patch_size[0]] + list(self.basic_generator_patch_size))
        else:
            self.basic_generator_patch_size = get_patch_size(self.patch_size, self.data_aug_params['rotation_x'],
                                                             self.data_aug_params['rotation_y'],
                                                             self.data_aug_params['rotation_z'],
                                                             self.data_aug_params['scale_range'])

        self.data_aug_params["scale_range"] = (0.7, 1.4)
        self.data_aug_params["do_elastic"] = False
        self.data_aug_params['selected_seg_channels'] = [0]
        self.data_aug_params['patch_size_for_spatialtransform'] = self.patch_size

        self.data_aug_params["num_cached_per_thread"] = 2

    def maybe_update_lr(self, epoch=None):
        """
        if epoch is not None we overwrite epoch. Else we use epoch = self.epoch + 1

        (maybe_update_lr is called in on_epoch_end which is called before epoch is incremented.
        herefore we need to do +1 here)

        :param epoch:
        :return:
        """
        if epoch is None:
            ep = self.epoch + 1
        else:
            ep = epoch
        self.optimizer.param_groups[0]['lr'] = self.cyclical_lr_scheduler(ep)
        self.print_to_log_file("lr:", np.round(self.optimizer.param_groups[0]['lr'], decimals=6))

    def maybe_update_temperature(self):
        Tx = simulated_annealing(self.epoch, Tm1=1e-4,
                                 Tm2=1e-4, burn_in_num_epochs=200, inner_cycle_size=100, n_inner_cycles=3)
        self.optimizer.temperature = Tx

    def set_noise_injection(self):
        next_epoch_ratio = ((self.epoch + 1) % self.epoch_per_cycle) / self.epoch_per_cycle
        self.noise_injection = (self.warm_up_ratio <= next_epoch_ratio) and self.enable_hmc
        self.print_to_log_file("Noise injection: ", str(self.noise_injection), also_print_to_console=True)

    def maybe_save_checkpoint(self):
        """
        Saves a checkpoint every save_ever epochs.
        :return:
        """
        in_cycle_epoch = self.epoch % self.epoch_per_cycle
        if in_cycle_epoch < self.warm_up_ratio * self.epoch_per_cycle:
            self.save_every = 10
        else:
            self.save_every = self.sampling_every

        if self.save_intermediate_checkpoints and (self.epoch % self.save_every == (self.save_every - 1)):
            self.print_to_log_file("saving scheduled checkpoint file...")
            if not self.save_latest_only:
                self.save_checkpoint(join(self.output_folder, "model_ep_%04.0d.model" % (self.epoch + 1)),
                                     save_optimizer=(self.epoch % self.save_every == (40 - 1)))

                if in_cycle_epoch >= self.warm_up_ratio * self.epoch_per_cycle:
                    self.save_average_weight()
            self.save_checkpoint(join(self.output_folder, "model_latest.model"))
            self.print_to_log_file("done")

    def on_epoch_end(self):
        """
        overwrite patient-based early stopping. Always run to 1000 epochs
        :return:
        """
        super().on_epoch_end()
        self.build_running_average()
        self.maybe_save_checkpoint()
        self.set_noise_injection()
        self.maybe_update_temperature()
        self.print_to_log_file(f"Temperature: {self.optimizer.temperature:.3e}")
        # self.validate()
        create_ood_epoch = 199
        self.create_ood_split(epoch=create_ood_epoch)
        if self.epoch >= create_ood_epoch:
            res = self.diversity_validate()
            if res["accepted"]:
                self.save_accepted_checkpoint(join(self.output_folder, "model_ep_%04.0d.model" % (self.epoch + 1)),
                                              save_optimizer=False)
            self.online_monitoring_results.append(res)
        else:
            self.online_monitoring_results.append(None)

        continue_training = self.epoch < self.max_num_epochs

        # it can rarely happen that the momentum of nnUNetTrainerV2 is too high for some dataset. If at epoch 100 the
        # estimated validation Dice is still 0 then we reduce the momentum from 0.99 to 0.95
        if self.epoch == 100:
            if self.all_val_eval_metrics[-1] == 0:
                assert False, "Momentum check not implemented!"
                # self.optimizer.param_groups[0]["momentum"] = 0.95
                # self.network.apply(InitWeights_He(1e-2))
                # self.print_to_log_file("At epoch 100, the mean foreground Dice was 0. This can be caused by a too "
                #                        "high momentum. High momentum (0.99) is good for datasets where it works, but "
                #                        "sometimes causes issues such as this one. Momentum has now been reduced to "
                #                        "0.95 and network weights have been reinitialized")
        return continue_training

    def run_training(self):
        """
        if we run with -c then we need to set the correct lr for the first epoch, otherwise it will run the first
        continued epoch with self.initial_lr

        we also need to make sure deep supervision in the network is enabled for training, thus the wrapper
        :return:
        """
        self.maybe_update_lr(self.epoch)  # if we dont overwrite epoch then self.epoch+1 is used which is not what we
        # want at the start of the training
        self.set_noise_injection()
        ds = self.network.do_ds
        self.network.do_ds = True
        ret = super().run_training()
        self.network.do_ds = ds
        return ret

    def build_running_average(self):
        in_cycle_epoch = self.epoch % self.epoch_per_cycle

        # weight averaging
        if in_cycle_epoch >= self.warm_up_ratio * self.epoch_per_cycle:
            with torch.no_grad():
                aver_idx = self.epoch % self.sampling_every
                if aver_idx == 0:
                    self.running_average_weight = copy.deepcopy(self.network.state_dict())
                else:
                    current_weight = copy.deepcopy(self.network.state_dict())
                    for pname, w in self.running_average_weight.items():
                        self.running_average_weight[pname] = aver_idx / (aver_idx + 1) * w + \
                                                             1 / (aver_idx + 1) * current_weight[pname]

    def save_average_weight(self):
        start_time = time.time()
        state_dict = self.running_average_weight

        for key in state_dict.keys():
            state_dict[key] = state_dict[key].cpu()
        lr_sched_state_dct = None
        if self.lr_scheduler is not None and hasattr(self.lr_scheduler,
                                                     'state_dict'):  # not isinstance(self.lr_scheduler, lr_scheduler.ReduceLROnPlateau):
            lr_sched_state_dct = self.lr_scheduler.state_dict()
            # WTF is this!?
            # for key in lr_sched_state_dct.keys():
            #    lr_sched_state_dct[key] = lr_sched_state_dct[key]

        self.print_to_log_file("saving checkpoint...")
        save_this = {
            'epoch': self.epoch + 1,
            'state_dict': state_dict,
            'optimizer_state_dict': None,
            'lr_scheduler_state_dict': lr_sched_state_dct,
            'plot_stuff': (self.all_tr_losses, self.all_val_losses, self.all_val_losses_tr_mode,
                           self.all_val_eval_metrics),
            'best_stuff': (
                self.best_epoch_based_on_MA_tr_loss, self.best_MA_tr_loss_for_patience,
                self.best_val_eval_criterion_MA)}
        if self.amp_grad_scaler is not None:
            save_this['amp_grad_scaler'] = self.amp_grad_scaler.state_dict()

        fname = join(self.output_folder, "model_aw_%04.0d.model" % (self.epoch + 1))
        torch.save(save_this, fname)
        self.print_to_log_file("done, saving took %.2f seconds" % (time.time() - start_time))

    def validate(self, do_mirroring: bool = True, use_sliding_window: bool = True, step_size: float = 0.5,
                 save_softmax: bool = True, use_gaussian: bool = True, overwrite: bool = True,
                 validation_folder_name: str = 'validation_raw', debug: bool = False, all_in_gpu: bool = False,
                 segmentation_export_kwargs: dict = None):
        """
        if debug=True then the temporary files generated for postprocessing determination will be kept
        """
        start_time = time.time()
        current_mode = self.network.training
        self.network.eval()
        self.print_to_log_file("Validating...")

        assert self.was_initialized, "must initialize, ideally with checkpoint (or train first)"
        if self.dataset_val is None:
            self.load_dataset()
            self.do_split()

        if segmentation_export_kwargs is None:
            if 'segmentation_export_params' in self.plans.keys():
                force_separate_z = self.plans['segmentation_export_params']['force_separate_z']
                interpolation_order = self.plans['segmentation_export_params']['interpolation_order']
                interpolation_order_z = self.plans['segmentation_export_params']['interpolation_order_z']
            else:
                force_separate_z = None
                interpolation_order = 1
                interpolation_order_z = 0
        else:
            force_separate_z = segmentation_export_kwargs['force_separate_z']
            interpolation_order = segmentation_export_kwargs['interpolation_order']
            interpolation_order_z = segmentation_export_kwargs['interpolation_order_z']

        # predictions as they come from the network go here
        output_folder = join(self.output_folder, validation_folder_name)
        maybe_mkdir_p(output_folder)
        # this is for debug purposes
        my_input_args = {'do_mirroring': do_mirroring,
                         'use_sliding_window': use_sliding_window,
                         'step_size': step_size,
                         'save_softmax': save_softmax,
                         'use_gaussian': use_gaussian,
                         'overwrite': overwrite,
                         'validation_folder_name': validation_folder_name,
                         'debug': debug,
                         'all_in_gpu': all_in_gpu,
                         'segmentation_export_kwargs': segmentation_export_kwargs,
                         }
        save_json(my_input_args, join(output_folder, "validation_args.json"))

        if do_mirroring:
            if not self.data_aug_params['do_mirror']:
                raise RuntimeError("We did not train with mirroring so you cannot do inference with mirroring enabled")
            mirror_axes = self.data_aug_params['mirror_axes']
        else:
            mirror_axes = ()

        for k in self.dataset_val.keys():
            properties = load_pickle(self.dataset[k]['properties_file'])
            fname = properties['list_of_data_files'][0].split("/")[-1][:-12]
            if overwrite or (not isfile(join(output_folder, fname + ".nii.gz"))) or \
                    (save_softmax and not isfile(join(output_folder, fname + ".npz"))):
                data = np.load(self.dataset[k]['data_file'])['data']

                print(k, data.shape)
                data[-1][data[-1] == -1] = 0

                softmax_pred = self.predict_preprocessed_data_return_seg_and_softmax(data[:-1],
                                                                                     do_mirroring=do_mirroring,
                                                                                     mirror_axes=mirror_axes,
                                                                                     use_sliding_window=use_sliding_window,
                                                                                     step_size=step_size,
                                                                                     use_gaussian=use_gaussian,
                                                                                     all_in_gpu=all_in_gpu,
                                                                                     mixed_precision=self.fp16)[1]

                softmax_pred = softmax_pred.transpose([0] + [i + 1 for i in self.transpose_backward])
                if save_softmax:
                    softmax_fname = join(output_folder, fname + ".npz")
                else:
                    softmax_fname = None
                save_segmentation_nifti_from_softmax(softmax_pred, join(output_folder, fname + ".nii.gz"),
                                                     properties, interpolation_order, self.regions_class_order,
                                                     None, None,
                                                     softmax_fname, None, force_separate_z,
                                                     interpolation_order_z)
        end_time = time.time()
        self.print_to_log_file(f"Validation nifti gen took: {end_time - start_time:.3f} s")
        self.network.train(current_mode)

    def create_ood_split(self, epoch=50):
        if self.epoch == epoch:
            time_start = time.time()
            current_mode = self.network.training
            self.network.eval()
            self.print_to_log_file("Creating validation set...")

            list_of_image_file, list_of_gt_file = [], []
            validation_base = Path("/home/yidongzhao/barrel/Data/ConvertedData/MM/EvaluationData/ACDC_ValidationSet")
            variants = ["original", "noise_03", "blur_03"]

            for v in variants:
                variant_base = validation_base / v
                for fname in (variant_base / "images").glob("*.nii.gz"):
                    list_of_image_file.append(fname)
                    list_of_gt_file.append(variant_base / "labels" / fname.parts[-1])
            # divisible_by = self.network.input_shape_must_be_divisible_by
            unsure_slices = []
            unsure_slices_gt = []
            for image_path, gt_path in zip(list_of_image_file, list_of_gt_file):
                # load images and do padding, resizing
                image, seg, properties = self.preprocess_patient_seg([image_path.as_posix()],
                                                                     seg_file=gt_path.as_posix(),
                                                                     verbose=False)
                data, slicer = pad_nd_image(image, mode="edge", kwargs={}, return_slicer=True,
                                            shape_must_be_divisible_by=[64, 64])
                data = np.transpose(data, axes=[1, 0, 2, 3])
                seg, _ = pad_nd_image(seg, mode="edge", kwargs={}, return_slicer=True,
                                      shape_must_be_divisible_by=[64, 64])
                seg = np.transpose(seg, axes=[1, 0, 2, 3])
                seg[seg < 0] = 0
                seg = seg[:, 0, :, :]

                # prediction and evaluation
                data_torch = torch.from_numpy(data).cuda().float()
                score = self.network(data_torch)[0]
                p = self.network.inference_apply_nonlin(score).detach().cpu().numpy()
                pred_seg = p.argmax(axis=1)
                dice_matrix = np.zeros((pred_seg.shape[0], self.num_classes - 1))
                for c in range(1, self.num_classes):
                    for sl in range(pred_seg.shape[0]):
                        gt_sl = seg[sl, :, :] == c
                        pred_seg_sl = pred_seg[sl, :, :] == c
                        if (gt_sl.sum() + pred_seg_sl.sum()) < 1:
                            dice = 1
                        else:
                            dice = 2 * (gt_sl * pred_seg_sl).sum() / (gt_sl.sum() + pred_seg_sl.sum())
                        dice_matrix[sl, c - 1] = dice
                min_dice = dice_matrix.min(axis=1)
                for sl in range(pred_seg.shape[0]):
                    if min_dice[sl] < 0.7 and (seg[sl, :, :] > 0).sum() > 200:
                        unsure_slices.append(data[sl, 0, :, :])
                        unsure_slices_gt.append(seg[sl, :, :])
            shapes = list(set([_.shape for _ in unsure_slices]))
            images_with_same_shape = [[] for _ in shapes]
            gts_with_same_shape = [[] for _ in shapes]
            for im, gt in zip(unsure_slices, unsure_slices_gt):
                idx = shapes.index(im.shape)
                images_with_same_shape[idx].append(im)
                gts_with_same_shape[idx].append(gt)
            images_with_same_shape = [np.array(_) for _ in images_with_same_shape]
            gts_with_same_shape = [np.array(_) for _ in gts_with_same_shape]
            validation_base = Path(self.output_folder) / "validation_data"
            validation_base.mkdir(exist_ok=True, parents=True)

            for idx, (im, gt) in enumerate(zip(images_with_same_shape, gts_with_same_shape)):
                np.savez_compressed(validation_base / f"data_{idx:02d}.npz", image=im, label=gt)

            self.network.train(current_mode)
            time_end = time.time()
            self.print_to_log_file(
                f"Validation set complete ({len(unsure_slices):d} slices)! It took {time_end - time_start} s")

    def diversity_validate(self):
        validation_base = Path(self.output_folder) / "validation_data"
        max_num_slice = 50
        if not validation_base.exists():
            # OOD set not created
            pass
        else:
            data_files = list(validation_base.glob("data_*.npz"))
            data_file_number = [_.parts[-1][-6:-4] for _ in data_files]
            validate_result_files = validation_base / "validate_result.pkl"

            # try to load evaluation results
            if validate_result_files.exists():
                with open(validate_result_files, "rb") as stream:
                    evaluation_data = pickle.load(stream)
            else:
                evaluation_data = {"n": 0, "dice": 0., "nll": np.inf, "error": np.inf, "entropy": 0.}

            # old evaluation results
            old_dice = evaluation_data["dice"]
            old_error = evaluation_data["error"]
            old_entropy = evaluation_data["entropy"]
            old_nll = evaluation_data["nll"]

            # evaluating current sample
            new_dice, new_error, new_entropy, new_nll = 0., 0., 0., 0.
            num_fg_pixels = 0  # we evaluate NLL on foreground pixels
            num_err_pixels = 0  # we evaluate entropy on erroneous predictions
            new_predictions = []

            for df in data_files:
                current_prediction_file = validation_base / f"pred_{df.parts[-1][-6:-4]}.npz"
                # try to load old prediction files
                if current_prediction_file.exists():
                    with np.load(current_prediction_file) as dat:
                        current_prediction = dat["p"]
                        current_prediction_mask = current_prediction.argmax(axis=1)
                else:
                    # if no-previous prediction file found, reset
                    current_prediction = None
                    current_prediction_mask = None
                    evaluation_data = {"n": 0, "dice": 0., "nll": np.inf, "error": np.inf, "entropy": 0.}

                n = evaluation_data["n"]  # number of models already averaged
                with np.load(df) as stream:
                    image = stream["image"]
                    label = stream["label"]

                image = image[:, None, :, :]
                # split image if necessary
                n_division = image.shape[0] // max_num_slice + 1
                pred_softmax = []
                for n_div in range(n_division):
                    lb = n_div * max_num_slice
                    hb = min((n_div + 1) * max_num_slice, image.shape[0])
                    image_d = image[lb:hb, :, :, :]
                    image_d_torch = torch.from_numpy(image_d).cuda().float()
                    pred_score_d = self.network(image_d_torch)[0]
                    pred_softmax_d = self.network.inference_apply_nonlin(pred_score_d)
                    pred_softmax_d = pred_softmax_d.detach().cpu().numpy()
                    pred_softmax.append(pred_softmax_d)
                pred_softmax = np.concatenate(pred_softmax, axis=0)

                # online model averaging
                if current_prediction is not None:
                    new_prediction = current_prediction * n / (n + 1) + pred_softmax / (n + 1)
                    current_error = current_prediction_mask != label
                else:
                    new_prediction = pred_softmax
                    current_error = None
                new_predictions.append(new_prediction)

                # prediction mask
                pred_mask = new_prediction.argmax(axis=1)
                for c in range(1, self.num_classes):
                    pred_mask_c, label_mask_c = (pred_mask == c), (label == c)
                    dice_c = 2 * (pred_mask_c * label_mask_c).sum() / (pred_mask_c.sum() + label_mask_c.sum())
                    new_dice += dice_c / (self.num_classes - 1) / len(data_files)

                error = pred_mask != label
                if current_error is None:
                    current_error = error
                entropy = pred_softmax * np.log2(pred_softmax + 1e-8)
                entropy = -entropy.sum(axis=1)
                entropy_amount = entropy[current_error].sum()
                num_err_pixels += current_error.sum()

                error_amount = error.sum()

                label_onehot = np.zeros((label.shape[0], self.num_classes, *label.shape[1:]))
                for c in range(self.num_classes):
                    label_onehot[:, c, :, :] = (label == c).astype(np.float32)
                nll = - (np.log(np.maximum(new_prediction, 1e-12)) * label_onehot).mean(axis=1)
                nll = nll[label > 0].sum()
                num_fg_pixels += (label > 0).sum()
                new_nll += nll

                new_error += error_amount
                entropy_amount += entropy_amount
            new_nll = new_nll / num_fg_pixels
            new_entropy = entropy_amount / num_err_pixels
            dH = new_entropy - old_entropy
            dNLL = new_nll - old_nll

            # determine if the sample is accepted
            self.print_to_log_file(f"Old evaluation: {old_dice:.3f} {old_error:.0f} {old_entropy:.3f} {old_nll:.3f}")
            if new_error <= evaluation_data["error"] and dNLL <= -0.005:
                self.print_to_log_file(
                    f"Sample accepted: {new_dice:.3f} {new_error:.0f} {new_entropy:.3f} {new_nll:.3f}")
                for idx, new_pred in enumerate(new_predictions):
                    np.savez_compressed(validation_base / f"pred_{data_file_number[idx]}.npz", p=new_pred)
                new_evaluation_res = {"n": n + 1, "dice": new_dice, "error": new_error,
                                      "entropy": new_entropy,
                                      "nll": new_nll}
                with open(validate_result_files, "wb") as stream:
                    pickle.dump(new_evaluation_res, stream)
                accepted = True

            else:
                new_evaluation_res = {"n": n, "dice": new_dice, "error": new_error,
                                      "entropy": new_entropy,
                                      "nll": new_nll}
                self.print_to_log_file(
                    f"Sample rejected: {new_dice:.3f} {new_error:.0f} {new_entropy:.3f} {new_nll:.3f}")
                accepted = False
        new_evaluation_res["accepted"] = accepted
        return new_evaluation_res

    def save_accepted_checkpoint(self, fname, save_optimizer=True):
        start_time = time.time()
        state_dict = self.network.state_dict()
        for key in state_dict.keys():
            state_dict[key] = state_dict[key].cpu()
        lr_sched_state_dct = None
        if self.lr_scheduler is not None and hasattr(self.lr_scheduler,
                                                     'state_dict'):  # not isinstance(self.lr_scheduler, lr_scheduler.ReduceLROnPlateau):
            lr_sched_state_dct = self.lr_scheduler.state_dict()
            # WTF is this!?
            # for key in lr_sched_state_dct.keys():
            #    lr_sched_state_dct[key] = lr_sched_state_dct[key]
        if save_optimizer:
            optimizer_state_dict = self.optimizer.state_dict()
        else:
            optimizer_state_dict = None

        self.print_to_log_file("saving checkpoint...")
        save_this = {
            'epoch': self.epoch + 1,
            'state_dict': state_dict,
            'online_monitor': self.online_monitoring_results,
            'optimizer_state_dict': optimizer_state_dict,
            'lr_scheduler_state_dict': lr_sched_state_dct,
            'plot_stuff': (self.all_tr_losses, self.all_val_losses, self.all_val_losses_tr_mode,
                           self.all_val_eval_metrics),
            'best_stuff': (
            self.best_epoch_based_on_MA_tr_loss, self.best_MA_tr_loss_for_patience, self.best_val_eval_criterion_MA)}
        if self.amp_grad_scaler is not None:
            save_this['amp_grad_scaler'] = self.amp_grad_scaler.state_dict()

        torch.save(save_this, fname)
        self.print_to_log_file("done, saving took %.2f seconds" % (time.time() - start_time))
        info = OrderedDict()
        info['init'] = self.init_args
        info['name'] = self.__class__.__name__
        info['class'] = str(self.__class__)
        info['plans'] = self.plans

        write_pickle(info, fname + ".pkl")
