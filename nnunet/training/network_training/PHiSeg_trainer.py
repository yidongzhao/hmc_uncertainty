#    Copyright 2020 Division of Medical Image Computing, German Cancer Research Center (DKFZ), Heidelberg, Germany
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import matplotlib
import matplotlib.pyplot as plt
from collections import OrderedDict
from typing import Tuple
import torch.backends.cudnn as cudnn
from tqdm import trange
import sys
from time import time, sleep
import numpy as np
import torch
from nnunet.training.data_augmentation.data_augmentation_moreDA import get_moreDA_augmentation
from nnunet.training.loss_functions.deep_supervision import MultipleOutputLoss2
from nnunet.utilities.to_torch import maybe_to_torch, to_cuda
from nnunet.training.data_augmentation.default_data_augmentation import default_2D_augmentation_params, \
    get_patch_size, default_3D_augmentation_params
from nnunet.training.dataloading.dataset_loading import unpack_dataset
from nnunet.training.network_training.nnUNetTrainer import nnUNetTrainer
from sklearn.model_selection import KFold
from torch.cuda.amp import autocast
from nnunet.training.learning_rate.poly_lr import poly_lr
from batchgenerators.utilities.file_and_folder_operations import *
from nnunet.network_architecture.PhiSegNet import PHiSeg_PosteriorPrior, PHiSeg_Likelihood, KL_two_gaussian
from itertools import chain
from nnunet.network_architecture.initialization import InitWeights_He


class PHiSeg_trainer(nnUNetTrainer):
    """
    Info for Fabian: same as internal nnUNetTrainerV2_2
    """

    def __init__(self, plans_file, fold, output_folder=None, dataset_directory=None, batch_dice=True, stage=None,
                 unpack_data=True, deterministic=True, fp16=False, user_defined_config=None):
        super().__init__(plans_file, fold, output_folder, dataset_directory, batch_dice, stage, unpack_data,
                         deterministic, fp16, user_defined_config=user_defined_config)
        self.max_num_epochs = 1000
        self.initial_lr = 2e-2
        self.save_every = 40

        self.deep_supervision_scales = None
        self.ds_loss_weights = None
        self.pin_memory = True

        self.latent_levels = self.user_defined_config["latent_levels"]
        self.resolution_levels = self.user_defined_config["resolution_levels"]
        self.base_feature_channels = self.user_defined_config["base_feature_channels"]
        self.latent_feature_channels = self.user_defined_config["latent_feature_channels"]
        self.KL_loss_weight = self.user_defined_config["KL_loss_weight"]
        self.num_classes = self.user_defined_config.get("num_classes", 4)

        self.posterior = None
        self.prior = None
        self.likelihood = None

    def initialize(self, training=True, force_load_plans=False, p_dropout=0):
        """
        - replaced get_default_augmentation with get_moreDA_augmentation
        - enforce to only run this code once
        - loss function wrapper for deep supervision

        :param training:
        :param force_load_plans:
        :return:
        """
        if not self.was_initialized:
            maybe_mkdir_p(self.output_folder)

            if force_load_plans or (self.plans is None):
                self.load_plans_file()
            self.process_plans(self.plans)
            self.setup_DA_params()

            # deep supervision weights
            weights = np.array([1 / 2 ** i for i in range(self.latent_levels)])
            weights = weights / weights.sum()
            self.ds_loss_weights = weights
            self.loss = MultipleOutputLoss2(self.loss, self.ds_loss_weights)
            self.folder_with_preprocessed_data = join(self.dataset_directory, self.plans['data_identifier'] +
                                                      "_stage%d" % self.stage)
            if training:
                self.dl_tr, self.dl_val = self.get_basic_generators()
                if self.unpack_data:
                    print("unpacking dataset")
                    unpack_dataset(self.folder_with_preprocessed_data)
                    print("done")
                else:
                    print(
                        "INFO: Not unpacking data! Training may be slow due to that. Pray you are not using 2d or you "
                        "will wait all winter for your model to finish!")

                self.tr_gen, self.val_gen = get_moreDA_augmentation(
                    self.dl_tr, self.dl_val,
                    self.data_aug_params[
                        'patch_size_for_spatialtransform'],
                    self.data_aug_params,
                    deep_supervision_scales=self.deep_supervision_scales,
                    pin_memory=self.pin_memory,
                    use_nondetMultiThreadedAugmenter=False
                )
                self.print_to_log_file("TRAINING KEYS:\n %s" % (str(self.dataset_tr.keys())),
                                       also_print_to_console=False)
                self.print_to_log_file("VALIDATION KEYS:\n %s" % (str(self.dataset_val.keys())),
                                       also_print_to_console=False)
            else:
                pass

            self.initialize_network(training=training)
            if training:
                self.initialize_optimizer_and_scheduler()
        else:
            self.print_to_log_file('self.was_initialized is True, not running self.initialize again')
        self.was_initialized = True

    def initialize_network(self, training=False):
        if training:
            self.posterior = PHiSeg_PosteriorPrior(latent_levels=self.latent_levels,
                                                   resolution_levels=self.resolution_levels,
                                                   base_feature_channels=self.base_feature_channels,
                                                   latent_feature_channels=self.latent_feature_channels,
                                                   input_feature_dim=2,
                                                   weight_initializer=InitWeights_He(1e-2))
        self.prior = PHiSeg_PosteriorPrior(latent_levels=self.latent_levels,
                                           resolution_levels=self.resolution_levels,
                                           base_feature_channels=self.base_feature_channels,
                                           latent_feature_channels=self.latent_feature_channels,
                                           input_feature_dim=1,
                                           weight_initializer=InitWeights_He(1e-2))
        self.likelihood = PHiSeg_Likelihood(n_classes=self.num_classes, base_feature_channels=self.base_feature_channels,
                                            resolution_levels=self.resolution_levels,
                                            latent_levels=self.latent_levels,
                                            latent_feature_channels=self.latent_feature_channels,
                                            weight_initializer=InitWeights_He(1e-2))

        if torch.cuda.is_available():
            if training:
                self.posterior.cuda()
            self.prior.cuda()
            self.likelihood.cuda()

    def initialize_optimizer_and_scheduler(self):
        # assert self.network is not None, "self.initialize_network must be called first"
        parameters = chain(self.posterior.parameters(), self.prior.parameters(), self.likelihood.parameters())
        self.optimizer = torch.optim.SGD(parameters, self.initial_lr, weight_decay=self.weight_decay,
                                         momentum=0.99, nesterov=True)
        self.lr_scheduler = None

    def run_online_evaluation(self, output, target):
        """
        due to deep supervision the return value and the reference are now lists of tensors. We only need the full
        resolution output because this is what we are interested in in the end. The others are ignored
        :param output:
        :param target:
        :return:
        """
        target = target[0]
        output = output[0]
        return 0.0

    def validate(self, do_mirroring: bool = True, use_sliding_window: bool = True,
                 step_size: float = 0.5, save_softmax: bool = True, use_gaussian: bool = True, overwrite: bool = True,
                 validation_folder_name: str = 'validation_raw', debug: bool = False, all_in_gpu: bool = False,
                 segmentation_export_kwargs: dict = None, run_postprocessing_on_folds: bool = True):
        raise NotImplementedError

    def predict_preprocessed_data_return_seg_and_softmax(self, data: np.ndarray, do_mirroring: bool = True,
                                                         mirror_axes: Tuple[int] = None,
                                                         use_sliding_window: bool = True, step_size: float = 0.5,
                                                         use_gaussian: bool = True, pad_border_mode: str = 'constant',
                                                         pad_kwargs: dict = None, all_in_gpu: bool = False,
                                                         verbose: bool = True, mixed_precision=True) -> Tuple[
        np.ndarray, np.ndarray]:
        raise NotImplementedError

    def run_iteration(self, data_generator, do_backprop=True, run_online_evaluation=False):
        """
        gradient clipping improves training stability

        :param data_generator:
        :param do_backprop:
        :param run_online_evaluation:
        :return:
        """
        data_dict = next(data_generator)
        data = data_dict['data']
        target = data_dict['target']

        data = maybe_to_torch(data)
        target = maybe_to_torch(target)

        if torch.cuda.is_available():
            data = to_cuda(data)
            target = to_cuda(target)

        self.optimizer.zero_grad()

        if self.fp16:
            with autocast():
                post = self.posterior(torch.cat([data, target[0]], dim=1))
                prior = self.prior(data)
                logits = self.likelihood(prior[-1])
                for i in range(len(post[0])):
                    kli = KL_two_gaussian(post[0][i], post[1][i], prior[0][i], prior[1][i]) * self.ds_loss_weights[i]
                    if i == 0:
                        kl_loss = kli
                    else:
                        kl_loss = kl_loss + kli
                l = self.loss(logits, target[:self.latent_levels])
                l_tot = l + self.KL_loss_weight * (2 ** (self.epoch / self.max_num_epochs * 10)) * kl_loss
                del data

            if do_backprop:
                self.amp_grad_scaler.scale(l_tot).backward()
                self.amp_grad_scaler.unscale_(self.optimizer)
                parameters = chain(self.posterior.parameters(), self.prior.parameters(), self.likelihood.parameters())
                torch.nn.utils.clip_grad_norm_(parameters, 12)
                self.amp_grad_scaler.step(self.optimizer)
                self.amp_grad_scaler.update()
        else:
            post = self.posterior(torch.cat([data, target[0]], dim=1))
            prior = self.prior(data)
            logits = self.likelihood(prior[-1])
            for i in range(len(post[0])):
                kli = KL_two_gaussian(post[0][i], post[1][i], prior[0][i], prior[1][i]) * self.ds_loss_weights[i]
                if i == 0:
                    kl_loss = kli
                else:
                    kl_loss = kl_loss + kli
            l = self.loss(logits, target[:self.latent_levels])
            l_tot = l + self.KL_loss_weight * (2 ** (self.epoch / self.max_num_epochs * 10)) * kl_loss
            del data

            if do_backprop:
                l_tot.backward()
                parameters = chain(self.posterior.parameters(), self.prior.parameters(), self.likelihood.parameters())
                torch.nn.utils.clip_grad_norm_(parameters, 12)
                self.optimizer.step()

        # if run_online_evaluation:
        #     self.run_online_evaluation(output, target)
        #
        del target

        return l.detach().cpu().numpy(), kl_loss.detach().cpu().numpy(), l_tot.detach().cpu().numpy()

    def do_split(self):
        """
        The default split is a 5 fold CV on all available training cases. nnU-Net will create a split (it is seeded,
        so always the same) and save it as splits_final.pkl file in the preprocessed data directory.
        Sometimes you may want to create your own split for various reasons. For this you will need to create your own
        splits_final.pkl file. If this file is present, nnU-Net is going to use it and whatever splits are defined in
        it. You can create as many splits in this file as you want. Note that if you define only 4 splits (fold 0-3)
        and then set fold=4 when training (that would be the fifth split), nnU-Net will print a warning and proceed to
        use a random 80:20 data split.
        :return:
        """
        if self.fold == "all":
            # if fold==all then we use all images for training and validation
            tr_keys = val_keys = list(self.dataset.keys())
        else:
            splits_file = join(self.dataset_directory, "splits_final.pkl")

            # if the split file does not exist we need to create it
            if not isfile(splits_file):
                self.print_to_log_file("Creating new 5-fold cross-validation split...")
                splits = []
                all_keys_sorted = np.sort(list(self.dataset.keys()))
                kfold = KFold(n_splits=5, shuffle=True, random_state=12345)
                for i, (train_idx, test_idx) in enumerate(kfold.split(all_keys_sorted)):
                    train_keys = np.array(all_keys_sorted)[train_idx]
                    test_keys = np.array(all_keys_sorted)[test_idx]
                    splits.append(OrderedDict())
                    splits[-1]['train'] = train_keys
                    splits[-1]['val'] = test_keys
                save_pickle(splits, splits_file)

            else:
                self.print_to_log_file("Using splits from existing split file:", splits_file)
                splits = load_pickle(splits_file)
                self.print_to_log_file("The split file contains %d splits." % len(splits))

            self.print_to_log_file("Desired fold for training: %d" % self.fold)
            if self.fold < len(splits):
                tr_keys = splits[0]['train']
                val_keys = splits[0]['val']
                self.print_to_log_file("This split has %d training and %d validation cases."
                                       % (len(tr_keys), len(val_keys)))
            else:
                self.print_to_log_file("INFO: You requested fold %d for training but splits "
                                       "contain only %d folds. I am now creating a "
                                       "random (but seeded) 80:20 split!" % (self.fold, len(splits)))
                # if we request a fold that is not in the split file, create a random 80:20 split
                rnd = np.random.RandomState(seed=12345 + self.fold)
                keys = np.sort(list(self.dataset.keys()))
                idx_tr = rnd.choice(len(keys), int(len(keys) * 0.8), replace=False)
                idx_val = [i for i in range(len(keys)) if i not in idx_tr]
                tr_keys = [keys[i] for i in idx_tr]
                val_keys = [keys[i] for i in idx_val]
                self.print_to_log_file("This random 80:20 split has %d training and %d validation cases."
                                       % (len(tr_keys), len(val_keys)))

        tr_keys.sort()
        val_keys.sort()
        self.dataset_tr = OrderedDict()
        for i in tr_keys:
            self.dataset_tr[i] = self.dataset[i]
        self.dataset_val = OrderedDict()
        for i in val_keys:
            self.dataset_val[i] = self.dataset[i]

    def setup_DA_params(self):
        """
        - we increase roation angle from [-15, 15] to [-30, 30]
        - scale range is now (0.7, 1.4), was (0.85, 1.25)
        - we don't do elastic deformation anymore

        :return:
        """

        self.deep_supervision_scales = [[1, 1, 1]] + list(list(i) for i in 1 / np.cumprod(
            np.vstack(self.net_num_pool_op_kernel_sizes), axis=0))[:-1]

        if self.threeD:
            self.data_aug_params = default_3D_augmentation_params
            self.data_aug_params['rotation_x'] = (-30. / 360 * 2. * np.pi, 30. / 360 * 2. * np.pi)
            self.data_aug_params['rotation_y'] = (-30. / 360 * 2. * np.pi, 30. / 360 * 2. * np.pi)
            self.data_aug_params['rotation_z'] = (-30. / 360 * 2. * np.pi, 30. / 360 * 2. * np.pi)
            if self.do_dummy_2D_aug:
                self.data_aug_params["dummy_2D"] = True
                self.print_to_log_file("Using dummy2d data augmentation")
                self.data_aug_params["elastic_deform_alpha"] = \
                    default_2D_augmentation_params["elastic_deform_alpha"]
                self.data_aug_params["elastic_deform_sigma"] = \
                    default_2D_augmentation_params["elastic_deform_sigma"]
                self.data_aug_params["rotation_x"] = default_2D_augmentation_params["rotation_x"]
        else:
            self.do_dummy_2D_aug = False
            if max(self.patch_size) / min(self.patch_size) > 1.5:
                default_2D_augmentation_params['rotation_x'] = (-15. / 360 * 2. * np.pi, 15. / 360 * 2. * np.pi)
            self.data_aug_params = default_2D_augmentation_params
        self.data_aug_params["mask_was_used_for_normalization"] = self.use_mask_for_norm

        if self.do_dummy_2D_aug:
            self.basic_generator_patch_size = get_patch_size(self.patch_size[1:],
                                                             self.data_aug_params['rotation_x'],
                                                             self.data_aug_params['rotation_y'],
                                                             self.data_aug_params['rotation_z'],
                                                             self.data_aug_params['scale_range'])
            self.basic_generator_patch_size = np.array([self.patch_size[0]] + list(self.basic_generator_patch_size))
        else:
            self.basic_generator_patch_size = get_patch_size(self.patch_size, self.data_aug_params['rotation_x'],
                                                             self.data_aug_params['rotation_y'],
                                                             self.data_aug_params['rotation_z'],
                                                             self.data_aug_params['scale_range'])

        self.data_aug_params["scale_range"] = (0.7, 1.4)
        self.data_aug_params["do_elastic"] = False
        self.data_aug_params['selected_seg_channels'] = [0]
        self.data_aug_params['patch_size_for_spatialtransform'] = self.patch_size

        self.data_aug_params["num_cached_per_thread"] = 2

    def maybe_update_lr(self, epoch=None):
        if epoch is None:
            ep = self.epoch + 1
        else:
            ep = epoch
        self.optimizer.param_groups[0]['lr'] = poly_lr(ep, self.max_num_epochs, self.initial_lr, 0.9)
        self.print_to_log_file("lr:", np.round(self.optimizer.param_groups[0]['lr'], decimals=6))

    def maybe_save_checkpoint(self):
        if self.save_intermediate_checkpoints and (self.epoch % self.save_every == (self.save_every - 1)):
            self.print_to_log_file("saving scheduled checkpoint file...")
            if not self.save_latest_only:
                self.save_checkpoint(join(self.output_folder, "model_ep_%04.0d.model" % (self.epoch + 1)),
                                     save_optimizer=(self.epoch % self.save_every == (40 - 1)),
                                     save_posterior=(self.epoch % self.save_every == (40 - 1)))

        if self.epoch % 5 == (5 - 1):
            self.save_checkpoint(join(self.output_folder, "model_latest.model"), save_optimizer=True,
                                 save_posterior=True)
            self.print_to_log_file("done")

    def on_epoch_end(self):
        continue_training = self.epoch < self.max_num_epochs
        self.plot_progress()
        self.maybe_update_lr()
        self.maybe_save_checkpoint()
        return continue_training

    def run_training(self):
        self.maybe_update_lr()
        if not torch.cuda.is_available():
            self.print_to_log_file(
                "WARNING!!! You are attempting to run training on a CPU (torch.cuda.is_available() is False). This can be VERY slow!")

        _ = self.tr_gen.next()
        _ = self.val_gen.next()

        if torch.cuda.is_available():
            torch.cuda.empty_cache()

        self._maybe_init_amp()

        maybe_mkdir_p(self.output_folder)
        self.plot_network_architecture()

        if cudnn.benchmark and cudnn.deterministic:
            self.print_to_log_file(
                "torch.backends.cudnn.deterministic is True indicating a deterministic training is desired. "
                "But torch.backends.cudnn.benchmark is True as well and this will prevent deterministic training! "
                "If you want deterministic then set benchmark=False")

        if not self.was_initialized:
            self.initialize(True)

        while self.epoch < self.max_num_epochs:
            self.print_to_log_file("\nepoch: ", self.epoch)
            epoch_start_time = time()
            train_losses_epoch = []
            train_losses_combined = []
            train_losses_kl = []

            # train one epoch
            self.posterior.train()
            self.prior.train()
            self.likelihood.train()

            if self.use_progress_bar:
                with trange(self.num_batches_per_epoch) as tbar:
                    for b in tbar:
                        tbar.set_description("Epoch {}/{}".format(self.epoch + 1, self.max_num_epochs))

                        l = self.run_iteration(self.tr_gen, True)

                        tbar.set_postfix(loss=l[-1])
                        train_losses_epoch.append(l[-1])
                        train_losses_combined.append(l[-3])
                        train_losses_kl.append(l[-2])
            else:
                for _ in range(self.num_batches_per_epoch):
                    l = self.run_iteration(self.tr_gen, True)
                    train_losses_epoch.append(l[-1])
                    train_losses_combined.append(l[-3])
                    train_losses_kl.append(l[-2])

            self.all_tr_losses.append(np.mean(train_losses_combined))
            self.print_to_log_file(
                f"train loss : {np.mean(train_losses_epoch):.4f}, {np.mean(train_losses_combined):.4f}, {np.mean(train_losses_kl):.4f}")
            self.print_to_log_file("train loss std : %.4f" % np.std(train_losses_epoch))

            with torch.no_grad():
                # validation with train=False
                self.prior.eval()
                self.likelihood.eval()
                val_losses = []
                val_losses_combined = []
                val_losses_kl = []
                for b in range(self.num_val_batches_per_epoch):
                    l = self.run_iteration(self.val_gen, False, True)
                    val_losses.append(l[-1])
                    val_losses_kl.append(l[-2])
                    val_losses_combined.append(l[-3])
                self.all_val_losses.append(np.mean(val_losses_combined))
                self.print_to_log_file(
                    f"validation loss : {np.mean(val_losses):.4f}, {np.mean(val_losses_combined):.4f}, {np.mean(val_losses_kl):.4f}")

                if self.also_val_in_tr_mode:
                    # self.network.train()
                    # validation with train=True
                    val_losses = []
                    val_losses_combined = []
                    val_losses_kl = []
                    for b in range(self.num_val_batches_per_epoch):
                        l = self.run_iteration(self.val_gen, False)
                        val_losses.append(l[-1])
                        val_losses_kl.append(l[-2])
                        val_losses_combined.append(l[-3])
                    self.all_val_losses_tr_mode.append(np.mean(val_losses))
                    self.print_to_log_file(
                        f"validation loss : {np.mean(val_losses):.4f}, {np.mean(train_losses_combined):.4f}, {np.mean(train_losses_kl):.4f}")

            self.update_train_loss_MA()  # needed for lr scheduler and stopping of training
            continue_training = self.on_epoch_end()
            epoch_end_time = time()

            if not continue_training:
                # allows for early stopping
                break

            self.epoch += 1
            self.print_to_log_file("This epoch took %f s\n" % (epoch_end_time - epoch_start_time))

        self.epoch -= 1  # if we don't do this we can get a problem with loading model_final_checkpoint.

        # if self.save_final_checkpoint: self.save_checkpoint(join(self.output_folder, "model_final_checkpoint.model"))
        # # now we can delete latest as it will be identical with final
        # if isfile(join(self.output_folder, "model_latest.model")):
        #     os.remove(join(self.output_folder, "model_latest.model"))
        # if isfile(join(self.output_folder, "model_latest.model.pkl")):
        #     os.remove(join(self.output_folder, "model_latest.model.pkl"))

    def save_checkpoint(self, fname, save_optimizer=True, save_posterior=False):
        if save_posterior:
            post_state_dict = self.posterior.state_dict()
        else:
            post_state_dict = None
        prior_state_dict = self.prior.state_dict()
        likelihood_state_dict = self.likelihood.state_dict()
        if save_optimizer:
            optim_state_dict = self.optimizer.state_dict()
        else:
            optim_state_dict = None
        save_this = {
            "epoch": self.epoch,
            "post_state_dict": post_state_dict,
            "prior_state_dict": prior_state_dict,
            "likelihood_state_dict": likelihood_state_dict,
            "optim_state_dict": optim_state_dict,
            "plot_stuff": (self.all_tr_losses, self.all_val_losses)
        }
        if self.amp_grad_scaler is not None:
            save_this['amp_grad_scaler'] = self.amp_grad_scaler.state_dict()
        torch.save(save_this, fname)

    def load_checkpoint_ram(self, checkpoint, train=False):
        optim_state_dict = checkpoint["optim_state_dict"]
        post_state_dict = checkpoint["post_state_dict"]
        prior_state_dict = checkpoint["prior_state_dict"]
        likelihood_state_dict = checkpoint["likelihood_state_dict"]
        all_tr_losses, all_val_losses = checkpoint["plot_stuff"]
        self.all_val_losses = all_val_losses
        self.all_tr_losses = all_tr_losses
        epoch = checkpoint["epoch"]
        if train:
            self.posterior.load_state_dict(post_state_dict)
            self.optimizer.load_state_dict(optim_state_dict)
        self.prior.load_state_dict(prior_state_dict)
        self.likelihood.load_state_dict(likelihood_state_dict)
        self.epoch = epoch
        if "amp_grad_scaler" in checkpoint.keys():
            self._maybe_init_amp()
            self.amp_grad_scaler.load_state_dict(checkpoint['amp_grad_scaler'])

    def load_best_checkpoint(self, train=True):
        if self.fold is None:
            raise RuntimeError("Cannot load best checkpoint if self.fold is None")
        if isfile(join(self.output_folder, "model_best.model")):
            self.load_checkpoint(join(self.output_folder, "model_best.model"), train=train)
        else:
            self.print_to_log_file("WARNING! model_best.model does not exist! Cannot load best checkpoint. Falling "
                                   "back to load_latest_checkpoint")
            self.load_latest_checkpoint(train)

    def load_latest_checkpoint(self, train=True):
        if isfile(join(self.output_folder, "model_final_checkpoint.model")):
            return self.load_checkpoint(join(self.output_folder, "model_final_checkpoint.model"), train=train)
        if isfile(join(self.output_folder, "model_latest.model")):
            return self.load_checkpoint(join(self.output_folder, "model_latest.model"), train=train)
        if isfile(join(self.output_folder, "model_best.model")):
            return self.load_best_checkpoint(train)
        raise RuntimeError("No checkpoint found")

    def load_checkpoint(self, fname, train=True):
        self.print_to_log_file("loading checkpoint", fname, "train=", train)
        if not self.was_initialized:
            self.initialize(train)
        saved_model = torch.load(fname, map_location=torch.device('cpu'))
        self.load_checkpoint_ram(saved_model, train)


    def plot_progress(self):
        """
        Should probably by improved
        :return:
        """
        try:
            font = {'weight': 'normal',
                    'size': 18}

            matplotlib.rc('font', **font)

            fig = plt.figure(figsize=(30, 24))
            ax = fig.add_subplot(111)
            # x_values = list(range(self.epoch + 1))
            ax.plot(self.all_tr_losses, color='b', ls='-', label="loss_tr")
            ax.plot(self.all_val_losses, color='r', ls='-', label="loss_val, train=False")
            ax.set_xlabel("epoch")
            ax.set_ylabel("loss")
            ax.legend()

            fig.savefig(join(self.output_folder, "progress.png"))
            plt.close()
        except IOError:
            self.print_to_log_file("failed to plot: ", sys.exc_info())





