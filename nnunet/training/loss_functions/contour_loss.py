from torch import nn, Tensor
import kornia
from nnunet.training.loss_functions.dice_loss import SoftDiceLoss
from nnunet.training.loss_functions.crossentropy import RobustCrossEntropyLoss
from nnunet.utilities.nd_softmax import softmax_helper


class ContourMap(nn.Module):
    def __init__(self, band_window=5, band_sigma=5):
        super(ContourMap, self).__init__()
        self.band_window = band_window
        self.band_sigma = band_sigma

    def forward(self, output, target):
        cont1 = kornia.filters.sobel(output)
        cont2 = kornia.filters.sobel(target)
        cont1b = kornia.filters.gaussian_blur2d(cont1, (self.band_window,
                                                        self.band_window),
                                                (self.band_sigma,
                                                 self.band_sigma))
        cont2b = kornia.filters.gaussian_blur2d(cont2, (self.band_window,
                                                        self.band_window),
                                                (self.band_sigma,
                                                 self.band_sigma))
        return cont1b, cont2b


class DSContourWeightedLoss(nn.Module):
    """
    this is just a compatibility layer because my target tensor is float and has an extra dimension
    """

    def __init__(self, ds_weights, contour_weighted_depth=1, contour_weight=1, dice_weight=1, ce_weight=1):
        super(DSContourWeightedLoss, self).__init__()
        self.ds_weights = ds_weights
        self.contour_weighted_depth = contour_weighted_depth
        self.contour_weight = contour_weight
        self.dice_weight = dice_weight
        self.ce_weight = ce_weight
        # sub-modules
        self.contblur = ContourMap(band_window=5, band_sigma=5)
        self.dice_loss = SoftDiceLoss(apply_nonlin=softmax_helper, batch_dice=True, do_bg=False, smooth=1e-5)
        self.ce_loss = RobustCrossEntropyLoss()

    def forward(self, output, target):
        loss = self.ds_weights[0] * (self.ce_loss(output[0], target[0]) * self.ce_weight +
                                     self.dice_loss(output[0], target[0]) * self.dice_weight)
        cont1b, cont2b = self.contblur(output[0], target[0])

        cont_loss = cont1b[:, 2] * cont2b[:, 2]
        loss = loss
        for i in range(1, len(output)):
            loss = loss + self.ds_weights[i] * (self.ce_loss(output[i], target[i]) * self.ce_weight +
                                                self.dice_loss(output[i], target[i]) * self.dice_weight)
        return loss, cont_loss
