from pathlib import Path
import shutil
import pandas as pd
import re
import nibabel as nib
import tqdm
from collections import OrderedDict
from batchgenerators.utilities.file_and_folder_operations import *
import numpy as np
from sklearn.model_selection import KFold


# paths to datasets
MM_training = Path('/mnt/wind/Datasets/MM_MICCAI2020/original/train_data_Labeled')
MM_info_file = Path('/mnt/wind/Datasets/MM_MICCAI2020/'
                    'original/train_data_unlabeled_and_datainfo/M&Ms Dataset Information.xlsx')
MM_intermediate = Path('/mnt/barrel/tmp/ACDCandMM')

# convert MM folder to ACDC structure
def MMlabel2ACDC(nibimg):
    fdata = nibimg.get_fdata()
    mask1 = (fdata == 1.)
    mask3 = (fdata == 3.)
    fdata[mask1] = 3.
    fdata[mask3] = 1.
    return nib.Nifti1Image(fdata, nibimg.affine, header=nibimg.header)


MM_CONVERSION = True
if MM_CONVERSION:
    MM_info = pd.read_excel(MM_info_file)
    MM_subject_pattern = re.compile(r'^([A-Z][0-9])+$')
    subject_dirs = [d for d in MM_training.iterdir() if MM_subject_pattern.search(d.parts[-1])]
    for isd, sd in tqdm.tqdm(enumerate(subject_dirs)):
        patient_code = sd.parts[-1]

        # load 4D volume
        im_nib = nib.load(sd / f'{patient_code}_sa.nii.gz')
        seg_nib = nib.load(sd / f'{patient_code}_sa_gt.nii.gz')

        # slicing
        patient_info = MM_info[MM_info['External code'] == patient_code]
        ED, ES, Vendor, Centre = patient_info.iloc[0]['ED'], patient_info.iloc[0]['ES'],\
                                 patient_info.iloc[0]['Vendor'], patient_info.iloc[0]['Centre']
        ED_im_nib = im_nib.slicer[:, :, :, ED]
        ED_seg_nib = MMlabel2ACDC(seg_nib.slicer[:, :, :, ED])
        ES_im_nib = im_nib.slicer[:, :, :, ES]
        ES_seg_nib = MMlabel2ACDC(seg_nib.slicer[:, :, :, ES])

        # save to file
        new_subject_num = 101 + isd
        new_subject_code = f'patient{new_subject_num}'
        subject_destination = MM_intermediate / 'training' / new_subject_code
        vol4d_destination = subject_destination / f'{new_subject_code}_4d.nii.gz'
        ED_im_destination = subject_destination / f'{new_subject_code}_frame{ED:02d}.nii.gz'
        ED_seg_destination = subject_destination / f'{new_subject_code}_frame{ED:02d}_gt.nii.gz'
        ES_im_destination = subject_destination / f'{new_subject_code}_frame{ES:02d}.nii.gz'
        ES_seg_destination = subject_destination / f'{new_subject_code}_frame{ES:02d}_gt.nii.gz'
        subject_destination.mkdir(parents=True, exist_ok=True)
        shutil.copy(sd / f'{patient_code}_sa.nii.gz', vol4d_destination)
        nib.save(ED_im_nib, ED_im_destination)
        nib.save(ED_seg_nib, ED_seg_destination)
        nib.save(ES_im_nib, ES_im_destination)
        nib.save(ES_seg_nib, ES_seg_destination)

    print("M&M Conversion done!")


if __name__ == "__main__":
    folder = (MM_intermediate / 'training').as_posix()
    out_folder = "/mnt/barrel/Data/nnunetdata/raw/nnUNet_raw_data/Task600_ACDCandMM"

    maybe_mkdir_p(join(out_folder, "imagesTr"))
    maybe_mkdir_p(join(out_folder, "labelsTr"))

    # train
    all_train_files = []
    patient_dirs_train = subfolders(folder, prefix="patient")
    for p in tqdm.tqdm(patient_dirs_train):
        current_dir = p
        data_files_train = [i for i in subfiles(current_dir, suffix=".nii.gz") if i.find("_gt") == -1 and i.find("_4d") == -1]
        corresponding_seg_files = [i[:-7] + "_gt.nii.gz" for i in data_files_train]
        for d, s in zip(data_files_train, corresponding_seg_files):
            patient_identifier = d.split("/")[-1][:-7]
            all_train_files.append(patient_identifier + "_0000.nii.gz")
            shutil.copy(d, join(out_folder, "imagesTr", patient_identifier + "_0000.nii.gz"))
            shutil.copy(s, join(out_folder, "labelsTr", patient_identifier + ".nii.gz"))

    json_dict = OrderedDict()
    json_dict['name'] = "ACDC"
    json_dict['description'] = "cardias cine MRI segmentation"
    json_dict['tensorImageSize'] = "4D"
    json_dict['reference'] = "see ACDC challenge"
    json_dict['licence'] = "see ACDC challenge"
    json_dict['release'] = "0.0"
    json_dict['modality'] = {
        "0": "MRI",
    }
    json_dict['labels'] = {
        "0": "background",
        "1": "RV",
        "2": "MLV",
        "3": "LVC"
    }
    json_dict['numTraining'] = len(all_train_files)
    json_dict['numTest'] = 0
    json_dict['training'] = [{'image': "./imagesTr/%s.nii.gz" % i.split("/")[-1][:-12], "label": "./labelsTr/%s.nii.gz" % i.split("/")[-1][:-12]} for i in
                             all_train_files]
    json_dict['test'] = []

    save_json(json_dict, os.path.join(out_folder, "dataset.json"))

    # create a dummy split (patients need to be separated)
    splits = []
    patients = np.unique([i[:10] for i in all_train_files])
    patientids = [i[:-12] for i in all_train_files]

    kf = KFold(n_splits=5, shuffle=True, random_state=12345)
    for tr, val in kf.split(patients):
        splits.append(OrderedDict())
        tr_patients = patients[tr]
        splits[-1]['train'] = [i[:-12] for i in all_train_files if i[:10] in tr_patients]
        val_patients = patients[val]
        splits[-1]['val'] = [i[:-12] for i in all_train_files if i[:10] in val_patients]

    save_pickle(splits, "/mnt/barrel/Data/nnunetdata/raw/nnUNet_raw_data/Task600_ACDCandMM/splits_final.pkl")
