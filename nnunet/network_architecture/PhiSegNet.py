#  Copyright 2020 Division of Medical Image Computing, German Cancer Research Center (DKFZ), Heidelberg, Germany
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.


from copy import deepcopy
from nnunet.utilities.nd_softmax import softmax_helper
from torch import nn
import torch
import numpy as np
from nnunet.network_architecture.initialization import InitWeights_He
from nnunet.network_architecture.neural_network import SegmentationNetwork
import torch.nn.functional
from typing import Union, Tuple, List


class ConvDropoutNormNonlin(nn.Module):
    def __init__(self, input_channels, output_channels,
                 conv_op=nn.Conv2d, conv_kwargs=None,
                 norm_op=nn.BatchNorm2d, norm_op_kwargs=None,
                 dropout_op=nn.Dropout2d, dropout_op_kwargs=None,
                 nonlin=nn.LeakyReLU, nonlin_kwargs=None):
        super(ConvDropoutNormNonlin, self).__init__()
        if nonlin_kwargs is None:
            nonlin_kwargs = {'negative_slope': 1e-2, 'inplace': True}
        if dropout_op_kwargs is None:
            dropout_op_kwargs = {'p': 0.5, 'inplace': True}
        if norm_op_kwargs is None:
            norm_op_kwargs = {'eps': 1e-5, 'affine': True, 'momentum': 0.1}
        if conv_kwargs is None:
            conv_kwargs = {'kernel_size': 3, 'stride': 1, 'padding': 1, 'dilation': 1, 'bias': True}

        self.nonlin_kwargs = nonlin_kwargs
        self.nonlin = nonlin
        self.dropout_op = dropout_op
        self.dropout_op_kwargs = dropout_op_kwargs
        self.norm_op_kwargs = norm_op_kwargs
        self.conv_kwargs = conv_kwargs
        self.conv_op = conv_op
        self.norm_op = norm_op

        self.conv = self.conv_op(input_channels, output_channels, **self.conv_kwargs)
        if self.dropout_op is not None and self.dropout_op_kwargs['p'] is not None and self.dropout_op_kwargs[
            'p'] > 0:
            self.dropout = self.dropout_op(**self.dropout_op_kwargs)
        else:
            self.dropout = None
        self.instnorm = self.norm_op(output_channels, **self.norm_op_kwargs)
        self.lrelu = self.nonlin(**self.nonlin_kwargs)

    def forward(self, x):
        x = self.conv(x)
        if self.dropout is not None:
            x = self.dropout(x)
        return self.lrelu(self.instnorm(x))


class ConvDropoutNonlinNorm(ConvDropoutNormNonlin):
    def forward(self, x):
        x = self.conv(x)
        if self.dropout is not None:
            x = self.dropout(x)
        return self.instnorm(self.lrelu(x))


class StackedConvLayers(nn.Module):
    def __init__(self, input_feature_channels, output_feature_channels, num_convs,
                 conv_op=nn.Conv2d, conv_kwargs=None,
                 norm_op=nn.BatchNorm2d, norm_op_kwargs=None,
                 dropout_op=nn.Dropout2d, dropout_op_kwargs=None,
                 nonlin=nn.LeakyReLU, nonlin_kwargs=None, first_stride=None, basic_block=ConvDropoutNormNonlin):
        self.input_channels = input_feature_channels
        self.output_channels = output_feature_channels

        if nonlin_kwargs is None:
            nonlin_kwargs = {'negative_slope': 1e-2, 'inplace': True}
        if dropout_op_kwargs is None:
            dropout_op_kwargs = {'p': 0.0, 'inplace': True}
        if norm_op_kwargs is None:
            norm_op_kwargs = {'eps': 1e-5, 'affine': True, 'momentum': 0.1}
        if conv_kwargs is None:
            conv_kwargs = {'kernel_size': 3, 'stride': 1, 'padding': 1, 'dilation': 1, 'bias': True}

        self.nonlin_kwargs = nonlin_kwargs
        self.nonlin = nonlin
        self.dropout_op = dropout_op
        self.dropout_op_kwargs = dropout_op_kwargs
        self.norm_op_kwargs = norm_op_kwargs
        self.conv_kwargs = conv_kwargs
        self.conv_op = conv_op
        self.norm_op = norm_op

        if first_stride is not None:
            self.conv_kwargs_first_conv = deepcopy(conv_kwargs)
            self.conv_kwargs_first_conv['stride'] = first_stride
        else:
            self.conv_kwargs_first_conv = conv_kwargs

        super(StackedConvLayers, self).__init__()
        self.blocks = nn.Sequential(
            *([basic_block(input_feature_channels, output_feature_channels, self.conv_op,
                           self.conv_kwargs_first_conv,
                           self.norm_op, self.norm_op_kwargs, self.dropout_op, self.dropout_op_kwargs,
                           self.nonlin, self.nonlin_kwargs)] +
              [basic_block(output_feature_channels, output_feature_channels, self.conv_op,
                           self.conv_kwargs,
                           self.norm_op, self.norm_op_kwargs, self.dropout_op, self.dropout_op_kwargs,
                           self.nonlin, self.nonlin_kwargs) for _ in range(num_convs - 1)]))

    def forward(self, x):
        return self.blocks(x)


class Upsample(nn.Module):
    def __init__(self, size=None, scale_factor=None, mode='nearest', align_corners=False):
        super(Upsample, self).__init__()
        self.align_corners = align_corners
        self.mode = mode
        self.scale_factor = scale_factor
        self.size = size

    def forward(self, x):
        if self.scale_factor > 1:
            z = nn.functional.interpolate(x, size=self.size, scale_factor=self.scale_factor, mode=self.mode,
                                          align_corners=self.align_corners)
        else:
            z = x
        return z


class PHiSeg_PosteriorPrior(nn.Module):
    """PHiSeg posterior or prior network."""

    def __init__(self, latent_levels=5, resolution_levels=7, base_feature_channels=32,
                 latent_feature_channels=2, input_feature_dim=1, weight_initializer=None):
        super(PHiSeg_PosteriorPrior, self).__init__()
        # configurations
        self.latent_levels = latent_levels
        self.resolution_levels = resolution_levels
        self.base_feature_channels = base_feature_channels
        self.latent_feature_channels = latent_feature_channels
        self.input_feature_dim = input_feature_dim
        self.weight_initializer = weight_initializer
        self.input_shape_must_be_divisible_by = 2 ** resolution_levels

        self.num_channels = [self.base_feature_channels * max(min(2 * level, 10), 1)
                             for level in range(self.resolution_levels)]

        # build up resolution layers of all resolution levels
        self.resolution_layers = []
        self.feature_down = []
        for res_level in range(self.resolution_levels):
            if res_level == 0:
                self.resolution_layers.append(
                    StackedConvLayers(self.input_feature_dim, self.num_channels[res_level], 3)
                )
            else:
                self.resolution_layers.append(
                    StackedConvLayers(self.num_channels[res_level - 1], self.num_channels[res_level], 3)
                )
            # down-sampling using MaxPool2d
            if res_level < self.resolution_levels - 1:
                self.feature_down.append(nn.MaxPool2d((2, 2)))

        # build up latent layers
        self.convs_after_up = [None] * (self.latent_levels - 1)
        self.convs_after_concat = [None] * (self.latent_levels - 1)
        self.latent_up = [None] * (self.latent_levels - 1)
        self.convs_mu = [None] * self.latent_levels
        self.convs_sigma = [None] * self.latent_levels
        musig_conv_args = {'kernel_size': 1, 'stride': 1, 'padding': 0, 'dilation': 1, 'bias': True}
        for latent_level in reversed(range(self.latent_levels)):
            if latent_level == self.latent_levels - 1:
                # lowest feature level, from features to mu and sigma
                z_input_dim = self.num_channels[self.resolution_levels - self.latent_levels + latent_level]
                self.convs_mu[latent_level] = StackedConvLayers(z_input_dim, self.latent_feature_channels, num_convs=1,
                                                                conv_kwargs=musig_conv_args)
                self.convs_sigma[latent_level] = StackedConvLayers(z_input_dim, self.latent_feature_channels,
                                                                   num_convs=1,
                                                                   conv_kwargs=musig_conv_args)
            else:
                # upsampling -> conv (z-f) -> concat -> conv (f-pre_z) -> conv (pre_z-mu, sigma)
                self.latent_up[latent_level] = Upsample(scale_factor=2, mode="bilinear")
                #  conv z-f
                self.convs_after_up[latent_level] = StackedConvLayers(self.latent_feature_channels,
                                                                      self.latent_feature_channels * self.base_feature_channels,
                                                                      num_convs=2)
                # conv cont - pre_z
                dim_before_concat = self.num_channels[self.resolution_levels - self.latent_levels + latent_level]
                dim_after_concat = dim_before_concat + self.latent_feature_channels * self.base_feature_channels
                self.convs_after_concat[latent_level] = StackedConvLayers(dim_after_concat, dim_before_concat,
                                                                          num_convs=2)
                self.convs_mu[latent_level] = StackedConvLayers(dim_before_concat, self.latent_feature_channels,
                                                                num_convs=1,
                                                                conv_kwargs=musig_conv_args)
                self.convs_sigma[latent_level] = StackedConvLayers(dim_before_concat, self.latent_feature_channels,
                                                                   num_convs=1,
                                                                   conv_kwargs=musig_conv_args)
        self.resolution_layers = nn.ModuleList(self.resolution_layers)
        self.feature_down = nn.ModuleList(self.feature_down)
        self.convs_after_up = nn.ModuleList(self.convs_after_up)
        self.convs_after_concat = nn.ModuleList(self.convs_after_concat)
        self.convs_mu = nn.ModuleList(self.convs_mu)
        self.convs_sigma = nn.ModuleList(self.convs_sigma)
        self.latent_up = nn.ModuleList(self.latent_up)

        if self.weight_initializer is not None:
            self.apply(self.weight_initializer)

    def forward(self, x):
        """ """
        # pre-z features
        pre_z_features = []
        for resolution_level in range(self.resolution_levels):
            if resolution_level == 0:
                z = self.resolution_layers[resolution_level](x)
            else:
                z = self.feature_down[resolution_level - 1](pre_z_features[resolution_level - 1])
                z = self.resolution_layers[resolution_level](z)
            pre_z_features.append(z)

        # now build z
        mu_list, sigma_list, z_list = [None] * self.latent_levels, \
                                      [None] * self.latent_levels, [None] * self.latent_levels
        for latent_level in reversed(range(self.latent_levels)):
            pre_z_level = self.resolution_levels - self.latent_levels + latent_level
            if latent_level == self.latent_levels - 1:
                zfeat = pre_z_features[pre_z_level]
            else:
                zup = self.latent_up[latent_level](z_list[latent_level + 1])
                zconv = self.convs_after_up[latent_level](zup)
                zconcat = torch.concat([pre_z_features[pre_z_level], zconv], dim=1)
                zfeat = self.convs_after_concat[latent_level](zconcat)

            mu = self.convs_mu[latent_level](zfeat)
            sigma = self.convs_sigma[latent_level](zfeat)

            z = mu + sigma * torch.randn_like(mu)
            mu_list[latent_level] = mu
            sigma_list[latent_level] = sigma
            z_list[latent_level] = z
        return mu_list, sigma_list, z_list


class PHiSeg_Likelihood(nn.Module):
    def __init__(self, n_classes=4, base_feature_channels=32, resolution_levels=7, latent_levels=5,
                 latent_feature_channels=2, weight_initializer=None):
        super(PHiSeg_Likelihood, self).__init__()
        self.n_classes = n_classes
        self.base_feature_channels = base_feature_channels
        self.resolution_levels = resolution_levels
        self.latent_levels = latent_levels
        self.n_classes = n_classes
        self.latent_feature_channels = latent_feature_channels
        self.weight_initializer = weight_initializer

        self.convs_before_up = [None] * self.latent_levels
        self.convs_after_up = [None] * self.latent_levels
        self.convs_after_concat = [None] * (self.latent_levels - 1)
        self.convs_logits = [None] * self.latent_levels
        self.up1 = [None] * self.latent_levels
        self.up2 = [None] * (self.latent_levels - 1)
        self.up_logits = [None] * (self.latent_levels - 1)

        self.num_channels = [self.base_feature_channels * max(min(2 * level, 10), 1)
                             for level in range(self.resolution_levels)]

        # z -> conv, conv
        logits_conv_args = {'kernel_size': 1, 'stride': 1, 'padding': 0, 'dilation': 1, 'bias': True}
        for latent_level in reversed(range(self.latent_levels)):
            scale_factor = 2 ** (self.resolution_levels - self.latent_levels)
            corresponding_z_level = self.resolution_levels - self.latent_levels + latent_level
            self.convs_before_up[latent_level] = StackedConvLayers(self.latent_feature_channels,
                                                                   self.num_channels[corresponding_z_level],
                                                                   num_convs=2)
            self.up1[latent_level] = Upsample(scale_factor=scale_factor, mode="bilinear")
            self.convs_after_up[latent_level] = StackedConvLayers(self.num_channels[corresponding_z_level],
                                                                  self.num_channels[corresponding_z_level],
                                                                  num_convs=1)
            if latent_level < self.latent_levels - 1:
                in_feat_dim = self.num_channels[corresponding_z_level] + self.num_channels[corresponding_z_level + 1]
                self.convs_after_concat[latent_level] = StackedConvLayers(in_feat_dim,
                                                                          self.num_channels[corresponding_z_level], num_convs=2)
                self.up2[latent_level] = Upsample(scale_factor=2, mode="bilinear")
                self.up_logits[latent_level] = Upsample(scale_factor=2, mode="bilinear")

            self.convs_logits[latent_level] = StackedConvLayers(self.num_channels[corresponding_z_level],
                                                                self.n_classes, num_convs=1,
                                                                conv_kwargs=logits_conv_args)
        self.convs_before_up = nn.ModuleList(self.convs_before_up)
        self.convs_after_up = nn.ModuleList(self.convs_after_up)
        self.convs_after_concat = nn.ModuleList(self.convs_after_concat)
        self.convs_logits = nn.ModuleList(self.convs_logits)
        self.up1 = nn.ModuleList(self.up1)
        self.up2 = nn.ModuleList(self.up2)
        self.up_logits = nn.ModuleList(self.up_logits)

        if self.weight_initializer is not None:
            self.apply(self.weight_initializer)

    def forward(self, z_list):
        z_before_logits = [None] * self.latent_levels
        logits_list = [None] * self.latent_levels
        for latent_level in reversed(range(self.latent_levels)):
            # conv, conv -> up -> conv
            z = z_list[latent_level]
            z = self.convs_before_up[latent_level](z)
            z = self.up1[latent_level](z)
            z = self.convs_after_up[latent_level](z)
            if latent_level < self.latent_levels - 1:
                # up sampling lower level z
                z_pre = self.up2[latent_level](z_before_logits[latent_level + 1],)
                z = torch.concat([z_pre, z], dim=1)
                z = self.convs_after_concat[latent_level](z)
                # compute logits
                z_before_logits[latent_level] = z
                logits = self.convs_logits[latent_level](z)
                logits_up = self.up_logits[latent_level](logits_list[latent_level + 1])
                logits = logits + logits_up
            else:
                z_before_logits[latent_level] = z
                logits = self.convs_logits[latent_level](z)
            logits_list[latent_level] = logits
        return logits_list


def KL_two_gaussian(mu1, sigma1, mu2, sigma2):
    sigma1_s = sigma1 ** 2
    sigma2_s = sigma2 ** 2

    log_sigma1_s = torch.log(sigma1_s + 1e-10)
    log_sigma2_s = torch.log(sigma2_s + 1e-10)

    _t = sigma1_s.add((mu2 - mu1) ** 2).div(sigma2_s + 1e-10)
    kl = torch.mean(log_sigma2_s - log_sigma1_s + _t - 1.0)
    return kl


if __name__ == "__main__":
    x = torch.randn((20, 1, 256, 256)).cuda()
    s = torch.randn((20, 1, 256, 256)).cuda()
    input = {"image": x, "seg": s}
    phi_seg = PHiSeg(latent_levels=5, resolution_levels=7, base_feature_channels=16)
    phi_seg.cuda()
    phi_seg.train()
    res = phi_seg(input)
    # phi_seg.eval()
    # res = phi_seg(input)
    print("Done!")
