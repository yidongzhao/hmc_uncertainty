# HMC Uncertainty Estimation in Medical Image Segmentation

This repository implements Uncertainty Estimation with Hamiltonina Monte Carlo (HMC) and is built upon the **[nnunetv1](https://github.com/MIC-DKFZ/nnUNet/tree/nnunetv1)** framework. The ensembling process of HMC is essentially conllecting checkpoints of training during SGD-momentum, with additional injected noise.

## Installation
Create a new environment and use ```pip install -e .``` for editable pip installation.

## Setting the Environmental Variables 
The following environmental variables specifies the paths to raw data, processed data and training results. 
```bash
export nnUNet_raw_data_base=/path/to/raw
export nnUNet_preprocessed=/path/to/processed
export RESULTS_FOLDER=/path/to/results
export nnUNet_config=/path/to/configs
```

## Training

### Additional Trainers
Our repository provides implementation for PhiSeg and MC-Dropout under the nnunet-v1 framework. 
- [PHiSeg_trainer](https://gitlab.tudelft.nl/yidongzhao/hmc_uncertainty/-/blob/codereading/nnunet/training/network_training/PHiSeg_trainer.py) for PHiSeg network training under nnunet setting.

- [nnUNetTrainerV2_dropout](https://gitlab.tudelft.nl/yidongzhao/hmc_uncertainty/-/blob/codereading/nnunet/training/network_training/nnUNetTrainerV2_dropout.py) for MC-Dropout training.

- [nnUNetTrainerV2_cSGHMC](https://gitlab.tudelft.nl/yidongzhao/hmc_uncertainty/-/blob/codereading/nnunet/training/network_training/nnUNetTrainerV2_cSGHMC.py) for HMC training.

  
### Configuration files
Exemplar configuration `json` files can be found [here](https://gitlab.tudelft.nl/yidongzhao/hmc_uncertainty/-/tree/codereading/trainer_configs), the temperature, dataset size and learning rate etc. are configurable via modifying the configuration file. 

```json
{
 "const_after":  0.6,
 "enable_hmc":  true,
 "epoch_per_cycle":  333,
 "identifier":  "mid_lr_m3_sc",
 "initial_lr":  0.02,
 "mode_jumping":  "high_lr",
 "n_train":  1518,
 "num_of_cycles":  3,
 "restart_epochs":  10,
 "restart_lr":  0.1,
 "restart_slope":  0.01,
 "sampling_every":  4,
 "temper":  0.0001,
 "warm_up_ratio":  0.6
}


```
Explanation:  
`epoch_per_cycle`: Number of epochs per cycle in annealing traning.  
`num_of_cycles`: Number of annealing cycles.  
`initial_lr`: The initial learning rate.  
`const_after`: The learning rate will keep constant after this fraction of "epoch_per_cycle".  




The configuration files must be save under the path defined by environmental variable  `$nnUNet_config`.

To start training:
```bash
# assume that csghmc.json is saved in $nnUNet_config
nnUNet_train 2d nnUNetTrainerV2_cSGHMC Task027_ACDC 0 --user_defined_config csghmc 

```

## Test
### Test with customized script.
Scripts for running inference  are placed [here](https://gitlab.tudelft.nl/yidongzhao/hmc_uncertainty/-/tree/codereading/misc/evaluation).  


# Citation
```bibtex
@inproceedings{zhao2022efficient,
  title={Efficient Bayesian uncertainty estimation for nnU-Net},
  author={Zhao, Yidong and Yang, Changchun and Schweidtmann, Artur and Tao, Qian},
  booktitle={International Conference on Medical Image Computing and Computer-Assisted Intervention},
  pages={535--544},
  year={2022},
  organization={Springer}
}

@article{zhao2024bayesian,
  title={Bayesian Uncertainty Estimation by Hamiltonian Monte Carlo: Applications to Cardiac MRI Segmentation},
  author={Zhao, Yidong and Tourais, Joao and Pierce, Iain and Nitsche, Christian and Treibel, Thomas A and Weing{\"a}rtner, Sebastian and Schweidtmann, Artur M and Tao, Qian},
  journal={arXiv preprint arXiv:2403.02311},
  year={2024}
}
```